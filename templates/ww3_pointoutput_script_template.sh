echo ' '
echo ' '
echo '###############################'
echo "  POINT OUTPUT FOR BUOY @@BUOYNAME"
echo "  BUOY ID: @@BUOYID"
echo "-------------------------------"
echo "Mean wave params:"

mod=@@BUOY_GRID
timestr=`echo $t_beg | cut -f1 -d' '`

cat > ww3_outp.inp << EOF
$ WAVEWATCH III Point output post-processing
$ ------------------------------------------
   $t_beg     $dt_grads  $tn
$
@@BUOYID
 -1
$
$ output of type 2: tables of mean parameters, see page 101 of handbook
  2
$ 2: mean wave pars
  2  50
$  1  50
$
$ End of input file
EOF
 
ln -sf mod_def.$mod mod_def.ww3
ln -sf out_pnt.$mod out_pnt.ww3
fname="@@BUOYNAME_wave_params_$timestr.tab"
echo "      Screen ouput routed to $path_log/ww3_outp.@@BUOYNAME_"$timestr"_wave_params.out"
$path_e/ww3_outp > $path_log/ww3_outp.@@BUOYNAME_wave_params.out
echo "      tab50.ww3 routed to $path_res/$fname"
mv tab50.ww3 $path_res/$fname
rm -f ww3_outp.inp
echo "Mean wave params tab correctly generated"
echo "-------------------------------"

echo "-------------------------------"
echo "Depth, currents, winds:"

cat > ww3_outp.inp << EOF
$ WAVEWATCH III Point output post-processing
$ ------------------------------------------
   $t_beg     $dt_grads  $tn
$
@@BUOYID
 -1
$
$ output of type 2: tables of mean parameters, see page 101 of handbook
  2
$ 1: Depth, currents, winds
  1  50
$  1  50
$
$ End of input file
EOF

fname="@@BUOYNAME_depth_curr_winds_$timestr.tab"
echo "      Screen ouput routed to $path_log/ww3_outp.@@BUOYNAME_depth_curr_winds.out"
$path_e/ww3_outp > $path_log/ww3_outp.@@BUOYNAME_depth_curr_winds.out
echo "      tab50.ww3 routed to $path_res/@@BUOYNAME_depth_curr_winds.tab"
mv tab50.ww3 $path_res/$fname
rm -f ww3_outp.inp
echo "Depth, currents, winds tab correctly generated"
echo "-------------------------------"

echo "-------------------------------"
echo "Spectra, tranfer file:"

cat > ww3_outp.inp << EOF
$ WAVEWATCH III Point output post-processing
$ ------------------------------------------
   $t_beg     $dt_grads  $tn
$
@@BUOYID
 -1
$
$ output of type 1: wave spectra, see page 101 of handbook
  1
$ 3: transfer file
  3   0.  0.  33  F
$
$ End of input file
EOF

echo "      Screen ouput routed to $path_log/ww3_outp.@@BUOYNAME_spectra.out"
$path_e/ww3_outp > "$path_log/ww3_outp.@@BUOYNAME_spectra.out"
fname="@@BUOYNAME_spectra_$timestr.spc";
echo "      saving tranfer file to $path_res/$fname"
mv ww3.*.spc $path_res/$fname
rm -f ww3_outp.inp
echo "Spectra tranfer file correctly generated"
echo "-------------------------------"

echo "-------------------------------"
echo "Spectra, grads mode:"

cat > gx_outp.inp << EOF
$ WAVEWATCH III Point output post-processing (GrADS)
$ ------------------------------------------
   $t_beg     $dt_grads  $tn
$
@@BUOYID
 -1
$
  T T T T T T
$
$ End of input file
EOF

echo "      Screen ouput routed to $path_log/gx_outp.@@BUOYNAME_spectra_grads.out"
$path_e/gx_outp > $path_log/gx_outp.@@BUOYNAME_spectra_grads.out

if [ $? -ne 0 ] 
then
  echo "ERROR GENERATING POINT OUTPUT FOR POINT @@BUOYNAME"
fi

rm -f gx_outp.inp
gfname=@@BUOYNAME_grads.spectra_$timestr.ww3
echo "      Moving grads spectra ctl file to $path_res/$gfname.ctl"
sed "s/ww3\.spec\.grads/@@BUOYNAME_grads\.spectra_$timestr\.ww3/g" ww3.spec.ctl > $path_res/$gfname.ctl
rm -f ww3.spec.ctl
echo "      Moving grads spectra data file to $path_res/$gfname"
mv ww3.spec.grads $path_res/$gfname
echo "      Moving mean data file to $path_res/@@BUOYNAME_grads_"$timestr".mean.ww3"
mv ww3.mean.grads $path_res/@@BUOYNAME_grads.mean_$timestr.ww3
echo "Spectra grads mode correctly generated"
echo "-------------------------------"

rm mod_def.ww3 out_pnt.ww3
echo "  POINT OUTPUT FOR BUOY @@BUOYNAME correctly generated"
echo '###############################'


  echo ' '
  echo '+--------------------+'
  echo '| Initial conditions |'
  echo '+--------------------+'
  echo ' '

cat > ww3_strt.inp << EOF
$ WAVEWATCH III Initial conditions input file
$ -------------------------------------------
   $itype
$   0.07 0.01  245. 5  -40.  20.  50.  10.  $Hini
$   itype=1 ;
$
$  FreqP='0.16'; FreqSpread='0.02'; MeanDir='225'; Xm=8.7E; Xspr=1; Ym=43.4N; Yspr=0.5; Hs = 3.5   # Set initial swell
  0.16 0.02 225 2 8.7 1 43.4 0.5 3.5
$  0.16 0.02 225 2 8.4 1 43.3 0.5 5.0
EOF

  for grid in ${mods[*]}
  do
    rm -f mod_def.ww3
    ln -s mod_def.$grid mod_def.ww3
    echo "   Running ww3_strt for initial conditions"
    echo "   Screen ouput routed to $path_log/ww3_strt.$grid.out"
    $path_e/ww3_strt > $path_log/ww3_strt.$grid.out
    #debug: /opt/intel/bin/idb $path_e/ww3_strt > $path_res/ww3_strt.$grid.out
    mv restart.ww3 restart.$grid
  done

  rm -f ww3_strt.inp mod_def.ww3
 

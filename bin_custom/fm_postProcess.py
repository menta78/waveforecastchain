from fm_customMeasures import *
import math
 
def computeWindModuleAndDir(wu, wv):
  wind_module = (wu**2 + wv**2)**0.5
  #wsind_dir is computed with respect to north direction. Thus, wv = cos(wind_dir_rad), wu = sin(wind_dir_rad)
  if wu != 0:
    wind_dir_rad = math.atan(wu/wv)
    if wv < 0:
      wind_dir_rad += math.pi
    if wind_dir_rad < 0:
      wind_dir_rad += 2*math.pi
    #pt_masr.wind_dir_rad IS NOW THE WIND PROPAGATION DIRECTION. WE WANT ORIGIN DIRECTION
    #ROTATING IT OF ANOTHER 2PI ANGLE
    wind_dir_rad -= math.pi
    if wind_dir_rad < 0:
      wind_dir_rad += 2*math.pi

    """
    if wind_dir_rad < 0:
      print("################")
      print("wind dir < 0")
      print("wu, wv = {wu}, {wv}".format(wu=wu, wv=wv))
      print("tan(wu/wv) = {t}".format(t=math.atan(wu/wv)))
      print("saved: {a}".format(a=wind_dir_rad))
      print("################")
    """
  else:
    wind_dir_rad = 0
  return wind_module, wind_dir_rad

def computeWaveDir(_dir):
  _dir = - _dir - math.pi/2.
  while _dir < 0:
    _dir += 2*math.pi
  return _dir

cumulationTimeStepsCount = 3  
 
def postProcess(self, points = None, intpMeasures = None, measureDefs = None):
  
  windModuleMsrDef = measureDefs.getByDbColName("wind_module")
  windDirMsrDef = measureDefs.getByDbColName("wind_dir_rad")
  
  cumulatedRainMsrDef = measureDefs.getByDbColName("rain")
  cumulatedSnowMsrDef = measureDefs.getByDbColName("snow")

  meandirMsrDef = measureDefs.getByGradsName("dirmn")
  peakdirMsrDef = measureDefs.getByGradsName("peakd")
  
  for pt in points:
    
    wus = intpMeasures.getMeasures(point = pt, measureDef = measureDefs.getByGradsName("ugrd10m"))
    wvs = intpMeasures.getMeasures(point = pt, measureDef = measureDefs.getByGradsName("vgrd10m"))
    
    rains = intpMeasures.getMeasures(point = pt, measureDef = cumulatedRainMsrDef)
    snows = intpMeasures.getMeasures(point = pt, measureDef = cumulatedSnowMsrDef)
    
    meandirs = intpMeasures.getMeasures(point = pt, measureDef = meandirMsrDef)
    peakdirs = intpMeasures.getMeasures(point = pt, measureDef = peakdirMsrDef)

    tymes = wus.keys()
    tymes.sort()
    for tm, tmIndex in zip(tymes, range(len(tymes))):
      wu = wus[tm]
      wv = wvs[tm]

      wmod, wdir = computeWindModuleAndDir(wu, wv)
      intpMeasures.addMeasure(point = pt, tyme = tm, measureDef = windModuleMsrDef, measure = wmod)
      intpMeasures.addMeasure(point = pt, tyme = tm, measureDef = windDirMsrDef, measure = wdir)
      
      meandir = meandirs[tm]
      meandir = computeWaveDir(meandir)
      peakdir = peakdirs[tm]
      peakdir = computeWaveDir(peakdir)
      intpMeasures.addMeasure(point = pt, tyme = tm, measureDef = meandirMsrDef, measure = meandir)
      intpMeasures.addMeasure(point = pt, tyme = tm, measureDef = peakdirMsrDef, measure = peakdir)

      if tmIndex > cumulationTimeStepsCount:
	tmSubtr = tymes[tmIndex - cumulationTimeStepsCount]
	rainToSubtract = rains[tmSubtr]
	snowToSubtract = snows[tmSubtr]
	cumulatedRain = rains[tm] - rainToSubtract
	cumulatedSnow = snows[tm] - snowToSubtract
        intpMeasures.addMeasure(point = pt, tyme = tm, measureDef = cumulatedRainMsrDef, measure = cumulatedRain)
        intpMeasures.addMeasure(point = pt, tyme = tm, measureDef = cumulatedSnowMsrDef, measure = cumulatedSnow)
    
    for customMeasure in customMeasures:
      customMeasure.compute(point = pt, measureDefs = measureDefs, intpMeasures = intpMeasures)

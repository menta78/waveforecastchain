import types

class measureDef:  
  def __init__(self, gradsName = "", dbColName = "", gradsFile = ""):
    self.dbColName = dbColName
    self.gradsFile = gradsFile
    self.gradsName = gradsName
  
  # some measures might have just gradsName or dbColName
  def key(self):
    return self.gradsName + '_' + self.dbColName
  
#returns a dictionary gradsFile -> [list of measureDef]
def groupDefsByGradsFile(measureDefsList):
  sortedFiles = []
  dictByFile = {}
  for df in measureDefsList:
    gradsFile = df.gradsFile
    msrsList = dictByFile.get(gradsFile, [])
    msrsList.append(df)
    if not gradsFile in dictByFile:
      sortedFiles.append(gradsFile)
    dictByFile[gradsFile] = msrsList
  return sortedFiles, dictByFile    
  
def substPatternInGradsFileNames(measureDefsList, pattern, value):
  for df in measureDefsList:
    df.gradsFile = df.gradsFile.replace(pattern, value)
    
def getByGradsName(measureDefsList, gradsName):
  for df in measureDefsList:
    if df.gradsName == gradsName:
      return df
  return None
  
def getByDbColName(measureDefsList, dbColName):
  for df in measureDefsList:
    if df.dbColName == dbColName:
      return df
  return None

class measureDefsList(list):  
  def __init__(self, lst):
    self.extend(lst)
    self.groupDefsByGradsFile = types.MethodType(groupDefsByGradsFile, self)
    self.substPatternInGradsFileNames = types.MethodType(substPatternInGradsFileNames, self)
    self.getByGradsName = types.MethodType(getByGradsName, self)
    self.getByDbColName = types.MethodType(getByDbColName, self)
    

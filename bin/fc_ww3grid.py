import re

class ww3grid:
  
  def __init__(self, filepath, positivelons = False):
    self.nx = 0
    self.ny = 0
    self.dx = 0
    self.dy = 0
    self.minlon = 0
    self.minlat = 0
    self.positivelons = positivelons
    self.load(filepath)
  
  def maxlon(self):
    return self.minlon + self.dx*self.nx

  def maxlat(self):
    return self.minlat + self.dy*self.ny
    
  def minlonstr(self):
    return "{d:1.6}".format(d=self.minlon)
    
  def maxlonstr(self):
    return "{d:1.6}".format(d=self.maxlon())
    
  def minlatstr(self):
    return "{d:1.6}".format(d=self.minlat)
    
  def maxlatstr(self):
    return "{d:1.6}".format(d=self.maxlat())
    
  def _isUnstructured(self, lines):
    for l in lines:
      if re.match("( *)'UNST' [TF] '([a-zA-Z]*)'( *)(\n?)", l):
        return True
    return False

  def load(self, filepath):
    fl = open(filepath, "r")
    lines = fl.readlines()

    self.isUnstructured = self._isUnstructured(lines)

    griddeflines = filter(lambda s: s.find("GRID_DEFINITION") != -1, lines)
    if griddeflines:
      griddefstr = griddeflines[0].strip(" \n")
      griddef = filter(lambda s: s, griddefstr.split(" "))[-6:]
      
      self.minlon = float(griddef[0])
      maxlon = float(griddef[1])
      self.nx = int(griddef[2])
      self.dx = (maxlon - self.minlon)/self.nx
      
      self.minlat = float(griddef[3])
      maxlat = float(griddef[4])
      self.ny = int(griddef[5])
      self.dy = (maxlat - self.minlat)/self.ny
    elif not self.isUnstructured:
      #parsing min/max lon, min/max lat from ww3 grid definition. Might be affected by errors!!
      lines = filter(lambda s: not s.strip(' ')[0] in ["$", "@", "&"] and not re.match('END OF NAMELISTS', s), lines)[4:8]
      res = re.match("^(?P<nx>(\d+))([ \t]*)(?P<ny>(\d+))([ \t]*)$", lines[1].strip(" "))
      self.nx = int(res.group("nx"))
      self.ny = int(res.group("ny"))
      res = re.match("^(?P<dx>([\d\.]+))([ \t]*)(?P<dy>([\d\.]+))([ \t]*)(?P<fct>([\d\.]+))([ \t]*)$", lines[2].strip(" \n"))
      fct = float(res.group("fct"))
      self.dx = float(res.group("dx"))/fct
      self.dy = float(res.group("dy"))/fct
      res = re.match("^(?P<minlon>([-\d\.]+))([ \t]*)(?P<minlat>([-\d\.]+))([ \t]*)(?P<fct>([-\d\.]+))([ \t]*)$", lines[3].strip(" \n"))
      fct = float(res.group("fct"))
      self.minlon = float(res.group("minlon"))/fct
      if self.positivelons and (self.minlon < 0):
	self.minlon += 360.0
      self.minlat = float(res.group("minlat"))/fct
    elif self.isUnstructured:
      raise Exception('GRID_DEFINITION line not found in file ' + filepath)
    
    

class zipmanager:
  
  def __init__(self, zipfilepath):
    if zipfilepath:
      self.setzipfilepath(zipfilepath)

  def __getsimname(self):
    return self.zipinternaldir
  simname = property(__getsimname)
        
  def setzipfilepath(self, zipfilepath):
    self.zipfilepath = zipfilepath
    pathchunks = self.zipfilepath.split("/")
    self.zipdirectory = "/".join(pathchunks[:-1])
    import zipfile
    fl = zipfile.ZipFile(zipfilepath, allowZip64 = True)
    names = fl.namelist()
    if len(names):
      flname = names[0]
      self.zipinternaldir = flname.split("/")[0]
    
  def zipfile(self, filename):
    #import pdb; pdb.set_trace()
    import os, zipfile    
    archive = zipfile.ZipFile(self.zipfilepath, "a", zipfile.ZIP_DEFLATED, allowZip64 = True)
    relfilename = filename.split("/")[-1]
    relpth = os.path.join(self.zipinternaldir, relfilename)
    archive.write(filename, relpth)
    
  def unzipfile(self, filename):
    import zipfile
    import os
    relfilename = filename.split("/")[-1]
    relpth = os.path.join(self.zipinternaldir, relfilename)
    archive = zipfile.ZipFile(self.zipfilepath, "r", zipfile.ZIP_DEFLATED, allowZip64 = True)
    result = relpth in archive.namelist()
    if result:
      archive.extract(relpth, self.zipdirectory)
    archive.close()
    return result    
    
  def removefiles(self, filenames):
    import zipfile
    import os
    import shutil
    filenames = map(lambda s: os.path.join(self.zipinternaldir, s.split("/")[-1]), filenames)
    zin = zipfile.ZipFile (self.zipfilepath, "r", allowZip64 = True)
    zout = zipfile.ZipFile (self.zipfilepath + "_cpy", "w", allowZip64 = True)
    for item in zin.infolist():
      buffer = zin.read(item.filename)
      if item.filename not in filenames:
        zout.writestr(item, buffer)
    zout.close()
    zin.close()
    os.remove(self.zipfilepath)
    shutil.move(self.zipfilepath + "_cpy", self.zipfilepath)

  
  def zipfiles(self, filenames):
    for fl in filenames:
      self.zipfile(fl)
    
def zipresult(settings):
  zipdirectory(settings.getresultsdir(), settings.getresultsdir() + ".zip")

def zipdirectory(adirectory, adestfile): 
  import os, zipfile
  archivedirname = adirectory.split("/")[-1]
  archive = zipfile.ZipFile(adestfile, "w", zipfile.ZIP_DEFLATED, allowZip64 = True)
  for pth, dirs, files in os.walk(adirectory):
    for fl in files:
      #import pdb; pdb.set_trace()
      abspth = os.path.join(pth, fl)
      relpth = archivedirname + abspth.replace(adirectory, "")
      archive.write(abspth, relpth)
  archive.close()


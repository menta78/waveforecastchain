from fc_settings import *
from fc_swan_settings import *
import fc_swannestpoints_logics

import fc_common
if fc_common.use_custom_settings:
  from fc_custom_settings import *

from math import *
import os

nestPoint = fc_swannestpoints_logics.nestPoint
    
def NestPoints(swnNstSttns = swanNestSettings):
  return fc_swannestpoints_logics.NestPoints(swnNstSttns)

def nestPoints_FileText(nestPoints):
  return fc_swannestpoints_logics.nestPoints_FileText(nestPoints)
  
def nestPoints_OutpFileText(nestPoints, first_index = 1):
  return fc_swannestpoints_logics.nestPoints_OutpFileText(nestPoints, swanNestSettings, first_index)

def check_nests():  
  grids = [main_grid_name]
  grids.extend(zoom_grid_names)
  fc_swannestpoints_logics.check_nests(swanNestSettings, grids)
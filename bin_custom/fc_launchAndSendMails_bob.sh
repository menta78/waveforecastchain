#!/bin/bash
set -e
curdir="/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/bin_custom"
echo "entering current directory"
cd $curdir

echo "launching fc"
cd ../bin
./fc.sh

echo "sending emails"
cd $curdir
python fc_sendMailWithQtys.py > run_sendmail.log 2>&1

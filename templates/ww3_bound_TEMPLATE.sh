
  echo ' '
  echo '+--------------------+'
  echo '| Boundary conditions |'
  echo '+--------------------+'
  echo ' '

cat > ww3_bound.inp << EOF
$ -------------------------------------------------------------------- $                        
$ WAVEWATCH III Boundary input processing
$
$ -------------------------------------------------------------------- $
$ Boundary option: READ or WRITE
$
WRITE
$
$ Interpolation method: 1: nearest
$ 2: linear interpolation
@@BOUND_INTERP_METHOD   
$                                                                                               
$ Verbose mode [0-1]                                                                            
$
1
$ 
$ List of spectra files. These ASCII files use the WAVEWATCH III
$ format as described in the ww3_outp.inp file. The files are
$ defined relative to the directory in which the program is run.
$
$ Examples of such files can be found at (for example):
$ ftp://polar.ncep.noaa.gov/pub/waves/develop/glw.latest_run/
$ (the *.spec.gz files)
$ http://tinyurl.com/iowagaftp/HINDCAST/GLOBAL/2009_ECMWF/SPEC
$
$ If data it used other han from previous WAVEWATCH III runs, then
$ these data will need to be converted to the WAVEWATCH III format. 
$   
$ In the case of NetCDF files see ww3_bounc.inp
$   
@@BOUND_SPEC_SHORT_FILES
'STOPSTRING'
$ 
$
$ -------------------------------------------------------------------- $
$ End of input file
$
$ -------------------------------------------------------------------- $
EOF

  maingrid="@@MAIN_GRID"
  rm -f mod_def.ww3
  ln -s mod_def.$maingrid mod_def.ww3
  @@BOUND_SPEC_LINK_FILES
  echo "   Running ww3_bound for initial conditions"
  echo "   Screen ouput routed to $path_log/ww3_bound.$maingrid.out"
  $path_e/ww3_bound > $path_log/ww3_bound.$maingrid.out
  #debug: /opt/intel/bin/idb $path_e/ww3_strt > $path_res/ww3_strt.$grid.out
  set +e
  mv nest.ww3 nest.$maingrid
  set -e
  @@BOUND_SPEC_REMOVE_FILES

  rm -f ww3_bound.inp mod_def.ww3

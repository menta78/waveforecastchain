"""
this module deals with the conversion of wind data from grib (or any other format) 
to the text needed by wwiii
"""
from abc import *
from collections import namedtuple
import numpy as np
from geodicca import triangleUtils
from geodicca import gribMessagesRetriever
from geodicca import gribUtils
from geodicca.unstructDataInterpolator import unstructDataInterpolator
from fc_ww3grid import ww3grid
from fc_common import fc_exception

gridTypes = ['REG', 'UNST']
windGridSpecTyp = namedtuple('windDataConverterGridSpec', 'gridName gridType gridFile gridUnstMshFile outFile')

class windDataConverterException(Exception):
  pass

class abstractWindDataConverter:
  __metaclass__ = ABCMeta

  def __init__(self, windGridSpec, windGribFile, startDate, endDate, outputFileOpenMode = 'a'):
    if not windGridSpec.gridType in gridTypes:
      raise windDataConverterException('wind grid must be REG or UNST')
    self.windGridSpec = windGridSpec
    self.startDate = startDate
    self.endDate = endDate
    self.outputFileOpenMode = outputFileOpenMode
    self.windGribFile = windGribFile

  @abstractmethod
  def execute(self):
    pass

def windDataConverter(windGridSpec, windGribFile, startDate, endDate):
  if windGridSpec.gridType == 'REG':
    return regDataConverter(windGridSpec, windGribFile, startDate, endDate)
  elif windGridSpec.gridType == 'UNST':
    return unstDataConverter(windGridSpec, windGribFile, startDate, endDate)
  else:
    raise fc_exception('wind grid type must be one of: ' + ', '.join(gridTypes))

class regDataConverter(abstractWindDataConverter):

  def execute(self):
    gridFl = ww3grid(self.windGridSpec.gridFile)
    intpXs = np.linspace(gridFl.minlon, gridFl.maxlon(), gridFl.nx)
    intpYs = np.linspace(gridFl.minlat, gridFl.maxlat(), gridFl.ny)
    intpXs, intpYs = np.meshgrid(intpXs, intpYs)
    intpXs, intpYs = intpXs.flatten(), intpYs.flatten()
    grbRtrvr = gribMessagesRetriever.gribMessagesRetriever(True)
    msgs = grbRtrvr.listMessages(self.windGribFile, self.startDate, self.endDate, ['u10', 'v10'])
    outfl = open(self.windGridSpec.outFile, self.outputFileOpenMode)

    actualDate = None
    for msg in msgs:
      dt = gribUtils.grib2ForecastDateTime(msg)
      if dt != actualDate:
        actualDate = dt
        ln = actualDate.strftime('%Y%m%d %H%M%S') + '\n'
        outfl.write(ln)
        print('elaborating ' + ln.strip('\n'))
      dats = msg.data()
      gribXs = dats[2].flatten()
      gribYs = dats[1].flatten()
      vals = dats[0].flatten()
      interp = unstructDataInterpolator(gribXs, gribYs, gridFl.minlon, gridFl.maxlon(), vals)
      intpvals = interp.execute(intpXs, intpYs)
      for v in intpvals:
        outfl.write(str(v) + '\n') 

    outfl.close()  

class unstDataConverter(abstractWindDataConverter):

  def execute(self):
    gridFl = ww3grid(self.windGridSpec.gridFile)
    mshfl = triangleUtils.mshFile(self.windGridSpec.gridUnstMshFile)
    intpXs, intpYs = mshfl.getX(), mshfl.getY()
    grbRtrvr = gribMessagesRetriever.gribMessagesRetriever(True)
    msgs = grbRtrvr.listMessages(self.windGribFile, self.startDate, self.endDate, ['u10', 'v10'])
    outfl = open(self.windGridSpec.outFile, self.outputFileOpenMode)

    actualDate = None
    for msg in msgs:
      dt = gribUtils.grib2ForecastDateTime(msg)
      if dt != actualDate:
        actualDate = dt
        ln = actualDate.strftime('%Y%m%d %H%M%S') + '\n'
        outfl.write(ln)
        print('elaborating ' + ln.strip('\n'))
      dats = msg.data()
      gribXs = dats[2].flatten()
      gribYs = dats[1].flatten()
      vals = dats[0].flatten()
      interp = unstructDataInterpolator(gribXs, gribYs, gridFl.minlon, gridFl.maxlon(), vals)
      intpvals = interp.execute(intpXs, intpYs)
      for v in intpvals:
        outfl.write(str(v) + '\n') 

    outfl.close()  

  

    




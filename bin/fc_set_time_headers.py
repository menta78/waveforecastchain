def set_time_headers(fileName, outFileName, startTimeString, increment, fieldsCount):
  timeFormat = "%Y%m%d %H%M%S"
  import time
  #import pdb
  #pdb.set_trace()
  tmtpl = time.strptime(startTimeString, timeFormat)
  tm = time.mktime(tmtpl)

  print "opening file " + fileName
  fl = open(fileName, 'r')
  print "reading lines"
  fixedHeader = ""
  outFile = open(outFileName, 'w')

  cnt = 0
  line = fl.readline()
  nlines = 0
  #import pdb; pdb.set_trace()
  while line:
    nlines += 1
    if (not fixedHeader):
      fixedHeader = line
    if (line == fixedHeader):
      if (cnt % fieldsCount == 0):
	timestr = time.strftime(timeFormat, tmtpl)
	line = timestr + "\n"
	outFile.write(line)
	tm += increment
	#tmtpl = time.localtime(tm)
	tmtpl = time.gmtime(tm - time.timezone)
	print("header " + timestr + " inserted")
  #      print("   elaborated " + str(cnt + 1) + " headers")
  #      print("   elaborated " + str(nlines) + " lines")
      cnt += 1
    elif (line.strip()):    
      outFile.write(line)
    line = fl.readline()

  fl.close()

  outFile.close() 

if __name__ == "__main__":
  import sys
  #input file name
  fileName = sys.argv[1]
  #output file name
  outFileName = sys.argv[2]
  #first time header
  startTimeString = sys.argv[3] + " " + sys.argv[4]
  #time increment in seconds
  increment = int(sys.argv[5])
  #number of fields (if fields are zonal and meridional wind speed, this parameter must be set to 2)
  fieldsCount = int(sys.argv[6])
  set_time_headers(fileName, outFileName, startTimeString, increment, fieldsCount)


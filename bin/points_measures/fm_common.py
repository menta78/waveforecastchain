import time

ww3_time_format = "%Y%m%d %H%M%S"
default_timezone = -time.timezone/3600.

def time2str(tm, time_format = ww3_time_format, timezone = default_timezone):
  timezone_s = -3600*timezone
  tmtpl = time.gmtime(tm - timezone_s)
  return time.strftime(time_format, tmtpl)
  
def str2time(timestr, timezone = default_timezone):
  timezone_s = -3600*timezone
  tmtpl = time.strptime(timestr, ww3_time_format)
  return time.mktime(tmtpl) - timezone_s

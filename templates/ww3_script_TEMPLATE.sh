#!/bin/bash

# script generated automatically on @@SCRIPT_CREATION_TIME
# 0. Preparations -----------------------------------------------------------

#this tells sh to exit if some istruction returns a 0 (error) value. Equivalent to $set -o errexit
  set -e

  ww3_env='.wwatch3.env'   # setup file

# 0.a Define model input


#   mods='mediterr'
#  mods='grd1 grd2 grd3 grd4'
   #NR=`echo $mods | wc -w | awk '{print $1}'`
   NR=@@N_GRIDS

# 0.c Set initial conditions

  itype=5                  # Start from calm conditions (if a restart file is not present)

# 0.d Set output. Output parameters are defined at page 37 of handbook. In this case output is only for mean water depth, mean wind speed, significant wave height, peak frequency 

  FIELDS=' @@WWIII_OUT_STR'
  NCDFOUTP='@@NCDFOUTP'
  NCDFTYPE='@@NCDFTYPE'
  GRIBOUTP='@@GRIBOUTP'
  GRADSOUTPUT='@@GRADSOUTP'
  ncrcatcmd='@@NCRCATCMD'

# 0.e Set run times
#     undefined t_rst will give no restart file

@@TIME_HORIZON

  #t_rst='@@TIME_RESTART'
  run_type='@@RUNTYPE'
  wind_input_mode='@@WIND_INPUT_MODE'
  restarts=(@@TIME_RESTART)
  nrestarts=@@NRESTARTS
  simsInterval=@@SIMS_INTERVAL

   fstID=`echo $t_beg | sed 's/ /\./g'`
   rstID=`echo $t_rst | sed 's/ /\./g'`

# 0.f Parallel environment 

   MPI='@@MPI_ENABLED'
   proc=@@MPI_NPROC

# 0.g Set-up variables

  cd
  if [ -f $ww3_env ]
  then
    # if file exists sets main_dir and temp_dir variables reading WWATCH3_DIR and WWATCH3_TMP fields
    set `grep WWATCH3_DIR $ww3_env` ; shift
    main_dir="$*"
    set `grep WWATCH3_TMP $ww3_env` ; shift
    temp_dir="$*"
  else
    echo "*** Set-up file $ww3_env not found ***";
    echo "**************************************";
    echo "CONTINUING THE EXECUTION, but variables custMainDir and custTempDir must be set in the settings file"
    echo "**************************************";
  fi
  cust_temp_dir=@@CUST_TEMP_DIR;
  if [ $cust_temp_dir != "" ]
  then
    temp_dir=$cust_temp_dir;
  fi
  cust_main_dir=@@CUST_MAIN_DIR;
  if [ $cust_main_dir != "" ]
  then
    main_dir=$cust_main_dir
  fi

  path_w="$temp_dir"              # work directory
# path_w="$temp_dir/mww3_case_01" # work directory
  path_e="$main_dir/exe"          # path for executables
  path_a="$main_dir/aux"          # path for aux files and scripts
  path_t="@@FC_DIR"         # path for main simulation directory
  path_d="@@RESTART_DIR"   # path for data directory
  path_i="@@WIND_GRIDS"   # path for data directory
  path_grids="@@WW3_GRIDS"
  path_log="@@LOG_DIR"         # path for output files
  #path_res="$main_dir/forecast_chain/results"   # path for results
  path_res="@@RESULTS_DIR"   # path for results
  path_swannest="@@SWANNESTDIR"
  path_ice="@@ICECOVERINGDIR"
  path_spectralfigs=$path_res
  path_spectralfigs=$path_res
  
  if [ -d $path_ice ]
  then
    iceflag='y'
  else
    iceflag='n'
  fi

# 0.b Define model grids

#WARNING!!!! grid name can be at most 10 charachters long
   mods=("@@MAIN_GRID" @@ZOOM_GRIDS)
   if [ $wind_input_mode != 'SKIP' ]
   then
     if [ $run_type = 'MULTI' ]
     then
       wind=("w_@@MAIN_GRID" @@ZOOM_WIND_GRIDS)
     else
       wind=$mods
     fi
   else
     wind=
   fi
   wind_for_prep=("@@PREP_MAIN_GRID_STR" @@PREP_ZOOM_GRIDS_STR)
   if [ icecoveringflag == 'y' ];
   then 
     ices=("i_@@MAIN_GRID" @@ZOOM_ICE_GRIDS)
     ices_for_prep=("@@PREP_ICE_MAIN_GRID_STR" @@PREP_ICE_ZOOM_GRIDS_STR)
   else 
     ices=()
     ices_for_prep=()
   fi

# 0.h Clean-up

  mkdir -p $path_w
  mkdir -p $path_log
  mkdir -p $path_d
  mkdir -p $path_res
  cd $path_w

  echo ' ' ; echo ' '
  echo '                  ======> WAVEWATCH III SIMULATION OF LIGURIAN SEA <====== '
  echo ' '

# Grid pre-processor -----------------------------------------------------

  echo ' '
  echo '+--------------------+'
  echo '|  Grid preprocessor |'
  echo '+--------------------+'
  echo ' '

  set +e
  rm -f mod_def.*
  rm -f *.ww3
  cp -f $path_i/ww3_grid.inp.* .
  cp -f $path_grids/ww3_grid.inp.* .
  cp -f $path_grids/*.depth_ascii .
  cp -f $path_grids/*.obstr_lev1 .
  cp -f $path_grids/*.mask .
  cp -f $path_grids/*.maskorig_ascii .
  cp -f $path_grids/*.msh .
  if [ $iceflag == 'y' ];
  then
    cp -f $path_ice/* .
  fi
  set -e

  if [ $run_type = 'MULTI' ]
  then
    grids="${mods[*]} ${wind[*]} ${ices[*]}"
  else
    grids=${mods[*]}
  fi
  
  for mod in $grids
  do
    if [ "$mod" != 'no' ]
    then
      mv ww3_grid.inp.$mod ww3_grid.inp

      echo "   Screen ouput routed to $path_log/ww3_grid.$mod.out"
      $path_e/ww3_grid > $path_log/ww3_grid.$mod.out

      rm -f ww3_grid.inp mapsta.ww3 mask.ww3
      echo $mod "done"
      mv mod_def.ww3 mod_def.$mod
    fi
  done

  echo "grids preprocessed"

  rm -f *.depth_ascii *.obstr_lev1 *.maskorig_ascii ww3_grid.inp.*

# Initial conditions -----------------------------------------------------
@@INITIAL_CONDITIONS

# Input fields -----------------------------------------------------------

  echo ' '
  echo '+--------------------+'
  echo '| Input data         |'
  echo '+--------------------+'
  echo ' '
  
  NRI="0"
  if [ $wind_input_mode != 'SKIP' ] 
  then
    if [ $run_type = "MULTI" ]
    then
      wind_len=${#wind[*]}
      NRI="$wind_len"
    else
      wind_len=${#mods[*]}
      NRI="0"
    fi
    #echo "wind_len="$wind_len
    index=0
    while [ "$index" -lt "$wind_len" ]
    do
      wnd=${wind[$index]}
      grid_str=${wind_for_prep[$index]}
      grid_name=${mods[$index]}
      echo "Elaborating wind of grid '$wnd'"
      echo "    using file $path_i/$wnd.raw"
      ln -sf $path_i/$wnd.raw .
cat > ww3_prep.inp.$wnd << EOF
$ WAVEWATCH III Field preprocessor input file
$ -------------------------------------------
$ 'WND' significa che i dati riguardano il vento
$ 'LL' significa che i campi sono definiti su una griglia regolare longitude-latitude
$ 'AI':  Transfer field ’as is’. 
$ T T significa, il tempo è incluso nel file e che il file ha headers.
  'WND' '@@WND_GRID_INPUT_FILE_TYPE' T T
$ longitudine inizio/fine/numero di punti   latitudine inizio/fine/numero di punti
$    -5.5 25.82  246  34. 45.7 130
$grid_str
$ vedi la guida a pag. 80
$ 'NAME': apre il file per nome
$ 4: parametro IDLA, "reads line by line top to bottom, single statement" (vedi pag 80 del manuale)
$ 2: parametro IDFM, "fixed format with above fixed description" (vedi pag 80 del manuale)
$ (I8, I7) è il formato fortran dell'header con la data
$ (16F5.2) è il formato fortran dei dati: in questo caso 16 float con 5 cifre di cui 2 decimali
$  'NAME' 4 2 '(I8,I7)' '(1F6.6)'
  'NAME' 3 2 '(I8,I7)' '(1F6.6)'
$ 20 è lo unit number
$ wind.raw è il nome del file dei dati
    20 '$wnd.raw'
$ End of input file
EOF

      rm -f mod_def.ww3
      ln -s mod_def.$wnd mod_def.ww3
      cp ww3_prep.inp.$wnd ww3_prep.inp
      echo "   Screen ouput routed to $path_log/ww3_prep.$wnd.out"
      $path_e/ww3_prep > $path_log/ww3_prep.$wnd.out
      mv wind.ww3 wind.$wnd
      echo "   Copy wind.$wnd to $path_i"
      cp wind.$wnd $path_i/.
  
      rm -f wind.raw ww3_prep.inp.$wnd ww3_prep.inp mod_def.ww3
      echo ' '
  
      echo ' '
      let "index = $index + 1"
    done
  fi

# input fields: ice covering  

  echo 'input: elaborating ice covering'
  ice_len=${#ices[*]}
  if [ $run_type = 'MULTI']
  then
    let "NRI = $NRI + $ice_len"
  fi
  index=0
  while [ "$index" -lt "$ice_len" ]
  do
    ic=${ices[$index]}
    grid_str=${ices_for_prep[$index]}
    grid_name=${mods[$index]}
    echo "Elaborating ice covering of grid '$ic'"
    icefile=$path_ice/$ic.raw
    if [ ! -f $icefile ];
    then
      echo "    ERROR: FILE $icefile NOT FOUND";
      exit;
    fi
    
    echo "    using file $path_ice/$ic.raw"
    ln -sf $path_ice/$ic.raw .
    
cat > ww3_prep.inp.$ic << EOF
$ WAVEWATCH III Field preprocessor input file
$ -------------------------------------------
$ 'ICE' significa che i dati riguardano la copertura del ghiaccio
$ 'LL' significa che i campi sono definiti su una griglia regolare longitude-latitude
$ T T significa, il tempo è incluso nel file e che il file ha headers.
  'ICE' 'LL' T T
$ longitudine inizio/fine/numero di punti   latitudine inizio/fine/numero di punti
$    -5.5 25.82  246  34. 45.7 130
  $grid_str
$ vedi la guida a pag. 80
$ 'NAME': apre il file per nome
$ 4: parametro IDLA, "reads line by line top to bottom, single statement" (vedi pag 80 del manuale)
$ 2: parametro IDFM, "fixed format with above fixed description" (vedi pag 80 del manuale)
$ (I8, I7) è il formato fortran dell'header con la data
$ (16F5.2) è il formato fortran dei dati: in questo caso 16 float con 5 cifre di cui 2 decimali
$  'NAME' 4 2 '(I8,I7)' '(1F6.6)'
  'NAME' 3 2 '(I8,I7)' '(1F6.6)'
$ 20 è lo unit number
$ wind.raw è il nome del file dei dati
    20 '$ic.raw'
$ End of input file
EOF

    rm -f mod_def.ww3
    ln -s mod_def.$ic mod_def.ww3
    cp ww3_prep.inp.$ic ww3_prep.inp
    echo "   Screen ouput routed to $path_log/ww3_prep.$ic.out"
    $path_e/ww3_prep > $path_log/ww3_prep.$ic.out
    mv ice.ww3 ice.$ic
    echo "   Copy ice.$ic to $path_i"
    cp ice.$ic $path_i/.

    rm -f ice.raw ww3_prep.inp.$ic ww3_prep.inp mod_def.ww3
    echo ' '

    echo ' '
    
    let "index = $index + 1"
  done

# Boundary conditions input if necessary
@@BOUNDARY_INPUT

# Main program -----------------------------------------------------------

  echo ' '
  echo '+--------------------+'
  echo '|    Main program    |'
  echo '+--------------------+'
  echo ' '

  #copying restart files to current directory
  set +e #tells bash that even if ls returns an error scrip should not be quitted
  #FOR USAGE OF THE NEW OBSTRUCTION SYSTEM
  cp -f $path_t/advanced_obstructions/* .
  ##########################################
  if [ "$(ls $path_d/*)" ] 
  then
    cp $path_d/* ./
    #ln -s $path_d/* ./
  fi
  set -e


################################################################
#exit
################################################################



  cd $path_w

  @@MAIN_PROGRAM

  #removing advanced obstruciton files
  set +e
  rm obstructions.*
  set -e
  
  #checking if exists the output for the main grid
  if [ ! -e out_grd.${mods[0]} ];
    then
      echo "something wrong in ww3_multi run";
      exit 0;
  fi

  for grid in ${mods[*]}
  do
    for nrest in `seq 1 $nrestarts`;
    do
      itimestr=$(($nrest - 1));
      timestr=${restarts[$itimestr]};
      #substituting space with _ in timestr
      timestr=${timestr// /_};
      echo "   Output file restart00$nrest.$grid routed to"
      echo "      $path_d/restart.$grid.$timestr"
      set +e
################################################################
#      mv restart00$nrest.$grid $path_d/restart.$grid.$timestr
      cp restart00$nrest.$grid $path_d/restart.$grid.$timestr
################################################################      
      set -e
    done
  done

  echo "   Log files routed to $path_log"
  set +e
  mv log.* $path_log/.

  rm -f ww3_multi.inp
  rm -f restart.*
  rm -f wind.*
  rm -f ice.*

  nr_test=`ls test* 2> /dev/null | wc -w | awk '{ print $1}'`
  if [ "$nr_test" != '0' ]
  then
    for file in `ls test* 2> /dev/null`
    do
      size=`wc -w $file | awk '{print $1}'`
      if [ "$size" = 0 ] ; then
        rm -f $file ; fi
    done
  fi

  nr_test=`ls test* 2> /dev/null | wc -w | awk '{ print $1}'`
  if [ "$nr_test" != '0' ]
  then
    echo "   Output file test[nnn].[m]ww3 routed to $path_log"
    mv test* $path_log/.
  fi

  nr_prof=`ls prf*.mww3 2> /dev/null | wc -w | awk '{ print $1}'`
  if [ "$nr_prof" != '0' ]
  then
    echo "   Profiling file prf.*.mww3 routed to $path_log"
    mv prf*.mww3 $path_log/. 2> /dev/null
  fi

  rm -f prf*.mww3
  echo ' '

# exit 99

# 5. Gridded output ---------------------------------------------------------

  echo ' '
  echo '+--------------------+'
  echo '|   Gridded output   |'
  echo '+--------------------+'
  echo ' '

if [ $NCDFOUTP = "true" ]
then
  cat > ww3_ounf.inp << EOF
$ --------------------------------------------------------------------
$
$ WAVEWATCH III Grid output post-processing ( NETCDF )
$
$ --------------------------------------------------------------------
$
$ Time, time increment and number of outputs.
$
  $t_beg  $dt_grads  $tn_grads
$
$ Request flags identifying fields as in ww3_shel input and
$ section 2.4 of the manual.
$
  N
  $FIELDS
$
$ Output type 4 [3,4] (version netCDF)
$ and variable type 4 [2 = SHORT, 3 = it depends , 4 = REAL]
$ Output type 0 1 2 [0,1,2,3,4,5] (swell partition)
$ variables T [T] or not [F] in the same file
$
$NCDFTYPE 4
0 1 2
T
$
$
$ -------------------------------------------------------------------- $
$ File prefix
$ number of characters in date
$ IX, IY range
ww3.
8
0 2000000 0 2000000
$ --------------------------------------------------------------------
$
$ End of input file
$
$ --------------------------------------------------------------------
EOF

  for mod in ${mods[*]}
  do
    echo "    generating NETCDF output ..."
    echo "      Screen ouput routed to $path_log/ww3_ounf.$mod.out"
    ln -sf mod_def.$mod mod_def.ww3
    ln -sf out_grd.$mod out_grd.ww3

    rm *.nc

    $path_e/ww3_ounf > $path_log/ww3_ounf.$mod.out
    if [ $? -ne 0 ] 
    then
      echo "ERROR GENERATING NETCDF OUTPUT FOR GRID "$mod
    fi
    ncfname="WW3_"$mod"_"`echo $t_beg | cut -f1 -d' '`".nc"
##############################################################
    echo $path_res
    echo $ncfname
##############################################################
    # checking how many nc files have been generated
    nnc=`ls -1 *.nc | wc -w`;
    if [ ! $nnc -eq 1 ];
    then
      # ww3_ounf generated more than one file 
      # merging them with ncrcat
      fls=`ls -1 *.nc`;
      $ncrcatcmd -O $fls ww3.nc; 
      rm $fls;
    fi
    mv *.nc $path_res/$ncfname;
    
    rm mod_def.ww3
    rm out_grd.ww3
  done
fi

if [ $GRIBOUTP = "true" ]
then
  cat > ww3_grib.inp << EOF
$ --------------------------------------------------------------------
$
$ WAVEWATCH III Grid output post-processing ( GRIB )
$
$ --------------------------------------------------------------------
$
$ Time, time increment and number of outputs.
$
  $t_beg  $dt_grads  $tn_grads
$
$ Request flags identifying fields as in ww3_shel input and
$ section 2.4 of the manual.
$
  N
  $FIELDS
$
$ Aditional info needed for grib file
$ Forecast time, center ID, generating process ID, grid definition
$ and GDS/BMS flag
$
$ 80: rome
$ 19: Limited-area Fine Mesh (LFM) analysis
$ 0: latlon
$ 0: mask bitmap is in each record
$ 20040219 000000 80 19 0 0
  $t_beg 80 19 0 0
$
$ --------------------------------------------------------------------
$
$ End of input file
$
$ --------------------------------------------------------------------
EOF

  for mod in ${mods[*]}
  do
    echo "    generating GRIB output ..."
    echo "      Screen ouput routed to $path_log/ww3_grib.$mod.out"
    ln -sf mod_def.$mod mod_def.ww3
    ln -sf out_grd.$mod out_grd.ww3

    $path_e/ww3_grib > $path_log/ww3_grib.$mod.out
    if [ $? -ne 0 ] 
    then
      echo "ERROR GENERATING GRIB OUTPUT FOR GRID "$mod
    fi
    gfname="WW3_"$mod"_"`echo $t_beg | cut -f1 -d' '`".grb2"
    mv gribfile $path_res/$gfname
    
    currpath=`pwd`
    cd $path_res
    g2ctl.pl $gfname > $gfname.ctl
    gribmap -i $gfname.ctl
    cd $currpath
    rm mod_def.ww3
    rm out_grd.ww3
  done
fi

if [ $GRADSOUTPUT = "true" ]
then
  #generating grads output

  cat > gx_outf.inp << EOF
$ WAVEWATCH III Grid output post-processing
$ -----------------------------------------
  $t_beg  $dt_grads  $tn_grads
$
  N
  $FIELDS
$
  0 999 0 999  T F
$
$ End of input file
EOF

  for mod in ${mods[*]}
  do
    echo "   GrADS data for $mod ..."
    echo "      Screen ouput routed to $path_log/gx_outf.$mod.out"
    ln -sf mod_def.$mod mod_def.ww3
    ln -sf out_grd.$mod out_grd.ww3

    $path_e/gx_outf > $path_log/gx_outf.$mod.out
    if [ $? -ne 0 ] 
    then
      echo "ERROR GENERATING GRADS OUTPUT FOR GRID "$mod
    fi

    echo "      ww3.ctl routed to $path_res/$mod.ctl"
    sed "s/ww3\.grads/grads\.$mod/g" ww3.ctl > $path_res/grads.$mod.ctl
    rm -f ww3.ctl
    echo "      ww3.grads routed to $path_res/grads.$mod"
    mv ww3.grads $path_res/grads.$mod

    rm -f mod_def.ww3 out_grd.ww3

  done

fi

# Point output -----------------------------------------------------------

  echo ' '
  echo '+--------------------+'
  echo '|    Point output    |'
  echo '+--------------------+'
  echo ' '

#mod=${mods[1]} #getting only zoom mod


@@POINTOUTPUTS

@@SWANTRANSFERT

@@SPECTRALFIGSWWIIIOUTP

#######################################################################
  echo ' ' ; echo "Cleaning-up `pwd`"
  rm -f  mod_def.*   out_pnt.*   log.*  out_grd.*  *.mask  *.raw  *.inp 
#  rm *.nc
#  echo ' ' ; echo " WARNING!!! It is not cleaning-up `pwd`"
#######################################################################
  echo ' ' ; echo ' '
  echo '                  ======>  END OF WAVEWATCH III  <====== '
  echo '                    ==================================   '
  echo ' '

# End of mww3_case_01 -------------------------------------------------------

#!/usr/bin/python

from datetime import datetime
import os
import sys
import time

wrfFilesPath = '/project/meteo/wrf3.5/WPS/domains/med_hist/out_oper'
restartFilesPath = '/project/meteo/waves/output/restart/'

now = datetime.now()
simstart = datetime(now.year, now.month, now.day)
simstartstring = simstart.strftime('%Y%m%d%H')

logfile = open('scheduledLaunch.log', 'w')
sys.stdout = logfile

def wrfIsReady():

  def findGrb2CtlIdx(filepattern):
    return os.path.isfile(filepattern + '.grb2') and\
           os.path.isfile(filepattern + '.ctl') and\
           os.path.isfile(filepattern + '.grb2.idx')    

  wrffat = os.path.join(wrfFilesPath, 'WRF_FAT_' + simstartstring)
  wrfson = os.path.join(wrfFilesPath, 'WRF_SON_' + simstartstring)
  return findGrb2CtlIdx(wrffat) and findGrb2CtlIdx(wrfson)


while not wrfIsReady():
  now = datetime.now()
  if now.day != simstart.day:
    print('Process could not start in the scheduled day. Expiring.')
    exit()
  print(now.strftime('%Y%m%d%H%M') + ': wrf files not yet ready. Sleeping for 5 minutes')
  sys.stdout.flush()
  time.sleep(60*5)
print('WRF files ready. Launching WWIII')

starttimefile = open(os.path.join(restartFilesPath, 'start_time'), 'w')
starttimefile.write(simstart.strftime('%Y%m%d %H%M%S'))
starttimefile.close()

sys.stdout.flush()

os.system('./waves.sh > waves.log 2>&1 &')


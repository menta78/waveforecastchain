#!/usr/bin/python
# sample call:
#    python fc_event.py "20061204 000000" 9

hindcastServer = 'wavewatch@bob'
hindcastWindDir = '/data02/hindcast/out_cfsr/'
hindcastWindDir = '/data03/hindcast/90s/' #1995 event
hindcastWindDir = '/home/wavewatch/usr/wrf/WPS/domains/med2/out_nest/caseStudiesFatherData/' #2013 03 event
hindcastWindDir = '/data03/hindcast/wrf_operational/caseStudiesFatherData' #20121024 event
hindcastWindDir = '/home/wavewatch/usr/wrf/WPS/domains/med_hist/out_casestudies_tyrrh' #20 km tyrrhenian events
#hindcastWindDir = '/home/wavewatch/usr/wrf/WPS/domains/med2/out_nest/'
#gribfilePattern = 'WRF10_'
gribfilePattern = 'WRF20_'

import sys
import imp
import time
import os
imp.load_source('fc_settings', '../fc_settings_lowres.py')
import fc_settings
import calendar
import shutil

starttimestr = sys.argv[1]
dayscount = int(sys.argv[2])

wind_dir = fc_settings.wind_dir
wind_file = fc_settings.grib_patterns[fc_settings.main_grid_name]
if not os.path.isdir(wind_dir):
  os.makedirs(wind_dir)
gribmap = fc_settings.gribmap

starttmtpl = time.strptime(starttimestr, fc_settings.ww3_time_format)
tm = calendar.timegm(starttmtpl)

print('start time: ' + starttimestr)
print('listing necessary files')

files = []
#looping on the month
#dayscount = 2
print('Days in this month: ' + str(dayscount))
fc_settings.sim_duration = dayscount*86400
fc_settings.simulations_time_interval = dayscount*86400
for d in range(dayscount):
  tmtpl = time.gmtime(tm + d*86400)
  tmstr = time.strftime('%Y%m%d%H', tmtpl)
  flname = gribfilePattern + tmstr
  files.append(flname)

print('copying files from server')
for fl in files:
  gfpath = os.path.join(wind_dir, fl + '.grb')
  #if os.path.isfile(gfpath):
    #continue
  print('copying file ' + fl)
  sys.stdout.flush()
  flpath = os.path.join(hindcastWindDir, fl)
  os.system('scp ' + hindcastServer + ':' + flpath + '\* ' + wind_dir)
  print('regenerating idx file')
  curpth = os.getcwd()
  os.chdir(wind_dir)
  try:
    exitStatus = os.system(gribmap + ' -i ' + fl + '.ctl')
    if exitStatus:
      exitStatus = os.system(gribmap + ' -i ' + fl + '.grb2.ctl')
  finally:
    os.chdir(curpth)

print('all hindcast files successfully downloaded')
print('generating the all-wind grib file')
windFilePath = os.path.join(wind_dir, wind_file)
wgrib2 = fc_settings.wgrib2path + 'wgrib2'
wgribcmd = wgrib2 + ' -match ":anl" -append -grib ' + windFilePath + ' ' + os.path.join(wind_dir, files[0])
for fl in files:
  gribfile = fl + '.grb'
  gfpath = os.path.join(wind_dir, gribfile)

  if not os.path.isfile(gfpath):
    gribfile = gribfile + '2'
    gfpath = gfpath + '2'

  wgribcmd = wgrib2 + ' -match "[UV]GRD:10 m" -not ":anl" -append -grib ' + windFilePath + ' ' + gfpath
  print(wgribcmd)
  os.system(wgribcmd)
  sys.stdout.flush()
  
print("generating ctl and idx files")
os.environ['PATH'] += ":" + fc_settings.wgrib2path
g2ctlcmd = '../bin/g2ctl.pl ' + windFilePath + ' > ' + windFilePath + '.ctl'
os.system(g2ctlcmd)
os.system(gribmap + ' -i ' + windFilePath + '.ctl')
 
print("launching fc")
sys.stdout.flush()
oldcwd = os.getcwd()
os.chdir('../bin')
simDur = str(fc_settings.sim_duration)
cmd = './fc.py -t "' + starttimestr + '" -i ' + simDur + ' -d ' + simDur + ' -n -f ../fc_settings_lowres.py'
print('fc command:')
print(cmd)
sys.stdout.flush()
exitstatus = os.system(cmd) 
if exitstatus:
  print("SOMETHING WRONG WITH FC!!")
os.chdir(oldcwd)

try:
  if not exitstatus:
    shutil.rmtree(wind_dir)
except:
  pass

print("all done")



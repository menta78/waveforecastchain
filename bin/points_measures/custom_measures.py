"""
#measure class prototype
class sample_measure:
  def __init__(self):
    self.name = "samplemeasure"
    self.description = "altezza d'onda"
    
  def varnames(self):
    return ["sample_msr", "sample_msr_risk_level"]
    
  def compute(self, point_vars):
    hs = point_vars.hs
    risklevel = 0
    if hs > 1:
      risklevel = 1
    if hs > 3:
      risklevel = 2
    return [hs, risklevel] 
    
#in order to be evaluated, a measure object must be instantiated and added to measures list
"""

import math

Hmax = 5.
Tmax = 11.
Lmax = 9.8*Tmax**2./(2*math.pi)

S1 = 10./(Hmax*Lmax)
S2 = 10./(Hmax*Tmax)
S3 = 10./(Hmax**2.*Tmax)
S4 = 10./(Hmax*Lmax)**0.5

class I_HL:
  
  def __init__(self):
    self.name = "I_HL"
    self.description = "measures obtainded by wave high-length"
    
  def varnames(self):
    return ["I1", "I4"]
    
  def compute(self, point_vars):
    I1 = min(point_vars.hs*point_vars.meanl*S1, 10)
    I4 = min((point_vars.hs*point_vars.meanl)**0.5*S4, 10)
    return [I1, I4]
   
class I_HT:
  
  def __init__(self):
    self.name = "I_HT"
    self.description = "measures obtainded by wave high-period"
    
  def varnames(self):
    return ["I2", "I3"]
    
  def compute(self, point_vars):
    I2 = min(point_vars.hs*point_vars.meanp*S2, 10)
    I3 = min(point_vars.hs**2.*point_vars.meanp*S3, 10)
    return [I2, I3]
   
class I_Surf:
  
  def __init__(self):
    self.name = "I_Surf"
    self.description = "measures for surfing"
    
  def varnames(self):
    return ["ISurf"]
    
  def compute(self, point_vars):
    H = point_vars.hs
    T = point_vars.meanp
    g = 9.8
    ISurf = H*100.*T*(H*100.+T**2./(g*100.))/(g*250.*259) * 10
    ISurf = max(0, min(ISurf, 10))
    return [ISurf]
   
measures = [I_HL(), I_HT(), I_Surf()]    

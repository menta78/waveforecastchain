from interp_grads import point_vars  
from fm_common import *
from fc_settings import *

from fc_common import *
if use_custom_settings:
  from fc_custom_settings import *
  
def createSqlite3Conn():
  import sqlite3
  return sqlite3.connect(sqlite3_db)

createDbConn = createSqlite3Conn  
  
def create_msr_table(dbConn):
  sqlcommand = "CREATE TABLE MEASURES (SPOT VARCHAR2(128), MSR_TIME VARCHAR2(64)";
  for msr_name in point_vars.get_vars_names():
    sqlcommand += ", " + msr_name + " FLOAT"
  sqlcommand += ")"
  dbConn.execute(sqlcommand)
  dbConn.execute("CREATE INDEX MEASURES_IDX0 ON MEASURES (SPOT, MSR_TIME)")
  
def saveToDb(point_vars_dict, dbConn, recreate_table = True):
  if recreate_table:
    try:
      dbConn.execute("DROP TABLE MEASURES")
    except:
      pass
    create_msr_table(dbConn)
  
  sqlGetTimeZone = "SELECT GMOFFSET FROM SPOTS WHERE SPOT=:SPOT"
  v_names = point_vars.get_vars_names()
  sqlcommand = "INSERT INTO MEASURES (SPOT, MSR_TIME, {fields}) VALUES (:SPOT, :MSR_TIME, :{params})" \
		.format(fields = ", ".join(v_names), params = ", :".join(v_names))
  
  point_ids = point_vars_dict.get_pointsstr()
  for pt_id in point_ids:
    pt_msrs_by_time = point_vars_dict.get_msrs_bytime(pt_id)
    for tm, point_var in pt_msrs_by_time.iteritems():
      qTimeZone = dbConn.execute(sqlGetTimeZone, [pt_id])
      timezone = qTimeZone.fetchone()[0]
      timestr = time2str(tm, timezone = timezone)
      pt_vars = [pt_id, timestr]
      pt_vars.extend(point_var.get_vars_list())
      dbConn.execute(sqlcommand, pt_vars)
  

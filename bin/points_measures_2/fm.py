from fm_interpolator import *
import sqlite3

measureDefsList = None   
sqlite3db = ""
timeStepDelta = 1
postProcess = None
      
def execute_interpolation(timeStepsCount = 0, gradsBin = "", substsMap = {}):
  if not measureDefsList:
    return

  for k in substsMap.keys():
    val = substsMap[k]
    for df in measureDefsList:
      df.gradsFile = df.gradsFile.replace(k, val)
  
  print("connecting to db " + sqlite3db)
  db = sqlite3.connect(sqlite3db)
  print("loading points")
  points = fm_points.points(db) 
  print("interpolating")
  interpolator = msrInterpolator(points = points, measureDefs = measureDefsList, timeStepsCount = timeStepsCount,\
         timeStepDelta = timeStepDelta, db = db, gradsBin = gradsBin, postProcess = postProcess)
  interpolator.execute()
  db.close()
  

import os
from fc_settingsLoader import settings
import fc_common
      
def reviewEnvironPath():
  gribmapPath = os.path.dirname(settings.gribmap)
  g2ctlPath = os.path.dirname(os.path.abspath(__file__))
  wgrib2pth = os.path.dirname(settings.wgrib2path)
  os.environ["PATH"] = ":".join([g2ctlPath, wgrib2pth, gribmapPath, os.environ["PATH"]])
  
def getGribOutput():
  try:
    return settings.gribOutput 
  except:
    return False
    
def getGradsOutput():
  try:
    return settings.gradsOutput
  except:
    return False

def getNetcdfOutput():
  try:
    return settings.netcdfOutput
  except:
    return True

def getNetcdfType():
  try:
    return str(settings.netcdfType)
  except:
    return '3'
    
def getArchiveType():
  try:
    return settings.archiveType
  except:
    return fc_common.zipArchive
    


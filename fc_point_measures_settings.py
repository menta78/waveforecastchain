from points_measures_2 import fm
from points_measures_2.fm_measureDefs import *
import os
modulepath = os.path.dirname(__file__)
import sys
sys.path.append(modulepath)
from fc_settings import *
from bin_custom import fm_postProcess, fm_customMeasures

fm.sqlite3db = basedir + "/results/db/places2.db"
fm.timeDelta = 1
fm.postProcess = fm_postProcess.postProcess

#waveMainFile = "grads.mediterr.ctl"
#waveZoomFile = "grads.liguria.ctl"
waveMainFile = "WW3_mediterr_@@FIRSTTIME.grb2.ctl"
waveZoomFile = "WW3_liguria_@@FIRSTTIME.grb2.ctl"
meteoMainFile = wind_dir + "/WRF_FAT_@@FIRSTTIME.ctl"
meteoZoomFile = wind_dir + "/WRF_SON_@@FIRSTTIME.ctl"

fm.measureDefsList = measureDefsList([
# measureDef("hs", "hs", waveMainFile),
# measureDef("tmn", "meanp", waveMainFile),
# measureDef("peakp", "peakp", waveMainFile),
# measureDef("dirmn", "meand", waveMainFile),
# measureDef("peakd", "peakd", waveMainFile),
# measureDef("lmn", "meanl", waveMainFile),

# measureDef("hs", "hs", waveZoomFile),
# measureDef("tmn", "meanp", waveZoomFile),
# measureDef("peakp", "peakp", waveZoomFile),
# measureDef("dirmn", "meand", waveZoomFile),
# measureDef("peakd", "peakd", waveZoomFile),
# measureDef("lmn", "meanl", waveZoomFile),

  measureDef("HTSGWSFC", "hs", waveMainFile),
  measureDef("MWSPERSFC", "meanp", waveMainFile),
  measureDef("PERPWSFC", "peakp", waveMainFile),
  measureDef("WWSDIRSFC", "meand", waveMainFile),
  measureDef("DIRPWSFC", "peakd", waveMainFile),
  measureDef("WLENSFC", "meanl", waveMainFile),

  measureDef("HTSGWSFC", "hs", waveZoomFile),
  measureDef("MWSPERSFC", "meanp", waveZoomFile),
  measureDef("PERPWSFC", "peakp", waveZoomFile),
  measureDef("WWSDIRSFC", "meand", waveZoomFile),
  measureDef("DIRPWSFC", "peakd", waveZoomFile),
  measureDef("WLENSFC", "meanl", waveZoomFile),
  
  measureDef("ugrd10m", "", meteoMainFile),
  measureDef("vgrd10m", "", meteoMainFile),
  measureDef("tcdcclm", "cldness", meteoMainFile),
  measureDef("tmp2m", "temperature", meteoMainFile),
  measureDef("apcpsfc", "rain", meteoMainFile),
  measureDef("snodsfc", "snow", meteoMainFile),
  measureDef("prmslmsl", "pressure", meteoMainFile),
  measureDef("msletmsl", "MSLETMSL", meteoMainFile),
  measureDef("gustsfc", "GUSTSFC", meteoMainFile),
  measureDef("rhprs", "RHPRS", meteoMainFile),
  
  measureDef("ugrd10m", "", meteoZoomFile),
  measureDef("vgrd10m", "", meteoZoomFile),
  measureDef("tcdcclm", "cldness", meteoZoomFile),
  measureDef("tmp2m", "temperature", meteoZoomFile),
  measureDef("apcpsfc", "rain", meteoZoomFile),
  measureDef("snodsfc", "snow", meteoZoomFile),
  measureDef("prmslmsl", "pressure", meteoZoomFile),
  measureDef("msletmsl", "MSLETMSL", meteoZoomFile),
  measureDef("gustsfc", "GUSTSFC", meteoZoomFile),
  measureDef("rhprs", "RHPRS", meteoZoomFile),
  
  #postprocessed measures
  measureDef("", "wind_module", ""),
  measureDef("", "wind_dir_rad", ""),
  measureDef("", "I1", ""),
  measureDef("", "I2", ""),
  measureDef("", "I3", ""),
  measureDef("", "I4", ""),
  measureDef("", "ISurf", ""),
  ])
fm_customMeasures.addCustomMeasuresToMsrDefs(fm.measureDefsList)    

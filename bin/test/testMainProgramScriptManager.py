import unittest
import re
from fc_common import fc_exception
from fc_mainProgramScriptManager import *

class mockSettings:
  pass

class testMainProgramScriptManger(unittest.TestCase):

  def _getSettings(self):
    stts = mockSettings()
    stts.main_grid_name = 'testgrid'
    stts.zoom_grid_names = ['zoomGrid1', 'zoomGrid2']
    stts.mpi_command = 'mpi'
    stts.omp_num_threads = 16
    pointText = """   0.0       0.0       "FAKE_PT_0"
   1.0       1.0       "FAKE_PT_1"
"""
    return stts, pointText

  def testMainProgramScriptManager(self):
    stts, pointText = self._getSettings()
    sm = mainProgramScriptManager('SHEL', stts, pointText)
    self.assertEqual(ww3ShellScriptManager, sm.__class__)
    sm = mainProgramScriptManager('MULTI', stts, pointText)
    self.assertEqual(ww3MultiScriptManager, sm.__class__)
    self.failUnlessRaises(fc_exception, mainProgramScriptManager, 'FOO', stts, pointText)

  def testWw3MultiScriptManager(self):
    stts, pointText = self._getSettings()
    sm = mainProgramScriptManager('MULTI', stts, pointText)
    scrpttext = sm.generateScript()
    self.assertTrue(scrpttext)
    self.assertTrue(re.search(pointText, scrpttext))
    self.assertFalse(re.search('@@', scrpttext))
    self.assertEqual(wwiiiMultiScrpt.strip(' \n'), scrpttext.strip(' \n'))

  def testWw3ShelScriptManager(self):
    stts, pointText = self._getSettings()
    sm = mainProgramScriptManager('SHEL', stts, pointText)
    scrpttext = sm.generateScript()
    self.assertTrue(scrpttext)
    self.assertTrue(re.search(pointText, scrpttext))
    self.assertFalse(re.search('@@', scrpttext))
    self.assertEqual(wwiiiShelScrpt.strip(' \n'), scrpttext.strip(' \n'))

wwiiiMultiScrpt = """cat > ww3_multi.inp << EOF
$ WAVEWATCH III multi-grid input file
$ ------------------------------------
  $NR $NRI F 1 T T
$
$ for each grid: a defining line for example:
$ 'gridname' 'no' 'no' 'wind_grid' 'no' 'no' 'no' 'no' 1 1  0.00 1.00  F
$ first is gridname the next 7 words are grids of forcing fields (3rd is wind)    
$ 0.0 1.00 means that this grid uses all the processors. For example 0.00 0.10 means that this grid can use only one tenth of the computation power.
EOF
  echo "'w_testgrid'  F F T F F F F"   >> ww3_multi.inp ;
  testgrid

  echo "'testgrid' 'no' 'no' 'w_testgrid' 'no' 'no' 'no' 'no' 1 1  0.00 1.00  F" >> ww3_multi.inp
  echo "'zoomGrid1' 'no' 'no' 'w_zoomGrid1' 'no' 'no' 'no' 'no' 2 1  0.00 1.00  F"   >> ww3_multi.inp
echo "'zoomGrid2' 'no' 'no' 'w_zoomGrid2' 'no' 'no' 'no' 'no' 2 1  0.00 1.00  F"   >> ww3_multi.inp


  if [ iceflag == 'y' ];
  then
    echo "'testgrid' 'no' 'no' 'no' 'i_testgrid' 'no' 'no' 'no' 1 1  0.00 1.00  F" >> ww3_multi.inp
    echo "'zoomGrid1' 'no' 'no' 'no' 'i_zoomGrid1' 'no' 'no' 'no' 2 1  0.00 1.00  F"   >> ww3_multi.inp
echo "'zoomGrid2' 'no' 'no' 'no' 'i_zoomGrid2' 'no' 'no' 'no' 2 1  0.00 1.00  F"   >> ww3_multi.inp

  fi

cat >> ww3_multi.inp << EOF
$
   $t_beg  $t_end
$
   T  T
$
   $t_beg  $dt  $t_end
   N
   $FIELDS
   $t_beg     $dt  $t_end
   0.0       0.0       "FAKE_PT_0"
   1.0       1.0       "FAKE_PT_1"
   0.0       0.0       "STOPSTRING"
   $t_beg      0  $t_end
EOF

#inserting restart file informations
t_rst_start=${restarts[0]};
t_rst_end=${restarts[$(($nrestarts - 1))]};
echo "   $t_rst_start      $simsInterval  $t_rst_end" >> ww3_multi.inp

cat >> ww3_multi.inp << EOF
   $t_beg      0  $t_end
   $t_beg      0  $t_end
$
  'the_end'  0
$
  'STP'
$
$ End of input file
EOF

  echo "   Running multi-grid model ..."
  echo "   Screen output routed to $path_log/ww3_multi.out"

  cd $path_w

  #this is to prevent some problem in mpi exit to cause the whole process to crash
  set +e
  if [ "$MPI" = 'yes' ]
  then
    mpi -np $proc $path_e/ww3_multi  > $path_log/ww3_multi.out
#   mpi -np $proc -mca orte_tmpdir_base /tmp/openmpi_ww3 $path_e/ww3_multi > $path_log/ww3_multi.out
#   poe hpmcount $path_e/ww3_multi # > $path_log/ww3_multi.out
#   poe $path_e/ww3_multi # > $path_log/ww3_multi.out
  else
    export OMP_NUM_THREADS=16
    $path_e/ww3_multi > $path_log/ww3_multi.out
  fi
  set -e
"""

wwiiiShelScrpt="""ln -sf mod_def.testgrid mod_def.ww3
ln -sf restart.testgrid restart.ww3
ln -sf wind.testgrid wind.ww3

cat > ww3_shel.inp << EOF
$ WAVEWATCH III ww3_shel input file
$ ------------------------------------
$ Define input to be used with flag for use and flag for definition
$ as a homogeneous field (first three only); eight input lines.
$ For example line "T F  Winds" means that winds are considered 
$ and that they are not a homogeneous field
F F  Water levels
F F  Currents
T F  Winds
F    Ice concentrations
F    Assimilation data : Mean parameters
F    Assimilation data : 1-D spectra
F    Assimilation data : 2-D spectra.
$
$ Start date
   $t_beg  
$ End date
   $t_end
$ 
$ Define output server mode. This is used only in the parallel version
$ of the model. To keep the input file consistent, it is always needed.
$ IOSTYP = 1 is generally recommended. IOSTYP > 2 may be more efficient
$ for massively parallel computations. Only IOSTYP = 0 requires a true
$ parallel file system like GPFS.
$
$ IOSTYP = 0 : No data server processes, direct access output from
$ each process (requirese true parallel file system).
$ 1 : No data server process. All output for each type
$ performed by process that performes computations too.
$ 2 : Last process is reserved for all output, and does no
$ computing.
$ 3 : Multiple dedicated output processes.
$
   1
$
   $t_beg  $dt  $t_end
   N
   $FIELDS
   $t_beg     $dt  $t_end
   0.0       0.0       "FAKE_PT_0"
   1.0       1.0       "FAKE_PT_1"
   0.0       0.0       "STOPSTRING"
   $t_beg      0  $t_end
EOF

#inserting restart file informations
t_rst_start=${restarts[0]};
t_rst_end=${restarts[$(($nrestarts - 1))]};
echo "   $t_rst_start      $simsInterval  $t_rst_end" >> ww3_shel.inp

cat >> ww3_shel.inp << EOF
   $t_beg      0  $t_end
   $t_beg      0  $t_end
$
  'the_end'  0
$
  'STP'
$
$ End of input file
EOF

  echo "   Running ww3_shel model ..."
  echo "   Screen output routed to $path_log/ww3_shel.out"

  cd $path_w

  #this is to prevent some problem in mpi exit to cause the whole process to crash
  set +e
  if [ "$MPI" = 'yes' ]
  then
    mpi -np $proc $path_e/ww3_shel  > $path_log/ww3_shel.out
  else
    export OMP_NUM_THREADS=16
    $path_e/ww3_shel > $path_log/ww3_shel.out
  fi
  set -e

rm *.ww3
rm ww3_shel.inp
"""

if __name__ == '__main__':
  unittest.main()

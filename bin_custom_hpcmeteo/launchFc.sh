#!/bin/bash

binpath='/project/meteo/waves/usr/wavewatch/waveforecastchain_v3.14/bin/'
lastrespath='/project/meteo/waves/output/lastResults/'
logpath='/project/meteo/waves/usr/wavewatch/log/'

pythonpath='/project/meteo/waves/usr/python/bin/python'

pwd=$PWD
cd $binpath

logfile=$logpath"run_`date +%Y%m%d`.log"
rm -f $logfile
echo "launching fc.py"
$pythonpath ./fc.py > $logfile 2>&1

#renaming last results ctl so that they have always the same name
cd $lastrespath
ls * | sed 's/\(WW3_[a-z]\+\)\(_[0-9]\+\)\(\.grb2\.ctl\)/mv & \1\3/' | sh

cd $pwd


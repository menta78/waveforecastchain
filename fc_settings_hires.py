main_time_step = 1800 # 1/2 hours
wind_time_step = 3600 # 1 hours
grads_time_step = 3600 # 1 hour

ww3_time_format = "%Y%m%d %H%M%S"

wind_file_time_format = "%Y%m%d%H"
main_grid_name = "mediterr"
#zoom_grid_names = ["liguria"]
zoom_grid_names = ["tyrrhen"]

log_keep_days = 30

loop_time_start = "20130107 000000"

#operative
skip_extra_output = True

#archive generation
#restartdir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/restart_archive_generation"
#skip_extra_output = True

#rescue
#loop_time_start = "20120320 000000"
#restartdir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/restart_rescue"
#skip_extra_output = True


## SETTINGS FOR THE UNIVERSITY 
mpi_enabled = False
mpi_nproc = 4
mpi_command = "/usr/lib64/openmpi/bin/mpirun"

omp_num_threads = 4

userdir = '/home/wavewatch'
fcdatabasedir = userdir + '/usr/wavewatch/v3.14/fc/hires'
tmpdir = userdir + '/tmp/tmp_hires'
basedir = '/vhe/naswavewatch/volume1/wavewatch/wavewatch/forecast_chain'
wind_dir = userdir + '/tmp/tmp_hires/wind'
wind_grids_dir = basedir + '/winds'
ww3_grids_dir = basedir + '/grids'
custTempDir = tmpdir + '/computation'
custMainDir = fcdatabasedir + '/maindir'
ww3_script_path = tmpdir + '/ww3_script'

results_dir = "/vhe/naswavewatch/volume1/wavewatch/wavewatch/hires_mesoscale/WIP_bidinest"
archive_dir = results_dir

log_dir = fcdatabasedir + "/log"

# THOSE SETTINGS ARE NOT IMPORTANT HERE: VARIABLE grib_patterns IS USED INSTEAD
main_input_file_pattern = "WRF_FAT_@@FIRSTTIME.ctl$"
zoom_input_file_patterns = [main_input_file_pattern]

#sim_duration = 3*86400 #72 hours
zoom_sim_duration = 20*86400 #48 hours
#sim_duration = 86400 #24 hours
#sim_duration = 86400/2 #12 hours
simulations_time_interval = 86400
#simulations_time_interval = 86400/2

sqlite3_db = basedir + "/results/db/places.db"
points_interpolation_processes = 24

u_var_name = "ugrd10m"
v_var_name = "vgrd10m"

interpolate_by_wgrib = True
grib_patterns = {main_grid_name: "wind.grb", zoom_grid_names[0]: "wind_son.grb"}
wgrib2path = userdir + "/usr/wavewatch/wgrib2_ipolates/"
gribmap = "/home/wavewatch/usr/wrf/build/grads-2.0.2/bin/gribmap"
gradsBin = "/home/wavewatch/usr/wrf/build/grads-2.0.2/bin/grads"

restartdir = fcdatabasedir + "/restart"
swanNestDataDir = fcdatabasedir + "/swannest"

nrestarts = 1
restart_files_keep_days = 1

#wwiiiOutputString = 'T F T F F  T T T T T  T T F F F  F F F F F  F F F F F  F F F F F  F'
wwiiiOutputString = 'T F T F F  T T T T T  T T T T T  T T T T F  F T F F F  F F F F F  F'

gribOutput = True









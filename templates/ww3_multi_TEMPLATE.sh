cat > ww3_multi.inp << EOF
$ WAVEWATCH III multi-grid input file
$ ------------------------------------
  $NR $NRI F 1 T T
$
$ for each grid: a defining line for example:
$ 'gridname' 'no' 'no' 'wind_grid' 'no' 'no' 'no' 'no' 1 1  0.00 1.00  F
$ first is gridname the next 7 words are grids of forcing fields (3rd is wind)    
$ 0.0 1.00 means that this grid uses all the processors. For example 0.00 0.10 means that this grid can use only one tenth of the computation power.
EOF
  if [ $wind_input_mode != 'SKIP' ]
  then
    echo "'w_@@MAIN_GRID'  F F T F F F F"   >> ww3_multi.inp ;
    @@ZOOM_WW3_MULTI_1_GRIDS
    echo "'@@MAIN_GRID' 'no' 'no' 'w_@@MAIN_GRID' 'no' 'no' 'no' 'no' 1 1  0.00 1.00  F" >> ww3_multi.inp
  else
    echo "'@@MAIN_GRID' 'no' 'no' 'no' 'no' 'no' 'no' 'no' 1 1  0.00 1.00  F" >> ww3_multi.inp
  fi
  @@ZOOM_WW3_MULTI_2_GRIDS

  if [ iceflag == 'y' ];
  then
    echo "'@@MAIN_GRID' 'no' 'no' 'no' 'i_@@MAIN_GRID' 'no' 'no' 'no' 1 1  0.00 1.00  F" >> ww3_multi.inp
    @@ZOOM_WW3_MULTI_3_GRIDS
  fi

cat >> ww3_multi.inp << EOF
$
   $t_beg  $t_end
$
   T  T
$
   $t_beg  $dt  $t_end
   N
   $FIELDS
   $t_beg     $dt  $t_end
@@POINTS
   0.0       0.0       "STOPSTRING"
   $t_beg      0  $t_end
EOF

#inserting restart file informations
t_rst_start=${restarts[0]};
t_rst_end=${restarts[$(($nrestarts - 1))]};
echo "   $t_rst_start      $simsInterval  $t_rst_end" >> ww3_multi.inp

cat >> ww3_multi.inp << EOF
   $t_beg      0  $t_end
   $t_beg      0  $t_end
$
  'the_end'  0
$
  'STP'
$
$ End of input file
EOF

  echo "   Running multi-grid model ..."
  echo "   Screen output routed to $path_log/ww3_multi.out"

  cd $path_w

  #this is to prevent some problem in mpi exit to cause the whole process to crash
  set +e
  if [ "$MPI" = 'yes' ]
  then
    @@MPI_COMMAND -np $proc $path_e/ww3_multi  > $path_log/ww3_multi.out 2>&1 & echo "running ww3_multi ..."
    wait
    echo "ww3_multi run complete"
#   @@MPI_COMMAND -np $proc -mca orte_tmpdir_base /tmp/openmpi_ww3 $path_e/ww3_multi > $path_log/ww3_multi.out
#   poe hpmcount $path_e/ww3_multi # > $path_log/ww3_multi.out
#   poe $path_e/ww3_multi # > $path_log/ww3_multi.out
  else
    export OMP_NUM_THREADS=@@OMP_NUM_THREADS
    export OMP_STACKSIZE=@@OMP_STACKSIZE
    $path_e/ww3_multi > $path_log/ww3_multi.out
  fi
  set -e



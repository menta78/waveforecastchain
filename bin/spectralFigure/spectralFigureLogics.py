import os

class point:
  def __init__(self, index, x, y):
    self.x = x
    self.y = y
    self.index = index

class spectralFigure:
  def __init__(self, name, gridname):
    self.name = name
    self.gridname = gridname
    self.points = []
  
  def getPoints(self):
    return self.points
    
sfOutTypOneFile = 'onefile'
sfOutTypOneFilePerPoint = 'onefileperpoint'

class spectralFigureScriptGenerator:  

  def __init__(self, outputType = sfOutTypOneFile, outputDt = 3600*3):
    self.outputType = outputType
    self.outputDt = outputDt
 
  def _getTemplateLines(self):
    dirname = os.path.dirname(__file__)
    if self.outputType == sfOutTypOneFile:
      templateFileName = os.path.join(dirname, "./ww3_script_SPECTRALFIGURE_ONEFILE_TEMPLATE")
    elif self.outputType == sfOutTypOneFilePerPoint:
      templateFileName = os.path.join(dirname, "./ww3_script_SPECTRALFIGURE_ONEFILEPERPOINT_TEMPLATE")
    fl = open(templateFileName)
    lines = fl.readlines()
    fl.close()
    return lines

  def _getWw3MultiPointStr(self, pt):
    lonstr = str(pt.x).ljust(9)[0:9]
    latstr = str(pt.y).ljust(9)[0:9]
    name = ("\"SQFIG_" + str(pt.index)).ljust(11) + "\""
    rslt = "   " + "   ".join([lonstr, latstr, name]) + "\n"
    return rslt
    
  def _getWw3outpPointsNewLineStr(self, squareConf):
    rslt = ''
    pts = squareConf.points
    rslt = '\n'.join(str(pt.index) for pt in pts)
    return rslt.strip('\n')
    
  def _getWw3outpPointsStr(self, squareConf):
    rslt = ''
    pts = squareConf.points
    rslt = ' '.join(str(pt.index) for pt in pts)
    return rslt.strip(' ')
    
  def getWw3OutpScriptLines(self, squareConf):
    scrptLines = self._getTemplateLines()
    scrptLines = map(lambda s: s.replace("@@SPECTRALFIGURENAME", squareConf.name), scrptLines)
    scrptLines = map(lambda s: s.replace("@@GRIDNAME", squareConf.gridname), scrptLines)
    scrptLines = map(lambda s: s.replace("@@SPECTRALFIGUREPOINTS", self._getWw3outpPointsStr(squareConf)), scrptLines)
    scrptLines = map(lambda s: s.replace("@@SPECTRALFIGURE_DT", '{dt:5.0f}'.format(dt = self.outputDt)), scrptLines)
    scrptLines = map(lambda s: s.replace("@@SPECTRALFIGURE_NEWLINE_POINTS", self._getWw3outpPointsNewLineStr(squareConf)), scrptLines)
    return scrptLines
    
  def getWw3MultiScriptLines(self, squareConf):
    rslt = '$ points for spectral figure ' + squareConf.name + '\n'
    pts = squareConf.points
    for pt in pts:
      rslt += self._getWw3MultiPointStr(pt)
    rslt = rslt.strip(' \n\r')
    return rslt
    
class wholeScriptGenerator:
  
  def getWw3OutpScriptLines(self, squareConfs, outputType, outputDt):
    scrptLns = []
    sqScrptGen = spectralFigureScriptGenerator(outputType, outputDt)
    for squareConf in squareConfs:
      lns = sqScrptGen.getWw3OutpScriptLines(squareConf)
      scrptLns.extend(lns)
    return scrptLns
      
  def getWw3MultiScriptLines(self, squareConfs, outputType, outputDt):
    scrptLns = []
    sqScrptGen = spectralFigureScriptGenerator(outputType, outputDt)
    for squareConf in squareConfs:
      lns = sqScrptGen.getWw3MultiScriptLines(squareConf)
      scrptLns.extend(lns)
    return scrptLns
  

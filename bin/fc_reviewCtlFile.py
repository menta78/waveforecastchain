from fc_settingsLoader import settings
  
import re
import os

def reviewCtlFiles(resultsDir, isGrib2FileName = False):
  try:
    namesDict = ctlNamesMapping
  except:
    print("Variable fc_settings.ctlNamesMapping is not defined. Review of output ctl files won't take place")
    return
  grids = [settings.main_grid_name]
  grids.extend(settings.zoom_grid_names)
  for grd in grids:
    ctlFile = getGrib2CtlFileName(resultsDir, grd) if isGrib2FileName else getCtlFileName(resultsDir, grd)
    reviewCtlFile(ctlFile, namesDict)
  
def getCtlFileName(fileDirectory, gridname):
  flName = "grads." + gridname + ".ctl"
  return os.path.join(fileDirectory, flName)
  
def getGrib2CtlFileName(fileDirectory, gridname):
  flPattern = "WW3_" + gridname + "_([0-9]*).grb2.ctl"
  fls = [f for f in os.listdir(fileDirectory) if re.match(flPattern, f)]
  assert len(fls) == 1
  return os.path.join(fileDirectory, fls[0])

def reviewCtlFile(fileName, namesDict):    
  f = open(fileName, "r")
  lines = f.readlines()
  f.close()
  
  lines = reviewCtlFileLines(lines, namesDict)
  
  f = open(fileName, "w")
  f.writelines(lines)
  f.close()
  
def reviewCtlFileLines(fileLines, namesDict):
  """
  >>> fileLines = ["HS      0  99  Wave height (m)", "PEAKP   0  99  Peak Per. (s)"]
  >>> namesDict = {"HS": "SWH", "PEAKP": "PP1D"}
  >>> fileLines = reviewCtlFileLines(fileLines, namesDict)
  >>> fileLines[0]
  'SWH      0  99  Wave height (m)'
  >>> fileLines[1]
  'PP1D   0  99  Peak Per. (s)'
  """
  names = namesDict.keys()
  for name in names:
    newname = namesDict[name]
    fileLines = map(lambda s: re.sub(name, newname, s), fileLines)
  return fileLines

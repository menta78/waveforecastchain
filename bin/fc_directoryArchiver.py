import os
import re
import shutil

class dirArchiver:
  
  def __init__(self, filesDir, archiveDir):
    self.filesDir = filesDir
    self.archiveDir = archiveDir
    self.gribSubdir = 'grib'
    self.netcdfSubdir = 'nc'
    self.buoyAvgParamsSubdir = 'buoyAvgParams'
    self.buoyGradsSpectraSubdir = 'buoyGradsSpectra'
    self.buoyTFSpectraSubdir = 'buoyTFSpectra'
    
  def doArchive(self):
    gribDir = os.path.join(self.archiveDir, self.gribSubdir)
    ncDir = os.path.join(self.archiveDir, self.netcdfSubdir)
    buoyAvgParamsDir = os.path.join(self.archiveDir, self.buoyAvgParamsSubdir)
    buoyGradsSpectraDir = os.path.join(self.archiveDir, self.buoyGradsSpectraSubdir)
    buoyTFSpectraDir = os.path.join(self.archiveDir, self.buoyTFSpectraSubdir)
    
    #moving grib files
    print("archiving grib files")
    files = [f for f in os.listdir(self.filesDir) if re.search("grb2", f)]
    if len(files):
      if not os.path.isdir(gribDir):
        os.mkdir(gribDir)
      for f in files:
        fpath = os.path.join(self.filesDir, f)
        destfpath = os.path.join(gribDir, f)
        shutil.move(fpath, destfpath)

    print("archiving nc files")
    files = [f for f in os.listdir(self.filesDir) if re.search("nc", f)]
    if len(files):
      if not os.path.isdir(ncDir):
        os.mkdir(ncDir)
      for f in files:
        fpath = os.path.join(self.filesDir, f)
        destfpath = os.path.join(ncDir, f)
        shutil.move(fpath, destfpath)
      
    #moving avg params
    print("archiving avg wave param files")
    if not os.path.isdir(buoyAvgParamsDir):
      os.mkdir(buoyAvgParamsDir)
    files = [f for f in os.listdir(self.filesDir) if re.search("wave_params", f)]
    for f in files:
      fpath = os.path.join(self.filesDir, f)
      destfpath = os.path.join(buoyAvgParamsDir, f)
      shutil.move(fpath, destfpath)
    
    #moving grads buoy spectra  
    print("archiving grads buoy spectra")
    if not os.path.isdir(buoyGradsSpectraDir):
      os.mkdir(buoyGradsSpectraDir)
    files = [f for f in os.listdir(self.filesDir) if re.search("grads.spectra", f)]
    for f in files:
      fpath = os.path.join(self.filesDir, f)
      destfpath = os.path.join(buoyGradsSpectraDir, f)
      shutil.move(fpath, destfpath)
      
    #moving transfer file buoy spectra
    print("archiving transfer file buoy spectra")
    if not os.path.isdir(buoyTFSpectraDir):
      os.mkdir(buoyTFSpectraDir)
    files = [f for f in os.listdir(self.filesDir) if re.search("spectra_(.*)\.spc", f)]
    for f in files:
      fpath = os.path.join(self.filesDir, f)
      destfpath = os.path.join(buoyTFSpectraDir, f)
      shutil.copy(fpath, destfpath)
    
    print("all done with archiving")

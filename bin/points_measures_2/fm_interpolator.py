import time
import types
import grads
import traceback
from numpy import array, ma
import matplotlib
matplotlib.use('Agg')
import fm_points
from fm_interpolatedMeasures import *
from fm_measureDefs import *
from fm_common import *
  
def get_current_grads_time(g):
  tmpl = time.strptime(g.coords().time[0], "%HZ%d%b%Y")
  return time.mktime(tmpl)
    
class msrInterpolator:
  
  def __init__(self, points = None, measureDefs = None, timeStepsCount = 0, timeStepDelta = 1, db = None,\
	      gradsBin = "", gradsEcho = False, postProcess = None):
    if gradsBin:
      self.g = grads.GrADS(Window=False, Bin=gradsBin, Echo = gradsEcho)
    else:
      self.g = grads.GrADS(Window=False, Echo = gradsEcho)
    self.db = db
    self.measureDefs = measureDefs
    self.timeStepsCount = timeStepsCount
    self.timeStepDelta = timeStepDelta
    self.interpolatedMeasures = interpolatedMeasures()
    self.points = points
    if postProcess:
      self.postProcess = types.MethodType(postProcess, self)
  
  def __interpolateOneFile(self, fileName, points, ptslons, ptslats, oneFileDefs):
    g = self.g
    print("opening file")
    g.open(fileName)
    timeIndex = 1
    while timeIndex <= self.timeStepsCount:
      g("set t {t}".format(t = timeIndex))
      tyme = get_current_grads_time(g)
      print("elaborating time " + str(timeIndex))
      for measureDef in oneFileDefs:
	if not measureDef.gradsName:
	  continue
	ipolatedDataList = g.sampleXY(measureDef.gradsName, array(ptslons), array(ptslats), order = 0)
	self.interpolatedMeasures.addMeasures(tyme = tyme, measureDef = measureDef, points = points,\
	     measures = ipolatedDataList)
      timeIndex += self.timeStepDelta
    g("close 1")
  
  def __serializeToDb(self):
    print("clearing table MEASURES")
    self.db.execute("DELETE FROM MEASURES")
    
    points = self.points
    flds = set([df.dbColName for df in self.measureDefs if df.dbColName])
    fldsStr = ", ".join(flds)
    parsStr = ":" + ", :".join(flds)
    sqlGetTimeZone = "SELECT GMOFFSET FROM SPOTS WHERE SPOT=:SPOT"
    insertQry = """\
         INSERT INTO MEASURES (SPOT, MSR_TIME, MSR_TIME_FORMATTED, {flds}) 
         VALUES (:SPOT, :MSR_TIME, :MSR_TIME_FORMATTED, {pars})
         """\
         .format(flds = fldsStr, pars = parsStr)
    
    times = self.interpolatedMeasures.getTimesList()
    for point in points:
      print("Serializing to db point " + point.name.encode('ascii', 'ignore'))
      qTimeZone = self.db.execute(sqlGetTimeZone, [point.name]) 
      timezone = qTimeZone.fetchone()[0]
      for tyme in times:
        vals = []
        vals.append(point.name)
        vals.append(tyme)
        timestr = time2str(tyme, timezone = timezone)
        vals.append(timestr)
        for fld in flds:
	  measureDef = getByDbColName(self.measureDefs, fld)
	  measure = self.interpolatedMeasures.getMeasure(measureDef = measureDef, point = point, tyme = tyme)
	  if not measure is ma.masked:
	    vals.append(float(measure))
	  else:
	    vals.append(-1)
        self.db.execute(insertQry, vals)
    self.db.commit()
  
  def execute(self):
    print("timeStepsCount = " + str(self.timeStepsCount))
    
    pts = self.points;
    ptslons, ptslats = fm_points.getCoordsList(pts)
    
    files, defsByFile = groupDefsByGradsFile(self.measureDefs)
    for fl in files:
      if not fl:
	continue      
      print("")
      print("Elaborating file " + fl)
      oneFileDefs = defsByFile[fl]
      defsString = str(map(lambda df: df.gradsName, oneFileDefs))
      print("file variables: " + defsString)
      self.__interpolateOneFile(fl, pts, ptslons, ptslats, oneFileDefs)
      
    try:
      print("")
      print("Performing variables postProcess, if required")
      #trying to access postProcess method, if set
      self.postProcess(points = self.points, intpMeasures = self.interpolatedMeasures, measureDefs = self.measureDefs)
    except:
      print(traceback.format_exc())
    
    print("")
    print("serializing to db")
    self.__serializeToDb()
  

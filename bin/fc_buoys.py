import os
from fc_settingsLoader import settings

class buoy:
  def __init__(self, name, lon, lat, grid, index):
    self.name = name
    self.lon = lon
    self.lat = lat
    self.grid = grid
    self.index = index

class buoys:
  def __init__(self, buoys_file):
    self.fc_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../')
    self.buoys_file = buoys_file
    self._items = []
    self._filelines = []
    try:
      f = open(self.buoys_filename(), "r")
    except:
      f = None
    index = 1
    gridnames = [settings.main_grid_name]
    gridnames.extend(settings.zoom_grid_names)
    actual_grid_name = settings.main_grid_name
    if f:
      for ln in f:
	if ln.strip(" \n"):
	  if ln[0] != "$":
	    self._filelines.append(ln.strip("\n"))
	    ln = ln.replace('"', "").strip(" \n")
	    elms = ln.split(" ")
	    elms = filter(lambda e: e, elms)
	    name = elms[2]
	    lon = float(elms[0])
	    lat = float(elms[1])
	    b = buoy(name, lon, lat, actual_grid_name, index)
	    self._items.append(b)
	    index += 1
	  else:
	    candidate_grid_name = ln.strip("$\n")
	    if candidate_grid_name in gridnames:
	      actual_grid_name = candidate_grid_name
      f.close()
	
  def buoys_filename(self):
    return self.buoys_file
	
  def filetext(self):
    filetext = "\n".join(self._filelines)
    if not filetext:
      filetext = '   0.0       0.0       "FAKE_POINT"'
    return filetext
   
  def points_out_template_filename(self):
    return os.path.join(self.fc_dir, "templates/ww3_pointoutput_script_template.sh")
	
  def get_template_text(self):
    f = open(self.points_out_template_filename())
    txt = f.read() + "\n"
    f.close()
    return txt
	
  def build_pointoutputs_script(self):
    s = ""
    for b in self:
      scpt = self.get_template_text()
      scpt = scpt.replace("@@BUOYID", str(b.index))
      scpt = scpt.replace("@@BUOYNAME", str(b.name))
      scpt = scpt.replace("@@BUOY_GRID", b.grid)
      s += scpt
    return s
	
  def __len__(self):
    return len(self._items)
    
  def __getitem__(self, i):
    return self._items[i]
      

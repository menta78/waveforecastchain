from spectralFigureLogics import *

try:
  import fc_spectralFigure_settings
  figures = fc_spectralFigure_settings.figures
  try:
    outputType = fc_spectralFigure_settings.outputType
  except:
    outputType = sfOutTypOneFile
  try:
    outputDt = fc_spectralFigure_settings.outputDt
  except:
    outputDt = 3600*3 #three hours
except:
  figures = []
  outputType = sfOutTypOneFile

def getWw3MultiScriptLines():
  scriptGen = wholeScriptGenerator()
  return scriptGen.getWw3MultiScriptLines(figures, outputType, outputDt)
  
def getWw3OutpScriptLines():
  scriptGen = wholeScriptGenerator()
  return scriptGen.getWw3OutpScriptLines(figures, outputType, outputDt)
  
  


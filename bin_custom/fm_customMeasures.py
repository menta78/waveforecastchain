
"""
#measure class prototype
class I_HL:
  
  def __init__(self):
    self.name = 'I_HL'
    self.description = "measures obtainded by wave high-length"
    self.I1Def = measureDef(dbColName = "I1")
    self.I4Def = measureDef(dbColName = "I4")
    self.measureDefs = [self.I1Def, self.I2Def]
    
  def compute(self, point = None, measureDefs = None, intpMeasures = None):
    hss = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("hs"))
    meanls = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("lmn"))  
    
    tymes = hss.key()
    for tm in tymes:
      hs = hss[tm]
      meanl = meanls[tm]
      I1 = min(hs*meanl*S1, 10)
      I4 = min((hs*meanl)**0.5*S4, 10)
      intpMeasures.addMeasure(point, tm, self.I1Def, I1)
      intpMeasures.addMeasure(point, tm, self.I4Def, I4)
    
#in order to be evaluated, a measure object must be instantiated and added to customMeasures list
"""

#list of customMeasure objects

import math
from points_measures_2.fm_measureDefs import *

Hmax = 3.5
Tmax = 9.
Lmax = 9.8*Tmax**2./(2*math.pi)

S1 = 10./(Hmax*Lmax)
S2 = 10./(Hmax*Tmax)
S3 = 10./(Hmax**2.*Tmax)
S4 = 10./(Hmax*Lmax)**0.5

class I_HL:
  
  def __init__(self):
    self.name = "I_HL"
    self.description = "measures obtainded by wave high-length"
    self.I1Def = measureDef(dbColName = "I1")
    self.I4Def = measureDef(dbColName = "I4")
    self.measureDefs = [self.I1Def, self.I4Def]
    
  def compute(self, point = None, measureDefs = None, intpMeasures = None):
    hss = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("hs"))
    meanls = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("lmn"))  
    
    tymes = hss.keys()
    for tm in tymes:
      hs = hss[tm]
      meanl = meanls[tm]
      I1 = min(hs*meanl*S1, 10)
      I4 = min((hs*meanl)**0.5*S4, 10)
      intpMeasures.addMeasure(point, tm, self.I1Def, I1)
      intpMeasures.addMeasure(point, tm, self.I4Def, I4)
   
class I_HT:
  
  def __init__(self):
    self.name = "I_HT"
    self.description = "measures obtainded by wave high-period"
    self.I2Def = measureDef(dbColName = "I2")
    self.I3Def = measureDef(dbColName = "I3")
    self.measureDefs = [self.I2Def, self.I3Def]
    
  def compute(self, point = None, measureDefs = None, intpMeasures = None):
    hss = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("hs"))
    meanps = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("tmn"))  
    
    tymes = hss.keys()
    for tm in tymes:
      hs = hss[tm]
      meanp = meanps[tm]
      I2 = min(hs*meanp*S2, 10)
      I3 = min(hs**2.*meanp*S3, 10)
      intpMeasures.addMeasure(point, tm, self.I2Def, I2)
      intpMeasures.addMeasure(point, tm, self.I3Def, I3)
   
class I_Surf:
  
  def __init__(self):
    self.name = "I_Surf"
    self.description = "measures for surfing"
    self.ISurfDef = measureDef(dbColName = "ISurf")
    self.measureDefs = [self.ISurfDef]
    
  def compute(self, point = None, measureDefs = None, intpMeasures = None):
    hss = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("hs"))
    meanps = intpMeasures.getMeasures(point, measureDef = measureDefs.getByGradsName("tmn"))  

    tymes = hss.keys()
    for tm in tymes:
      H = hss[tm]
      T = meanps[tm]
      g = 9.8
      ISurf = H*100.*T*(H*100.+T**2./(g*100.))/(g*250.*259) * 10
      ISurf = max(0, min(ISurf, 10))
      intpMeasures.addMeasure(point, tm, self.ISurfDef, ISurf)

customMeasures = [I_HL(), I_HT(), I_Surf()]

def addCustomMeasuresToMsrDefs(measureDefsList):
  map(lambda cm: measureDefsList.extend(cm.measureDefs), customMeasures)
    

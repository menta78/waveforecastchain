import time
import sys

def getOmpNumThreads():
  #return 8
#  tmtpl = time.localtime()
#  isIdle = (tmtpl.tm_wday in [5,6]) or (tmtpl.tm_hour >= 19) or (tmtpl.tm_hour < 9)
  while True:
    tmtpl = time.localtime()
    #isIdle = (tmtpl.tm_wday in [5,6]) or (tmtpl.tm_hour >= 19) or (tmtpl.tm_hour < 8)
    isIdle = (tmtpl.tm_hour >= 13) or (tmtpl.tm_hour < 5)
    day = tmtpl.tm_wday
    hour = tmtpl.tm_hour
    timestr = time.strftime("%Y%m%d %H%M%S")
    if isIdle:
      print(timestr + ', running the hindcast')
      break
    else:
      print(timestr + ', day=' + str(day) + ', hour=' + str(hour) + ', not the right moment to run. Sleeping for 10 minutes')
      sys.stdout.flush()
      time.sleep(10*60)
  isIdle = True
  if isIdle:
    return 8
  else:
    return 2

omp_num_threads = getOmpNumThreads()

main_time_step = 1800 # 1/2 hours
wind_time_step = 3600 # 1 hours
grads_time_step = 3600 # 1 hour

ww3_time_format = "%Y%m%d %H%M%S"

wind_file_time_format = "%Y%m%d%H"
main_grid_name = "mediterr"
zoom_grid_names = []

log_keep_days = 3

loop_time_start = "19790101 000000"
#loop_time_start = "19900101 000000"
#loop_time_start = "20000101 000000"
#loop_time_start = "20001101 000000"

#operative
skip_extra_output = True

#archive generation
#restartdir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/restart_archive_generation"
#skip_extra_output = True

#rescue
#loop_time_start = "20120320 000000"
#restartdir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/restart_rescue"
#skip_extra_output = True


## SETTINGS FOR THE UNIVERSITY 
mpi_enabled = True
mpi_nproc = 24
mpi_command = "/usr/lib64/openmpi/bin/mpirun"

userdir = '/home/wavewatch'
fcdatabasedir = userdir + '/usr/wavewatch/v3.14/fc/hindcast'
tmpdir = userdir + '/tmp/tmp_hindcast'
#basedir = '/d1/users/wavewatch/usr/wavewatch/forecast_chain'
#maindir = '/d1/users/wavewatch/usr/wavewatch/v3.14'
basedir = '/vhe/naswavewatch/volume1/wavewatch/wavewatch/forecast_chain'
maindir = userdir + '/usr/wavewatch/v3.14'
wind_dir = userdir + '/tmp/tmp_hindcast/wind'
wind_grids_dir = basedir + '/winds'
ww3_grids_dir = basedir + '/grids'
custTempDir = tmpdir + '/computation'
ww3_script_path = tmpdir + '/ww3_script'

#results_dir = "/d1/users/wavewatch/storici"
#results_dir = userdir + "/storici_test/"
results_dir = "/vhe/naswavewatch/volume1/wavewatch/wavewatch/WIP_hindcastACC350"
archive_dir = results_dir

log_dir = fcdatabasedir + "/log"

main_input_file_pattern = "WRF10_@@FIRSTTIME.ctl$"
#liguria zoom is done on wrf zoom. andalusia zoom is done on 10 km data
zoom_input_file_patterns = []

#sim_duration = 7*86400 #7 days
sim_duration = 30*86400 #30 days
zoom_sim_duration = 40*86400 #7 days
#sim_duration = 86400 #24 hours
#sim_duration = 86400/2 #12 hours
simulations_time_interval = sim_duration

sqlite3_db = basedir + "/results/db/places.db"
points_interpolation_processes = 24
buoys_file = basedir + '/buoys.txt'

u_var_name = "ugrd10m"
v_var_name = "vgrd10m"

interpolate_by_wgrib = True
#grib_patterns = {main_grid_name: "WRF10_@@FIRSTTIME.grb$"}
grib_patterns = {main_grid_name: "wind.grb"}
#wgrib2path = "/d1/users/wavewatch/usr/wgrib2/"
#gribmap = "/d1/users/wavewatch/usr/grads-2.0.2/bin/gribmap"
#gradsBin = "/d1/users/wavewatch/usr/grads-2.0.2/bin/grads"
wgrib2path = userdir + "/usr/wavewatch/wgrib2_ipolates/"
gribmap = "/home/wavewatch/usr/wrf/build/grads-2.0.2/bin/gribmap"
gradsBin = "/home/wavewatch/usr/wrf/build/grads-2.0.2/bin/grads"


restartdir = fcdatabasedir + "/restart"
swanNestDataDir = fcdatabasedir + "/swannest"

nrestarts = 1
restart_files_keep_days = 1

#wwiiiOutputString = 'T F T F F  T T T T T  T T F F F  F F F F F  F F F F F  F F F F F  F'
wwiiiOutputString = 'F F T F F  T T T T T  T T T T T  T T T T T  T T F F F  F F F F F  F'

gribOutput = True
archiveType = 1  

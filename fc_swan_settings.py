class swan_nest_andalus_lev_alm:
  def __init__(self):
    self.name = "levante_almeria"
    self.gridname = "andalus"
    self.points = [(-1.59, 37.45), (-1.59, 37.13), (-1.59, 36.83), (-1.84, 36.83), (-2.10, 36.83)]
    self.d = 0.02
    self.points_indexes = range(4, 57)

class swan_nest_andalus_cabo_gata:
  def __init__(self):
    self.name = "cabo_gata"
    self.gridname = "andalus"
    self.points = [(-1.81, 37.21), (-1.81, 36.32), (-2.33, 36.32), (-2.33, 36.83)]
    self.d = 0.02
    self.points_indexes = range(57, 153)

class swan_nest_andalus_malaga:
  def __init__(self):
    self.name = "malaga"
    self.gridname = "andalus"
    self.points = [(-4.04, 36.74),(-4.04, 36.29),(-4.73, 36.29),(-4.73, 36.56)]
    self.d = 0.02
    self.points_indexes = range(153, 220)

class swan_nest_genova:
  def __init__(self):
    self.name = "genova"
    self.gridname = "liguria"
    self.points = [(8.7, 44.405),(8.7, 44.27),(9.30, 44.27),(9.30, 44.32)]
    self.d = 0.01
    self.points_indexes = range(6, 83)

class swan_nest_spezia:
  def __init__(self):
    self.name = "spezia"
    self.gridname = "liguria"
    self.points = [(9.09,44.37),(9.09,43.92),(10.03,43.92)]
    self.d = 0.01
    self.points_indexes = range(84, 221)

#IMPERIESE
#7.60 44.21
#7.60 43.69
#8.35 43.69
#8.35 44.21
class swan_nest_imperia:
  def __init__(self):
    self.name = "imperia"
    self.gridname = "liguria"
    self.points = [(7.2,43.64),(7.2,43.6),(8.5,43.6),(8.5,44.3)]
    self.d = 0.01
    self.points_indexes = range(222, 423)

#SAVONESE
#8.30 44.45
#8.30 44.10
#8.75 44.10
#8.75 44.45 
class swan_nest_savona:
  def __init__(self):
    self.name = "savona"
    self.gridname = "liguria"
    self.points = [(8.3,44.15),(8.3,44.1),(8.85,44.1),(8.85,44.42)]
    self.d = 0.01
    self.points_indexes = range(423, 514)

"""
versilia
latini = 43.35;
latfin = 44.15;
lonini = 9.75;
lonfin = 10.50;
"""
class swan_nest_toscana_versilia:
  def __init__(self):
    self.name = "toscana_versilia"
    self.gridname = "liguria"
    self.points = [(10.50, 44.15), (9.75, 44.15), (9.75, 43.35), (10.50, 43.35)]
    self.d = 0.01
    self.points_indexes = range(515, 663)

"""
elba
latini = 42.50;
latfin = 43.50;
lonini = 9.60;
lonfin = 11.30;
"""
class swan_nest_toscana_elba:
  def __init__(self):
    self.name = "toscana_elba"
    self.gridname = "liguria"
    self.points = [(10.40, 43.50), (9.60, 43.50), (9.60, 42.50), (11.30, 42.50)]
    self.d = 0.01
    self.points_indexes = range(664, 985)

swanNestSettings = [swan_nest_andalus_lev_alm(), swan_nest_andalus_cabo_gata(),\
 	swan_nest_andalus_malaga(), swan_nest_genova(), swan_nest_spezia(),\
        swan_nest_imperia(), swan_nest_savona(),\
        swan_nest_toscana_versilia(), swan_nest_toscana_elba()]


from numpy import array, ma

class interpolatedMeasures:
  
  def __init__(self):
    self.__msrsByPt = {}
    self.__tymes = {}
    
  def __isMasked(self, measure):
    return measure is ma.masked

  def addMeasure(self, point = None, tyme = 0, measureDef = None, measure = 0):
    msrsByDef = self.__msrsByPt.get(point.name, {})
    self.__msrsByPt[point.name] = msrsByDef
    msrsByTime = msrsByDef.get(measureDef.key(), {})
    msrsByDef[measureDef.key()] = msrsByTime
    if not (msrsByTime.has_key(tyme) and self.__isMasked(measure)):
      msrsByTime[tyme] = measure
      self.__tymes[tyme] = None

  def addMeasures(self, tyme = 0, measureDef = None, points = [], measures = []):
    for pt, msr in zip(points, measures):
      self.addMeasure(point = pt, tyme = tyme, measureDef = measureDef, measure = msr)
  
  def getTimesList(self):
    return self.__tymes.keys()
  
  def getMeasure(self, point = None, tyme = 0, measureDef = None):
    msrsByDef = self.__msrsByPt[point.name]
    msrsByTime = msrsByDef[measureDef.key()]
    return msrsByTime[tyme]
    
  def getMeasures(self, point = None, measureDef = None):
    msrsByDef = self.__msrsByPt[point.name]
    msrsByTime = msrsByDef[measureDef.key()]
    return msrsByTime

    

from fc_settings import *

from fc_common import *
if use_custom_settings:
  from fc_custom_settings import *

import time
import os

def generateDayFiles(firstTime, ndays, directory):
  """
  for each day of symulation generates a file written, for example considering day 4/15/2012, Apr, 15 2012
  """
  tm = firstTime
  daySeconds = 86400
  maxday = tm + ndays*daySeconds
  timeIndx = 1
  while tm < maxday:
    saveFile(tm, timeIndx, directory)
    tm += daySeconds
    timeIndx += 1
  pass

def saveFile(tm, timeIndx, directory):
  fileTxt = getFileTxt(tm)
  filename = os.path.join(directory, "date{d}.txt".format(d = timeIndx))
  fl = open(filename, "w")
  fl.write(fileTxt)
  fl.close()

def getFileTxt(tm):
  """
  >>> getFileTxt(1333621526)
  'Thu Apr, 05 2012\\n'
  """
  tmtpl = time.gmtime(tm - time.altzone)
  return time.strftime("%a %b, %d %Y", tmtpl) + "\n"
#  return time.strftime("%Y, %a %b %d", tmtpl) + " - 00:00 UTC\n"

if __name__ == "__main__":
  generateDayFiles(1333621526, 3, './')

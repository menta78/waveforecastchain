#!/bin/csh
#python py_plot_Med_0.1.py
#python py_plot_Lig_0.1.py

############################################################
# GRADS script
############################################################
grads -blc "run grads_script_Medit.gs" > pippo
grads -blc "run grads_script_Lig.gs"   > pluto

############################################################
# Resizing and pasting UniGe Logo
############################################################
#typeset -Z3
set ind=1
while ( $ind <= 48 )
  set num=`printf %03d $ind`
#  echo $num  
  convert -rotate 0 Medit_Hs_tmp_$num.png  -draw  "image over  150,80    25,31  'logo_UniGe.jpg'"  -crop '770x530+0+70'  Medit_hs_$num.jpg
  convert -rotate 0 Medit_Tm_tmp_$num.png  -draw  "image over  150,80    25,31  'logo_UniGe.jpg'"  -crop '770x530+0+70'  Medit_tm_$num.jpg
  convert -rotate 0 Medit_Hs_tmp_IT_$num.png -draw "image over  190,-5    25,31  'logo_UniGe.jpg'" -crop '800x900+0+0'   Medit_hs_IT_$num.jpg

  convert -rotate 0 Ligurian_Hs_tmp_$num.png  -draw "image over  150,80    25,31  'logo_UniGe.jpg'" -crop '770x530+0+70' Ligurian_hs_$num.jpg
  convert -rotate 0 Ligurian_Tm_tmp_$num.png  -draw "image over  150,80    25,31  'logo_UniGe.jpg'" -crop '770x530+0+70' Ligurian_tm_$num.jpg
  @ ind= $ind + 1
end

############################################################
#Loop vecchio senza gli zeri
############################################################
#set ind=1
#while ( $ind <= 100 )
#  echo $ind
#  convert -rotate 0 Medit_Hs_tmp_$ind.png  -draw  "image over  150,80    25,31  'logo_UniGe.jpg'"  -crop '770x530+0+70'  Medit_hs_$ind.jpg

#  @ ind= $ind + 1
#end

############################################################
# Animation
############################################################
#mencoder "mf://Medit_hs*.jpg" -mf fps=2 -o Hs.avi -ovc lavc -lavcopts vcodec=mpeg4
convert -delay 100 -loop 0 Medit_hs_0*jpg     Hs_Med_anim.gif
convert -delay 100 -loop 0 Medit_tm_*jpg      Tm_Med_anim.gif
convert -delay 100 -loop 0 Medit_hs_IT_0*jpg  Hs_ITA_anim.gif
convert -delay 100 -loop 0 Medit_tm_IT_0*jpg  Tm_ITA_anim.gif
convert -delay 100 -loop 0 Ligurian_hs_*jpg   Hs_Lig_anim.gif
convert -delay 100 -loop 0 Ligurian_tm_*jpg   Tm_Lig_anim.gif

############################################################
# Cleaning
############################################################
rm Medit_Hs_tmp*.png
rm Medit_Tm_tmp*.png
rm Ligurian_Hs_tmp*.png
rm Ligurian_Tm_tmp*.png

mv /project/meteo/waves/output/maps/*.* /project/meteo/waves/output/maps/old
mv Medit*jpg Ligurian*jpg /project/meteo/waves/output/maps/
mv *gif /project/meteo/waves/output/maps/

rm pippo
rm pluto

cd /project/meteo/waves/output/maps/

scp -q /project/meteo/waves/output/maps/* giospud@server4:WWW/Home/Meteo/maps 

#cd /project/meteo/waves/output/maps/
#mv *.* /old
#cd


#        -font helvetica -fill black -pointsize 10 -draw "text 480,430
#'DIPARTIMENTO di INGEGNERIA delle'" \
#          -draw "text 450,445 'COSTRUZIONI, dell\'AMBIENTE e del TERRITORIO'"

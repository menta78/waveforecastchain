ln -sf mod_def.@@MAIN_GRID mod_def.ww3
ln -sf restart.@@MAIN_GRID restart.ww3
ln -sf wind.@@MAIN_GRID wind.ww3
set +e
ln -sf nest.@@MAIN_GRID nest.ww3
set -e

cat > ww3_shel.inp << EOF
$ WAVEWATCH III ww3_shel input file
$ ------------------------------------
$ Define input to be used with flag for use and flag for definition
$ as a homogeneous field (first three only); eight input lines.
$ For example line "T F  Winds" means that winds are considered 
$ and that they are not a homogeneous field
F F  Water levels
F F  Currents
EOF
if [ $wind_input_mode != 'SKIP' ]
then
  echo "T F  Winds" >> ww3_shel.inp;
else
  echo "F F  Winds" >> ww3_shel.inp;
fi
cat >> ww3_shel.inp << EOF
F    Ice concentrations
F    Assimilation data : Mean parameters
F    Assimilation data : 1-D spectra
F    Assimilation data : 2-D spectra.
$
$ Start date
   $t_beg  
$ End date
   $t_end
$ 
$ Define output server mode. This is used only in the parallel version
$ of the model. To keep the input file consistent, it is always needed.
$ IOSTYP = 1 is generally recommended. IOSTYP > 2 may be more efficient
$ for massively parallel computations. Only IOSTYP = 0 requires a true
$ parallel file system like GPFS.
$
$ IOSTYP = 0 : No data server processes, direct access output from
$ each process (requirese true parallel file system).
$ 1 : No data server process. All output for each type
$ performed by process that performes computations too.
$ 2 : Last process is reserved for all output, and does no
$ computing.
$ 3 : Multiple dedicated output processes.
$
   1
$
   $t_beg  $dt  $t_end
   N
   $FIELDS
   $t_beg     $dt  $t_end
@@POINTS
   0.0       0.0       "STOPSTRING"
   $t_beg      0  $t_end
EOF

#inserting restart file informations
t_rst_start=${restarts[0]};
t_rst_end=${restarts[$(($nrestarts - 1))]};
echo "   $t_rst_start      $simsInterval  $t_rst_end" >> ww3_shel.inp

cat >> ww3_shel.inp << EOF
   $t_beg      0  $t_end
   $t_beg      0  $t_end
$
  'the_end'  0
$
  'STP'
$
$ End of input file
EOF

  echo "   Running ww3_shel model ..."
  echo "   Screen output routed to $path_log/ww3_shel.out"

  cd $path_w

  #this is to prevent some problem in mpi exit to cause the whole process to crash
  set +e
  if [ "$MPI" = 'yes' ]
  then
    @@MPI_COMMAND -np $proc $path_e/ww3_shel  > $path_log/ww3_shel.out 2>&1 & echo "running ww3_shel ..."
    wait
    echo "ww3_multi run complete"
  else
    export OMP_NUM_THREADS=@@OMP_NUM_THREADS
    export OMP_STACKSIZE=@@OMP_STACKSIZE
    $path_e/ww3_shel > $path_log/ww3_shel.out
  fi

  mv out_grd.ww3 out_grd.${mods[0]}
  mv out_pnt.ww3 out_pnt.${mods[0]}
  ls restart*.ww3 | sed -e 'p;s/\(restart\)\([0-9]*\)\.ww3/\1\2.med_unst/' | xargs -n2 mv

  set -e

rm *.ww3
rm ww3_shel.inp


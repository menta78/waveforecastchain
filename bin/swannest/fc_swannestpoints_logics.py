from math import *
import os

class ESwanNest(Exception):
  pass

class nestPoint:
  def __init__(self, lon, lat, index, nestname):
    self.lon = lon
    self.lat = lat
    self.index = index
    self.nestname = nestname
    
  def ww3_conf_file_line(self):
    lonstr = str(self.lon).ljust(9)[0:9]
    latstr = str(self.lat).ljust(9)[0:9]
    name = ("\"SWNST_" + str(self.index)).ljust(11) + "\""
    rslt = "   " + "   ".join([lonstr, latstr, name]) + "\n"
    return rslt
    
class test_nest:
  def __init__(self):
    self.name = "test"
    self.gridname = "testgrid"
    self.points = [(1., 0.), (0., 1.), (-1., 0.)]
    self.d = 2.**0.5 / 2.    
    
class test_nest1:
  def __init__(self):
    self.name = "test"
    self.gridname = "testgrid"
    self.points = [(1., 0.)]
    self.d = 2.**0.5 / 2.    
    
def NestPoints(swnNstSttns):
  """
  >>> pts = NestPoints(swnNstSttns = [test_nest()])
  >>> len(pts)
  5
  >>> pts[0].lon
  1.0
  >>> pts[0].lat
  0.0
  >>> pts[0].index
  0
  >>> pts[0].nestname
  'test'
  >>> pts[1].lon
  0.5
  >>> pts[1].lat
  0.5
  >>> pts[2].lon
  0.0
  >>> pts[2].lat
  1.0
  >>> pts[3].lon
  -0.5
  >>> pts[3].lat
  0.5
  >>> pts[4].lon
  -1.0
  >>> pts[4].lat
  0.0
  >>> pts[4].nestname
  'test'
  >>> pts = NestPoints(swnNstSttns = [test_nest1()])
  >>> len(pts)
  1
  >>> pts[0].lon
  1.0
  >>> pts[0].lat
  0.0
  """
  result = []
  
  ptIndex = 0
  for nst in swnNstSttns:
    for ipt in range(len(nst.points) - 1):
      pt1 = nst.points[ipt]
      pt2 = nst.points[ipt + 1]
      dlon = pt2[0] - pt1[0]
      dlat = pt2[1] - pt1[1]
      
      dtot = (dlon**2+dlat**2)**0.5  
      nitr = floor(dtot/nst.d)
      dlon /= nitr
      dlat /= nitr
      
      lon0 = pt1[0]
      lat0 = pt1[1]
      for i in range(int(nitr)):
	lon = lon0 + dlon*i
	lat = lat0 + dlat*i
	np = nestPoint(lon, lat, ptIndex, nst.name)
	result.append(np)
	ptIndex += 1
    lastPt = nst.points[-1]
    np = nestPoint(lastPt[0], lastPt[1], ptIndex, nst.name)
    result.append(np)
    ptIndex += 1
  
  return result
	
def nestPoints_FileText(nestPoints):
  """
  >>> nstpts = [nestPoint(1., 1., 1, "test"), nestPoint(1., 2., 2, "test")]
  >>> nestPoints_FileText(nstpts)
  '   1.0         1.0         "SWNST_1   "\\n   1.0         2.0         "SWNST_2   "\\n'
  """
  rslt = ""
  actualnest = ""
  for np in nestPoints:
    rslt += np.ww3_conf_file_line()
  return rslt
  
def nestPoints_OutpFileText(nestPoints, swanNestSettings, first_index = 1):
  #rslt = ""
  #for np in nestPoints:
    #rslt += " " + str(np.index + first_index) + "\n"
  #return rslt.strip("\n")
  dirname = os.path.dirname(__file__)
  tmplfile = open(os.path.join(dirname, "ww3_script_SWANTRANSFERT_TEMPLATE"), "r");
  tmpllines = tmplfile.readlines()
  tmplfile.close()
  
  filetxt = []
  
  for nst in swanNestSettings:
    nstlines = map(None, tmpllines)
    nstlines = map(lambda s: s.replace("@@NESTNAME", nst.name), nstlines)
    nstlines = map(lambda s: s.replace("@@GRIDNAME", nst.gridname), nstlines)
    ptslines = ""
    if nst.points_indexes:
      for p in nst.points_indexes:
	ptslines += " " + str(p) + "\n"
    else:  
      pts = filter(lambda pt: pt.nestname == nst.name, nestPoints)
      for np in pts:
	ptslines += " " + str(np.index + first_index) + "\n"
    ptslines = ptslines.strip("\n")
    nstlines = map(lambda s: s.replace("@@SWANBOUNDARY", ptslines), nstlines)
    
    filetxt.extend(nstlines)
    
  return "".join(filetxt)
  
def check_nests(nests, gridnames):
  for nst in nests:
    if not nst.gridname in gridnames:
      raise ESwanNest("grid {c} is not defined. Check fc_swan_settings.py file".format(c = nst.gridname))
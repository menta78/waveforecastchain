import fc_common
from optparse import OptionParser
import imp

class FcOptionParser(OptionParser):
    def error(self, msg):
        pass

class paramArgs:
  
  def __init__(self):
    self.start_time = ''
    self.wind_ready = False
    self.disableSwan = False

def elabParams():
  paramargs = paramArgs()
  parser = FcOptionParser()
  parser.add_option("-s", "--skip_extra_output", help="Skips extra output", action="store_true")
  parser.add_option("-w", "--custom_swan_settings_file", help="loads custom fc_swan_settings module")
  parser.add_option("-c", "--settings_file_path", help="sets path for fc_settings module")
  parser.add_option("-t", "--start_time", help="sets actual simulation start time")
  parser.add_option("-r", "--wind_ready", help="system does not prepare wind data: they are ready", action="store_true")
  parser.add_option("-i", "--time_interval", help="sets simulations time interval")
  parser.add_option("-d", "--time_duration", help="sets simulations time duration")
  parser.add_option("-n", "--disable_swan", help="disables swan nest", action="store_true")
  parser.add_option("-f", "--spectral_figures_file", help="loads custom fc_spectralFigure_settings module")
  (options, args) = parser.parse_args()
  if options.settings_file_path:
    print("found parameter '--settings_file_path'. Importing corresponding settings")
    print("")
    imp.load_source("fc_settings", options.settings_file_path)
  if options.skip_extra_output:
    print("found parameter '--skip_extra_output'. Setting corresponding variable to True")
    print("")
    fc_settings.skip_extra_output = True
  if options.custom_swan_settings_file:
    print("found parameter '--custom_swan_settings_file'. Importing corresponding settings")
    print("")
    imp.load_source("fc_swan_settings", options.custom_swan_settings_file)
    
  if options.start_time:
    print("found parameter '--start_time'. Forcing simulation start time " + options.start_time)
    paramargs.start_time = options.start_time.strip("\"\'")
    
  if options.wind_ready:
    print("found parameter '--wind_ready. Skipping wind data preparation'")
    paramargs.wind_ready = options.wind_ready

  if options.time_duration:
    print("found parameter --time_duration. Setting time duration to " + options.time_duration)
    fc_settings.sim_duration = int(options.time_duration)

  if options.time_interval:
    print("found parameter --time_interval. Setting time interval to " + options.time_interval)
    fc_settings.simulations_time_interval = int(options.time_interval)
    
  if options.disable_swan:
    print("found parameter --disable_swan. Disabling swan nest")
    paramargs.disableSwan = True

  if options.spectral_figures_file:
    print("found parameter --spectral_figures_file. Importing corresponding settings")
    imp.load_source("fc_spectralFigure_settings", options.spectral_figures_file)
 
  return paramargs
 

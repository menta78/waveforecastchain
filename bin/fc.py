#!/usr/bin/python
import matplotlib
matplotlib.use('Agg')
from datetime import datetime
import sys
#sys.dont_use_bytecode = True
import os
import shutil
import traceback
import imp
if os.path.isfile("../fc_swan_settings.py"):
  imp.load_source("fc_swan_settings", "../fc_swan_settings.py")
if os.path.isfile("../fc_spectralFigure_settings.py"):
  imp.load_source("fc_spectralFigure_settings", "../fc_spectralFigure_settings.py")

import fc_params
paramargs = fc_params.elabParams()
from fc_settingsLoader import settings

swanNest = not paramargs.disableSwan
if swanNest:
  try:
    from fc_swan_settings import *
    import swannest.fc_swannestpoints as fc_swannestpoints
    print("swan nest enabled")
    swanNest = True 
  except:
    print("swan nest disabled")
    swanNest = False 

import fc_common
  
import fc_buoys
import fc_reviewCtlFile
import fc_restartFileManager
import fc_utils
import fc_directoryArchiver
from spectralFigure import spectralFigureIntf
from fc_windDataConverter import windGridSpecTyp, windDataConverter
import time
import calendar
import re
import fc_mainProgramScriptManager
import fc_ww3grid
import fc_boundaryCondsManager
  
def time2str(tm, time_format = settings.ww3_time_format):
  tmtpl = time.gmtime(tm)
  return time.strftime(time_format, tmtpl)
  
def str2time(timestr):
  tmtpl = time.strptime(timestr, settings.ww3_time_format)
  return calendar.timegm(tmtpl)

fcException = fc_common.fc_exception
  
class fc:
  
  def __init__(self):
    bindir = os.path.dirname(os.path.abspath(fc_common.__file__))
    self.fc_dir = os.path.dirname(bindir)
  
    try: 
      self.basedir = settings.basedir
    except:
      self.basedir = self.fc_dir
    
    self.latlon_gribfile = ""
    self.frst_tm = 0
    self.getWwiiiOutputStringInvokedOnce = False

  ################################
  #Directories and file names
  ################################
  
  def script_file(self):
    try:
      return settings.ww3_script_path
    except:
      return os.path.join(self.fc_dir, 'ww3_script')
  
  def script_template_file(self):
    try:
      return settings.customScriptTemplatePath
    except:
      result = os.path.join(self.fc_dir, 'templates/ww3_script_TEMPLATE.sh')
      print("settings.customScriptTemplatePath not found. Defaulting to " + result)
      return result
    
  def script_init_conds_template(self):
    return os.path.join(self.fc_dir, 'templates/ww3_script_init_conds_TEMPLATE.sh')
    
  def input_data_dir(self):
    try:
      return settings.wind_dir
    except:
      return os.path.join(self.basedir, "input_data")

  def tmp_dir(self):
    try:
      return settings.tempdir
    except:
      return os.path.join(self.basedir, "tmp")  

  def start_time_file(self):
    return os.path.join(self.restart_dir(), "start_time")
    
  def getNcrcatCmd(self):
    try:
      return settings.ncrcatcmd
    except:
      return 'ncrcat'

  def checkNcrcat(self):
    exitstatus = os.system(self.getNcrcatCmd() + ' -r')
    return exitstatus == 0

  def get_input_file(self, filePattern):
    filePattern = filePattern.replace("@@FIRSTTIME", \
                time2str(self.first_time(), time_format=settings.wind_file_time_format))
    lst = filter(lambda s: re.match(filePattern, s), os.listdir(self.input_data_dir()))
    if len(lst):
      return os.path.join(self.input_data_dir(), lst[0])
    else:
      return filePattern
    
  def winds_dir(self):
    #return os.path.join(self.fc_dir, "winds")
    return settings.wind_grids_dir
   
  def getGridFile(self, gridName):
    return os.path.join(settings.ww3_grids_dir, "ww3_grid.inp." + gridName)

  def winds_grid(self, gridName):
    if self.getRunType() == 'MULTI':
      return os.path.join(self.winds_dir(), "ww3_grid.inp.w_" + gridName)
    elif self.getRunType() == 'SHEL':
      return self.getGridFile(gridName)
   
  def ice_grid(self, gridName):
    return os.path.join(self.winds_dir(), "ww3_grid.inp.c_" + gridName)
    
  def winds_raw_file(self, gridName):
    if self.getRunType() == 'MULTI':
      return os.path.join(self.winds_dir(), "w_{s}.raw".format(s=gridName))
    else:
      return os.path.join(self.winds_dir(), "{s}.raw".format(s=gridName))

  def restart_dir(self):
    return settings.restartdir

  def results_subdir(self):
    return "WW3_" + time2str(self.first_time(), time_format="%Y%m%d%H")  

  def results_temp_path(self):
    return os.path.join(settings.results_dir, self.results_subdir())

  def getBuoysFile(self):
    try:
      return settings.buoys_file
    except:
      return os.path.join(self.basedir, 'buoys.txt')

  ################################
  # END #
  ################################  
  
  def first_time(self):
    if paramargs.start_time:
      return str2time(paramargs.start_time)

    if not self.frst_tm:
      if os.path.isfile(self.start_time_file()):
        fl = open(self.start_time_file(), "r")
        tmstr = fl.readline().strip(" \n")
        if tmstr:
          self.frst_tm = str2time(tmstr)
        else:
          self.frst_tm = str2time(settings.loop_time_start)
      else:
        #fc is called for the first time
        self.frst_tm = str2time(settings.loop_time_start)
    return self.frst_tm
    
  def next_run_first_time(self):
    return self.first_time() + settings.simulations_time_interval
    
  def first_time_string(self):
    return time2str(self.first_time())
    
  def time_steps_count(self):
    return int(settings.sim_duration)/int(settings.main_time_step)
    
  def wind_t_stps_count(self):
    return int(settings.sim_duration)/int(settings.wind_time_step)

  def zoom_wind_t_stps_count(self):
    zm_sim_dur = min(settings.zoom_sim_duration, settings.sim_duration)
    return int(zm_sim_dur)/int(settings.wind_time_step)
    
  def grads_steps_count(self):
    return int(settings.sim_duration)/int(settings.grads_time_step) + 1

  def last_time(self):
    return self.first_time() + self.time_steps_count()*settings.main_time_step

  def getWindTime(self, windTimeIndex):
    return self.first_time() + (windTimeIndex - 1)*settings.wind_time_step
   
  def last_time_string(self):
    tm = self.last_time()
    return time2str(tm)
    
  def getWgrib2Bin(self):
    try:
      return os.path.join(settings.wgrib2path, 'wgrib2')
    except:
      return 'wgrib2'

  def getRunType(self):
    try:
      return settings.runtype
    except:
      return 'MULTI'

  #generates ww3 wind input file from grib2  
  def grib2ww3(self, gridName, inputFilePattern, start_time_indx, end_time_indx, gribPattern):
    startDate = datetime.utcfromtimestamp(self.getWindTime(start_time_indx))
    endDate = datetime.utcfromtimestamp(self.getWindTime(end_time_indx))
    gridFile = self.winds_grid(gridName)
    g = fc_ww3grid.ww3grid(gridFile)
    gridType = 'UNST' if g.isUnstructured else 'REG' 
    gridMshGridFile = os.path.join(settings.ww3_grids_dir, gridName + '.msh')
    outputFilePath = self.winds_raw_file(gridName)
    windGridSpec = windGridSpecTyp(gridName, gridType, gridFile, gridMshGridFile, outputFilePath)
    windGribFile = self.get_input_file(gribPattern)
    windDtConv = windDataConverter(windGridSpec, windGribFile, startDate, endDate)
    windDtConv.execute()
    
  def getIceInputDir(self):
    try:
      return inputDir
    except:
      #returning an unexisting directory
      return '\#\#ice_disabled__not_existing_directory\#\#'
    
  def _enableSpectralFigs(self):
    try:
      return settings.enableSpectralFigs
    except:
      return False
  
  def getCustTempDir(self):
    try:
      return settings.custTempDir
    except:
      return ''

  def getCustMainDir(self):
    try:
      return settings.custMainDir
    except:
      return ''
  
  #generates ww3 script
  def prepare_script(self):
    
    def script_time_line():
      return  "t_beg='{t_beg:s}' ; t_end='{t_end:s}' ; t_rst='{t_end:s}' ; dt='{dt:s}' ; tn='{tn:s}' ; dt_grads='{dtgrads:s}' ; tn_grads='{tn_grads:s}'".\
	      format(t_beg=self.first_time_string(), t_end=self.last_time_string(), dt=str(settings.main_time_step), tn=str(self.time_steps_count()), dtgrads=str(settings.grads_time_step), tn_grads=str(self.grads_steps_count()))
	      
    def restart_exists():
      import os
      import re
      if not os.path.isdir(self.restart_dir()):
        os.mkdir(self.restart_dir())
      rstfls = filter(lambda s: re.search("restart", s), os.listdir(self.restart_dir()))
      return len(rstfls) > 0

    def preproc_grid_str(gridName):
      import fc_ww3grid
      g = fc_ww3grid.ww3grid(self.winds_grid(gridName))
      if g.isUnstructured:
        return "\$no grid string"
      else:
        return "   ".join([g.minlonstr(), g.maxlonstr(), str(g.nx), g.minlatstr(), g.maxlatstr(), str(g.ny)])

    def preproc_grid_str_ice(gridName):
      import fc_ww3grid
      iceGridFile = self.ice_grid(gridName)
      if os.path.isfile(iceGridFile):
        g = fc_ww3grid.ww3grid()
        return "   ".join([g.minlonstr(), g.maxlonstr(), str(g.nx), g.minlatstr(), g.maxlatstr(), str(g.ny)])
      else:
	return "$   UNEXISTING GRID, FILE WITHOUT MEANING"
      
    def getWwiiiOutputString():
      try:
	return settings.wwiiiOutputString
      except:
        result = 'WND HS LM T02 T0M1 T01 FP DIR SPR DP PHS PTP PLP PDIR PLP PSPR PWS PNR'
        if not self.getWwiiiOutputStringInvokedOnce:
     	  print("wwiiiOutputString variable not found in settings.")
          print("Defaulting to " + result)
          self.getWwiiiOutputStringInvokedOnce = True
	return result

    def getWindInputFileType():
      wgridfile = self.winds_grid(settings.main_grid_name)
      g = fc_ww3grid.ww3grid(wgridfile)
      return 'AI' if g.isUnstructured else 'LL'
      
    print("")
    print("generating script file ...")
    fl = open(self.script_template_file())
    scrptlines = fl.readlines()
    fl.close()
    scrptlines = map(lambda s: s.replace("@@RUNTYPE", self.getRunType()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@FC_DIR", self.fc_dir), scrptlines)
    scrptlines = map(lambda s: s.replace("@@CUST_TEMP_DIR", self.getCustTempDir()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@CUST_MAIN_DIR", self.getCustMainDir()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@WWIII_OUT_STR", getWwiiiOutputString()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@SCRIPT_CREATION_TIME", time.ctime()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@TIME_HORIZON", script_time_line()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@WW3_GRIDS", settings.ww3_grids_dir), scrptlines)
    scrptlines = map(lambda s: s.replace("@@WIND_GRIDS", self.winds_dir()), scrptlines)
    ngrids = 1 + len(settings.zoom_grid_names)
    scrptlines = map(lambda s: s.replace("@@N_GRIDS", str(ngrids)), scrptlines)
    scrptlines = map(lambda s: s.replace("@@RESULTS_DIR", self.results_temp_path()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@RESTART_DIR", self.restart_dir()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@ICECOVERINGDIR", self.getIceInputDir()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@LOG_DIR", os.path.join(settings.log_dir, self.results_subdir())), scrptlines)
    scrptlines = map(lambda s: s.replace("@@MPI_ENABLED", settings.mpi_enabled and 'yes' or 'no'), scrptlines)
    scrptlines = map(lambda s: s.replace("@@MPI_NPROC", str(settings.mpi_nproc)), scrptlines)
    scrptlines = map(lambda s: s.replace("@@MAIN_GRID", settings.main_grid_name), scrptlines)

    #scrptlines = map(lambda s: s.replace("@@TIME_RESTART", time2str(self.next_run_first_time())), scrptlines)
    restartFileManager = fc_restartFileManager.createFilesManager(self.next_run_first_time())
    scrptlines = map(lambda s: s.replace("@@TIME_RESTART", restartFileManager.getRestartTimesString()), scrptlines)
    scrptlines = map(lambda s: s.replace("@@NRESTARTS", str(restartFileManager.nrestarts)), scrptlines)
    scrptlines = map(lambda s: s.replace("@@SIMS_INTERVAL", str(settings.simulations_time_interval)), scrptlines)
    
    def getZoomGridsLine(pattern, separator = "\n"):
      reslt = ""
      for zoomgrid in settings.zoom_grid_names:
	reslt += pattern.format(grid = zoomgrid) + separator
      return reslt

    zoomgrids = getZoomGridsLine(pattern = '"{grid}"', separator = " ")
    scrptlines = map(lambda s: s.replace("@@ZOOM_GRIDS", zoomgrids), scrptlines)
    zoomgrids_wind = getZoomGridsLine(pattern = '"w_{grid}"', separator = " ")
    scrptlines = map(lambda s: s.replace("@@ZOOM_WIND_GRIDS", zoomgrids_wind), scrptlines)

    zoomgrids_ice = getZoomGridsLine(pattern = '"i_{grid}"', separator = " ")
    scrptlines = map(lambda s: s.replace("@@ZOOM_ICE_GRIDS", zoomgrids_ice), scrptlines)

    scrptlines = map(lambda s: s.replace("@@PREP_MAIN_GRID_STR", preproc_grid_str(settings.main_grid_name)), scrptlines)
    scrptlines = map(lambda s: s.replace("@@PREP_ICE_MAIN_GRID_STR", preproc_grid_str_ice(settings.main_grid_name)), scrptlines)
    prep_zoom_grids_str = ""
    prep_zoom_ice_grids_str = ""
    for zoom_grid_name in settings.zoom_grid_names:
      prep_zoom_grids_str += '"' + preproc_grid_str(zoom_grid_name) + '" '
      prep_zoom_ice_grids_str += '"' + preproc_grid_str_ice(zoom_grid_name) + '" '
    scrptlines = map(lambda s: s.replace("@@PREP_ZOOM_GRIDS_STR", prep_zoom_grids_str), scrptlines)    
    scrptlines = map(lambda s: s.replace("@@PREP_ICE_ZOOM_GRIDS_STR", prep_zoom_ice_grids_str), scrptlines)    
    gribOutputStr = 'true' if fc_utils.getGribOutput() else 'false'
    scrptlines = map(lambda s: s.replace("@@GRIBOUTP", gribOutputStr), scrptlines)   
    
    gradsOutputStr = 'true' if fc_utils.getGradsOutput() else 'false'
    scrptlines = map(lambda s: s.replace("@@GRADSOUTP", gradsOutputStr), scrptlines)
    
    if fc_utils.getNetcdfOutput() and (not self.checkNcrcat()):
      raise fcException("""the netcdf output requires an installation of nco.
Please install nco and if necessary set the variable ncrcatcmd in the settings file""")
      
    netcdfOutputStr = 'true' if fc_utils.getNetcdfOutput() else 'false'
    scrptlines = map(lambda s: s.replace('@@NCDFOUTP', netcdfOutputStr), scrptlines)
    netcdfTypeStr = fc_utils.getNetcdfType()
    scrptlines = map(lambda s: s.replace('@@NCDFTYPE', netcdfTypeStr), scrptlines)
    scrptlines = map(lambda s: s.replace('@@NCRCATCMD', self.getNcrcatCmd()), scrptlines)

    #adding point output parts
    buoys = fc_buoys.buoys(self.getBuoysFile())
    buoystext = buoys.filetext()
    scrptlines = map(lambda s: s.replace("@@POINTOUTPUTS", buoys.build_pointoutputs_script()), scrptlines)

    if swanNest:
      fc_swannestpoints.check_nests()
      nestPts = fc_swannestpoints.NestPoints()
      nestPtsFileText = fc_swannestpoints.nestPoints_FileText(nestPts)
      nestPtsOutpFileText = fc_swannestpoints.nestPoints_OutpFileText(nestPts, first_index = len(buoys) + 1)
      swanNestDir = settings.swanNestDataDir
    else:
      nestPts = "$"
      nestPtsFileText = "$"
      nestPtsOutpFileText = ""
      swanNestDir = "$"
    scrptlines = map(lambda s: s.replace("@@SWANNESTDIR", swanNestDir), scrptlines)
    scrptlines = map(lambda s: s.replace("@@SWANTRANSFERT", nestPtsOutpFileText), scrptlines)
      
    if self._enableSpectralFigs():
      spectralFigsScrptLines = spectralFigureIntf.getWw3MultiScriptLines()
      scrptlines = map(lambda s: s.replace("@@SPECTRALFIGSWWIIIMULTI", ''.join(spectralFigsScrptLines)), scrptlines)
      spectralFigsScrptLines = spectralFigureIntf.getWw3OutpScriptLines()
      scrptlines = map(lambda s: s.replace("@@SPECTRALFIGSWWIIIOUTP", ''.join(spectralFigsScrptLines)), scrptlines)
    else:
      spectralFigsPtsText = ''
      scrptlines = map(lambda s: s.replace("@@SPECTRALFIGSWWIIIOUTP", ''), scrptlines)
    ##########################
    if restart_exists():
      print("restart files found. No need of using ww3_strt")
      scrptlines = map(lambda s: s.replace("@@INITIAL_CONDITIONS", "#Initial conditions not needed: restart files exist"), scrptlines)
    else:
      print("restart files not found. Preparing script to use ww3_strt")
      fl = open(self.script_init_conds_template(), "r")
      init_conds_lines = "".join(fl.readlines()) + "\n"
      scrptlines = map(lambda s: s.replace("@@INITIAL_CONDITIONS", init_conds_lines), scrptlines)

    wwiiiPtsText = '\n'.join([buoystext, nestPtsFileText, spectralFigsPtsText])
    mainProgScriptManager = fc_mainProgramScriptManager.mainProgramScriptManager(self.getRunType(), settings, wwiiiPtsText)
    mainProgScrpt = mainProgScriptManager.generateScript()
    scrptlines = [s.replace("@@MAIN_PROGRAM", mainProgScrpt) for s in scrptlines]
    scrptlines = [s.replace('@@WND_GRID_INPUT_FILE_TYPE', getWindInputFileType()) for s in scrptlines]

    try:
      handleWindInput = settings.handleWindInput
    except:
      handleWindInput = True
    handleWindInputStr = 'HANDLE' if handleWindInput else 'SKIP'
    scrptlines = [s.replace("@@WIND_INPUT_MODE", handleWindInputStr) for s in scrptlines]
    

    scrptlines = fc_boundaryCondsManager.elaborateMainScriptInpuntBC(settings, scrptlines)

    fl = open(self.script_file(), "w")
    fl.writelines(scrptlines)
    fl.close()
    try:
      #this operation may fail
      os.chmod(self.script_file(), 0774)
    except:
      pass
  
  #generates extra output like png files and gif
  def generate_extra_output(self):

    def generate_points_msrs_2():
      try:
	print("Intepolating grids to points... (experimental version)")
	from points_measures_2 import fm
	firstTimeStr = time2str(self.first_time(), time_format=wind_file_time_format)
	fm.execute_interpolation(timeStepsCount = self.grads_steps_count(), gradsBin = self.getGradsBin(),\
		      substsMap = {"@@FIRSTTIME": firstTimeStr})
      except Exception, e:
	print("Error performing experimental points interpolation.")
	print("Exception type: " + str(e.__class__))
	print("Exception message: " + str(e))
	print(traceback.format_exc())

    def generateDateFileForSwan():
      flname = os.path.join(settings.swanNestDataDir, 'swan_date_file.txt')
      fl = open(flname, 'w')
      fl.write(self.first_time_string().replace(' ', '') + '\n')
      fl.write(time2str(self.next_run_first_time()).replace(' ', '') + '\n')
      fl.write(self.last_time_string().replace(' ', ''))
      fl.close()

    if settings.skip_extra_output:
      return
    import shutil
    import os
    reslt_dir = self.results_temp_path()
    files = filter(lambda s: s[-3:] in [".gs","csh","jpg"], os.listdir("../grads_scripts"))
    for f in files:
      shutil.copy(os.path.join("../grads_scripts", f), reslt_dir)
    curdir = os.getcwd()
    try:
      os.chdir(reslt_dir)
      print("")
      print("generating three days date files")
      import fc_generateDayFiles
      mapdir = os.path.join(settings.results_dir, 'maps')
      fc_generateDayFiles.generateDayFiles(self.first_time(), 3, mapdir)
      # generating date file for swan
      generateDateFileForSwan()
      print("done")
      print("")
      print("generating images ...")
      os.system("./create_img.csh")
      print("images generated")
      print("")
      try:
	print("")
        generate_points_msrs_2()
        print("")
        print("Intepolating grids to points...")
	grads_file = "grads.{grid}.ctl"
	grds_files = [grads_file.format(grid=settings.main_grid_name)]
	for zoom_grid_name in settings.zoom_grid_names:
	  grds_files.append(grads_file.format(grid=zoom_grid_name))
        from points_measures import fm
        fm.execute_interpolation(grds_files, \
			self.grads_steps_count(), settings.grads_time_step)
        print("grid interpolation to points executed")
        print("")
        print("saving measures db to site ...")
        os.system("scp -q ~/usr/wavewatch/v3.14/forecast_chain/results/db/* giospud@server4:WWW/Meteo/pntdb")
        print("done")
      except Exception as e:
	print("System failed to interpolate grids to points. Error: " + str(e))
      print("")
    finally:
      os.chdir(curdir)
      for f in files:
        os.remove(os.path.join(reslt_dir, f))
    
  def getLastComputationDir(self):
    try:
      return last_computation_dir
    except:
      return ''

  def archive_results(self):
    resltdir = os.path.join(settings.results_dir, self.results_subdir())
    if not os.path.isdir(settings.archive_dir):
      os.mkdir(settings.archive_dir)
    if fc_utils.getArchiveType() == fc_common.zipArchive:
      import wa_compression
      wa_compression.zipdirectory(resltdir,  os.path.join(settings.archive_dir, self.results_subdir() + ".zip"))
    else:
      assert fc_utils.getGribOutput() or fc_utils.getNetcdfOutput(), "directory archive supported only for gribOutput == True or netcdfOutput == True"
      archiver = fc_directoryArchiver.dirArchiver(resltdir, settings.archive_dir)
      archiver.doArchive()
    lastComputationDir = self.getLastComputationDir()
    if lastComputationDir:
      print("copying all last results to " + lastComputationDir)
      try:
        shutil.rmtree(lastComputationDir)
      except:
        pass
      try:
        shutil.copytree(resltdir, lastComputationDir)
      except:
        traceback.format_exc()
    shutil.rmtree(resltdir)

  def clean_old_log(self):
    t = time.time()
    logdirs = os.listdir(settings.log_dir)
    for d in logdirs:
      pth = os.path.join(settings.log_dir, d)
      if t - os.path.getmtime(pth) > 86400*settings.log_keep_days:
        if os.path.isfile(pth):
          os.remove(pth)
        else:
          shutil.rmtree(pth)

  def remove(self, filename):
    try:
      os.remove(filename)
    except:
      pass
          
  def run(self):
    fc_utils.reviewEnvironPath()
    
    if not os.path.isdir(self.tmp_dir()):
      os.mkdir(self.tmp_dir())
    if not os.path.isdir(self.restart_dir()):
      os.mkdir(self.restart_dir())
    if swanNest and not os.path.isdir(settings.swanNestDataDir):
      os.mkdir(settings.swanNestDataDir)
    
    if not paramargs.wind_ready:
      #############################################
      #interpolating input to format needed by ww3
      #############################################
      self.remove(self.winds_raw_file(settings.main_grid_name))
      tcnt = self.wind_t_stps_count()
      zoom_tcnt = self.zoom_wind_t_stps_count()
      self.grib2ww3(settings.main_grid_name, settings.main_input_file_pattern, 1, tcnt, settings.grib_patterns.get(settings.main_grid_name))
      for zoom_grid_name, zoom_input_file_pattern in zip(settings.zoom_grid_names, settings.zoom_input_file_patterns):      
	self.remove(self.winds_raw_file(zoom_grid_name))
	self.grib2ww3(zoom_grid_name, zoom_input_file_pattern, 1, zoom_tcnt, settings.grib_patterns.get(zoom_grid_name))
	if tcnt > zoom_tcnt:
	  self.grib2ww3(zoom_grid_name, settings.main_input_file_pattern, zoom_tcnt + 1, tcnt, settings.grib_patterns.get(settings.main_grid_name))
      ##################################################
    else:
      print("skipping wind data preparation")

    self.prepare_script()
    curdir = os.getcwd()
    exitstatus = 0
    try:
      restartFileManager = fc_restartFileManager.createFilesManager(self.next_run_first_time())
      print("setting up the right restart files ...")
      restartFileManager.setRestartFiles(self.first_time())
      print("done")
      exitstatus = os.system(self.script_file())
      if not exitstatus:
	#changing variables names in ctl output file, basing on fc_settings.ctlNamesMapping
	resltdir = os.path.join(settings.results_dir, self.results_subdir())
      	fc_reviewCtlFile.reviewCtlFiles(resltdir, isGrib2FileName = fc_utils.getGribOutput())
	print("writing start_time_file for next simulation")
	#os.remove(self.start_time_file())
	fl = open(self.start_time_file(), "w")
	fl.write(time2str(self.next_run_first_time()))
	fl.close()
      else:
	print("Some problems executing ww3 script")
    finally:
      os.chdir(curdir)

    def archive_generation_enabled():
      try:
        return settings.generate_archive
      except:
	return True
    
    if exitstatus == 0:
      self.generate_extra_output()
      if archive_generation_enabled():
        self.archive_results()
      try:
        shutil.rmtree(self.tmp_dir())
      except:
        traceback.format_exc()
      self.clean_old_log()
      try:
        restartFileManager.cleanRestartFiles(keepDays = restart_files_keep_days)
      except:
        restartFileManager.cleanRestartFiles()

if __name__ == "__main__":
  #raise Exception("under maintainance")
  runner = fc()
  runner.run()

#!/usr/bin/python

import sys
import imp
import time
import os
settingsFile = '../fc_settings_hindcast.py'
imp.load_source('fc_settings', settingsFile)
import fc_settings
import calendar
import shutil

hindcastServer = 'wavewatch@bob'
#hindcastWindDir = '/data02/hindcast/out_cfsr/'
#hindcastWindDir = '/data01/hindcast/out_cfsr/'
#hindcastWindDir = '/data03/hindcast/90s/'
hindcastWindDir = '/vhe/naswavewatch/volume1/wavewatch/wrf/hindcast'
wind_dir = fc_settings.wind_dir
wind_file = fc_settings.grib_patterns[fc_settings.main_grid_name]
if not os.path.isdir(wind_dir):
  os.makedirs(wind_dir)
gribmap = fc_settings.gribmap

startfile = fc_settings.restartdir + '/start_time'
if os.path.isfile(startfile):
  fl = open(startfile)
  starttimestr = fl.readline().strip('\n')
else:
  starttimestr = fc_settings.loop_time_start
starttmtpl = time.strptime(starttimestr, fc_settings.ww3_time_format)
tm = calendar.timegm(starttmtpl)

print('start time: ' + starttimestr)
print('listing necessary files')

files = []
#looping on the month
dayscount = calendar.monthrange(starttmtpl.tm_year, starttmtpl.tm_mon)[1]
#dayscount = 2
print('Days in this month: ' + str(dayscount))
fc_settings.sim_duration = dayscount*86400
fc_settings.simulations_time_interval = dayscount*86400
for d in range(dayscount):
  tmtpl = time.gmtime(tm + d*86400)
  tmstr = time.strftime('%Y%m%d%H', tmtpl)
  flname = 'WRF10_' + tmstr
  files.append(flname)

print('copying files from server')
for fl in files:
  gfpath = os.path.join(wind_dir, fl + '.grb')
  #if os.path.isfile(gfpath):
    #continue
  print('copying file ' + fl)
  sys.stdout.flush()
  flpath = os.path.join(hindcastWindDir, fl)
  #cmd = 'scp ' + hindcastServer + ':' + flpath + '\* ' + wind_dir
  #print('scp command: ' + cmd)
  cmd = 'cp ' + flpath + '* ' + wind_dir
  print('cp command: ' + cmd)
  error = os.system(cmd)
  if error:
    print("error copying file " + flpath)
  print('regenerating idx file')
  curpth = os.getcwd()
  os.chdir(wind_dir)
  try:
    os.system(gribmap + ' -i ' + fl + '.ctl')
  finally:
    os.chdir(curpth)

print('all hindcast files successfully downloaded')
print('generating the all-wind grib file')
windFilePath = os.path.join(wind_dir, wind_file)
wgrib2 = fc_settings.wgrib2path + 'wgrib2'
wgribcmd = wgrib2 + ' -match ":anl" -append -grib ' + windFilePath + ' ' + os.path.join(wind_dir, files[0])
for fl in files:
  gribfile = fl + '.grb'
  gfpath = os.path.join(wind_dir, gribfile)
  wgribcmd = wgrib2 + ' -match "[UV]GRD:10 m" -not ":anl" -append -grib ' + windFilePath + ' ' + gfpath
  print(wgribcmd)
  os.system(wgribcmd)
  sys.stdout.flush()
  
print("generating ctl and idx files")
os.environ['PATH'] += ":" + fc_settings.wgrib2path
g2ctlcmd = '../bin/g2ctl.pl ' + windFilePath + ' > ' + windFilePath + '.ctl'
os.system(g2ctlcmd)
os.system(gribmap + ' -i ' + windFilePath + '.ctl')
 
print("launching fc")
sys.stdout.flush()
oldcwd = os.getcwd()
os.chdir('../bin')
simDur = str(fc_settings.sim_duration)
cmd = './fc.py -i ' + simDur + ' -d ' + simDur + ' -n -f ' + settingsFile
print('fc command:')
print(cmd)
sys.stdout.flush()
exitstatus = os.system(cmd) 
if exitstatus:
  print("SOMETHING WRONG WITH FC!!")
os.chdir(oldcwd)

try:
  if not exitstatus:
    shutil.rmtree(wind_dir)
except:
  pass

if exitstatus:
  raise Exception('SOMETHING WRONG WITH FC')
print("all done")


#!/bin/bash
#this script must be launched after WRF
/project/meteo/waves/usr/wavewatch/waveforecastchain_v3.14/bin_custom_hpcmeteo/launchFc.sh

#ELABORATING SWAN
wwiiiOutputDir=/project/meteo/waves/output
wwiiiBobHome=/home/wavewatch/
wwiiiFCBobDir=$wwiiiBobHome/usr/wavewatch/v3.14/forecast_chain/
cd $wwiiiOutputDir
rm -r swannest_new
mkdir swannest_new
mv swannest/* swannest_new
zip -r swannest.zip swannest_new
rsync -avuz --progress -e "ssh mentaschi@server1.dicat.unige.it ssh" ./swannest.zip wavewatch@bob:$wwiiiFCBobDir"swannest.zip"
rm swannest.zip
ssh mentaschi@server1.dicat.unige.it ssh wavewatch@bob rm -r $wwiiiFCBobDir/swannest_new/
ssh mentaschi@server1.dicat.unige.it ssh wavewatch@bob unzip -o $wwiiiFCBobDir/swannest.zip -d $wwiiiFCBobDir
ssh mentaschi@server1.dicat.unige.it ssh wavewatch@bob rm $wwiiiFCBobDir/swannest.zip
ssh mentaschi@server1.dicat.unige.it ssh wavewatch@bob $wwiiiBobHome"/swan_run_new.bash"



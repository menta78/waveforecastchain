#!/usr/bin/python
# sample call:
#    python fc_event.py "20061204 000000" 9

hindcastServer = 'wavewatch@bob'
#hindcastWindDir = '/vhe/naswavewatch/volume1/wavewatch/wrf/hindcast/'
# 2-way storm
hindcastWindDir = '/home/wavewatch/usr/wrf/WPS/domains/med_hist/out_casestudies_tyrrh/2way'
#hindcastWindDir = '/data01/hindcast/out_cfsr/' #1995 event
#hindcastWindDirSon = '/home/wavewatch/usr/wrf/WPS/domains/med_hist/out_casestudies_fine'
#hindcastWindDirSon = '/home/wavewatch/usr/wrf/WPS/domains/med_hist/out_casestudies_tyrrh/'
# 2-way storm
hindcastWindDirSon = '/home/wavewatch/usr/wrf/WPS/domains/med_hist/out_casestudies_tyrrh/2way'
#hindcastWindDir = '/home/wavewatch/usr/wrf/WPS/domains/med2/out_nest/caseStudiesFatherData' #2013 event
#hindcastWindDir = '/data03/hindcast/wrf_operational/caseStudiesFatherData' #20121024 event
#hindcastWindDir = '/data01/hindcast/out_cfsr/'
#hindcastWindDir = '/home/wavewatch/usr/wrf/WPS/domains/med2/out_nest/'
gribfilePattern = 'WRF10_'

import sys
import imp
import time
import os
imp.load_source('fc_settings', '../fc_settings_hires.py')
import fc_settings
import calendar
import shutil

starttimestr = sys.argv[1]
dayscount = int(sys.argv[2])

wind_dir = fc_settings.wind_dir
wind_file = fc_settings.grib_patterns[fc_settings.main_grid_name]
wind_file_son = fc_settings.grib_patterns[fc_settings.zoom_grid_names[0]]
if not os.path.isdir(wind_dir):
  os.makedirs(wind_dir)
gribmap = fc_settings.gribmap

starttmtpl = time.strptime(starttimestr, fc_settings.ww3_time_format)
tm = calendar.timegm(starttmtpl)

print('start time: ' + starttimestr)
print('listing necessary files')

files = []
files_son = []
#looping on the month
#dayscount = 2
print('Days in this month: ' + str(dayscount))
fc_settings.sim_duration = dayscount*86400
fc_settings.simulations_time_interval = dayscount*86400
for d in range(dayscount):
  tmtpl = time.gmtime(tm + d*86400)
  tmstr = time.strftime('%Y%m%d%H', tmtpl)
  #flname = 'WRF10_' + tmstr
  flname = 'WRF_FAT_' + tmstr
  files.append(flname)
  flname = 'WRF_SON_' + tmstr
  files_son.append(flname)

print('copying files from server')
def copyFiles(files, hindcastWindDir):
  for fl in files:
    gfpath = os.path.join(wind_dir, fl + '.grb')
    #if os.path.isfile(gfpath):
      #continue
    print('copying file ' + fl)
    sys.stdout.flush()
    flpath = os.path.join(hindcastWindDir, fl)
    #os.system('scp ' + hindcastServer + ':' + flpath + '\* ' + wind_dir)
    os.system('cp ' + flpath + '* ' + wind_dir)

    flpath = os.path.join(wind_dir, fl)
    if os.path.isfile(flpath + '.grb2'):
      shutil.move(flpath + '.grb2', flpath + '.grb')
      ctlFile = flpath + '.ctl'
      shutil.move(flpath + '.grb2.ctl', ctlFile)
      try:
        os.remove(flpath + '.grb2.idx')
      except:
        pass
      f = open(ctlFile)
      ctllines = f.readlines()
      f.close()
      ctllines = map(lambda s: s.replace('grb2', 'grb'), ctllines)
      f = open(ctlFile, 'w')
      f.writelines(ctllines)
      f.close()

    print('regenerating idx file')
    curpth = os.getcwd()
    os.chdir(wind_dir)
    try:
      os.system(gribmap + ' -i ' + fl + '.ctl')
    finally:
      os.chdir(curpth)

copyFiles(files_son, hindcastWindDirSon)
copyFiles(files, hindcastWindDir)

print('all hindcast files successfully downloaded')
print('generating the all-wind grib file')

def generateMergedGrib(wind_file, files):
  windFilePath = os.path.join(wind_dir, wind_file)
  wgrib2 = fc_settings.wgrib2path + 'wgrib2'
  wgribcmd = wgrib2 + ' -match "[UV]GRD:10 m(.*):anl" -append -grib ' + windFilePath + ' ' + os.path.join(wind_dir, files[0] + '.grb')
  os.system(wgribcmd)
  for fl in files:
    gribfile = fl + '.grb'
    gfpath = os.path.join(wind_dir, gribfile)
    wgribcmd = wgrib2 + ' -match "[UV]GRD:10 m" -not ":anl" -append -grib ' + windFilePath + ' ' + gfpath
    print(wgribcmd)
    os.system(wgribcmd)
    sys.stdout.flush()
  print("generating ctl and idx files")
  os.environ['PATH'] += ":" + fc_settings.wgrib2path
  g2ctlcmd = '../bin/g2ctl.pl ' + windFilePath + ' > ' + windFilePath + '.ctl'
  os.system(g2ctlcmd)
  os.system(gribmap + ' -i ' + windFilePath + '.ctl')

generateMergedGrib(wind_file, files)
generateMergedGrib(wind_file_son, files_son)
  
 
print("launching fc")
sys.stdout.flush()
oldcwd = os.getcwd()
os.chdir('../bin')
simDur = str(fc_settings.sim_duration)
cmd = './fc.py -t "' + starttimestr + '" -i ' + simDur + ' -d ' + simDur + ' -n -f ../fc_settings_hires.py'
print('fc command:')
print(cmd)
sys.stdout.flush()
exitstatus = os.system(cmd) 
if exitstatus:
  print("SOMETHING WRONG WITH FC!!")
os.chdir(oldcwd)

try:
  if not exitstatus:
    shutil.rmtree(wind_dir)
except:
  pass

print("all done")



class point:
  def __init__(self, name, lon, lat):
    self.name = name
    self.lon = lon
    self.lat = lat
    
  def to_string(self):
    return self.name
    
  def __str__(self):
    return self.to_string() 

#retruns a list of point instances
def points(dbconn):
  q = dbconn.execute("SELECT * FROM SPOTS")
  pts = []
  for r in q:
    name = r[0]
    lon = r[3]
    lat = r[4]
    pt = point(name, lon, lat)
    pts.append(pt)
  return pts

def getCoordsList(points):
  lons = []
  lats = []
  for pt in points:
    lons.append(pt.lon)
    lats.append(pt.lat)
  return lons, lats
import unittest
import os
from fc_windDataConverter import *
from datetime import datetime
from fc_common import fc_exception

class testRegData(unittest.TestCase):

  def removeFile(self, flpath):
    try:
      os.remove(flpath)
    except:
      pass

  def testCreateDataConverter(self):
    gribfile = 'test.grb'
    outputfile = 'test.txt'
    startdate = datetime(1979, 1, 1, 1)
    enddate = datetime(1979, 1, 1, 4)
    
    windGridSpec = windGridSpecTyp('test', 'UNST', 'test', 'test', outputfile)
    conv = windDataConverter(windGridSpec, gribfile, startdate, enddate)
    self.assertEqual(unstDataConverter, conv.__class__)
    self.assertEqual(str, conv.windGribFile.__class__)
    self.assertEqual(windGridSpecTyp, conv.windGridSpec.__class__)
    windGridSpec = windGridSpecTyp('test', 'REG', 'test', 'test', outputfile)
    conv = windDataConverter(windGridSpec, gribfile, startdate, enddate)
    self.assertEqual(regDataConverter, conv.__class__)
    windGridSpec = windGridSpecTyp('test', '', 'test', 'test', outputfile)
    self.failUnlessRaises(fc_exception, windDataConverter, windGridSpec, gribfile, startdate, enddate)
    

  def testUnstDataConverter(self):
    outputfile = 'test/windDataConverter/out.txt'
    windGridSpec = windGridSpecTyp('unst_test', 'UNST', 'test/windDataConverter/ww3_grid.inp.unst_test', 'test/windDataConverter/unst_test.msh', outputfile)
    gribFile = 'test/windDataConverter/WRF10_1979010100.grb'
    startdate = datetime(1979, 1, 1, 1)
    enddate = datetime(1979, 1, 1, 4)
    self.removeFile(outputfile)
    unstDtConv = unstDataConverter(windGridSpec, gribFile, startdate, enddate, 'w')
    unstDtConv.execute()
    fl = open(outputfile)
    lines = fl.readlines()
    fl.close()
    self.removeFile(outputfile)
    nnodes = 30366
    self.assertEqual(4.*(2*nnodes + 1), len(lines))
    self.assertEqual('19790101 010000\n', lines[0])
    val = float(lines[10000].strip('\n'))
    self.assertAlmostEqual(5.47640547, val)
    self.assertEqual('19790101 020000\n', lines[2*nnodes + 1])
    val = float(lines[110000].strip('\n'))
    self.assertAlmostEqual(1.9351776, val)
    self.assertEqual('19790101 030000\n', lines[(2*nnodes + 1)*2])
    val = float(lines[140000].strip('\n'))
    self.assertAlmostEqual(8.22173439, val)
    self.assertEqual('19790101 040000\n', lines[(2*nnodes + 1)*3])
    val = float(lines[240000].strip('\n'))
    self.assertAlmostEqual(0.27101557, val)


  def testRegDataConverter(self):
    outputfile = 'test/windDataConverter/out.txt'
    windGridSpec = windGridSpecTyp('unst_test', 'REG', 'test/windDataConverter/ww3_grid.inp.reg_test', '', outputfile)
    gribFile = 'test/windDataConverter/WRF10_1979010100.grb'
    startdate = datetime(1979, 1, 1, 1)
    enddate = datetime(1979, 1, 1, 4)
    self.removeFile(outputfile)
    regDtConv = regDataConverter(windGridSpec, gribFile, startdate, enddate, 'w')
    regDtConv.execute()
    fl = open(outputfile)
    lines = fl.readlines()
    fl.close()
    self.removeFile(outputfile)
    nnodes = 336*180
    self.assertEqual(4.*(2*nnodes + 1), len(lines))
    self.assertEqual('19790101 010000\n', lines[0])
    val = float(lines[10000].strip('\n'))
    self.assertAlmostEqual(3.83356822135, val)
    self.assertEqual('19790101 020000\n', lines[2*nnodes + 1])
    val = float(lines[3*nnodes + 20000].strip('\n'))
    self.assertAlmostEqual(1.41583432955, val)
    self.assertEqual('19790101 030000\n', lines[(2*nnodes + 1)*2])
    val = float(lines[4*nnodes + 30000].strip('\n'))
    self.assertAlmostEqual(10.4562773174, val)
    self.assertEqual('19790101 040000\n', lines[(2*nnodes + 1)*3])
    val = float(lines[7*nnodes + 30000].strip('\n'))
    self.assertAlmostEqual(-0.19838358098, val)


if __name__ == '__main__':
  unittest.main()

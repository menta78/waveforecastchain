############################################################################################### 
#### setting matplotlib in order not to load a graphical environment (necessary for crontab usage)
import matplotlib
matplotlib.use('Agg')
###############################################################################################

import logging
import multiprocessing
multiprocessing.log_to_stderr(logging.DEBUG)
import time
import grads 
import math
import sys
from fc_settings import *

from fc_common import *
if use_custom_settings:
  from fc_custom_settings import *
  
from numpy import array
from custom_measures import *

nprocs = points_interpolation_processes

class point:
  def __init__(self, name, lon, lat):
    self.name = name
    self.lon = lon
    self.lat = lat
    
  def to_string(self):
    return self.name
    
  def __str__(self):
    return self.to_string()
    
class point_vars:
  def __init__(self, point, tm):
    self.point = point
    self.tm = tm
    self.hs = 0
    self.peakp = 0
    self.meanp = 0
    self.meand = 0
    self.peakd = 0
    self.meanl = 0
    self.wind_module = 0
    self.wind_dir_rad = 0
    self.mask = True
    
  def get_vars_list(self):
    varslist = [self.hs, self.meanp, self.peakp, self.meand, self.peakd, self.meanl, self.wind_module, self.wind_dir_rad]
    for msr in measures:
      varslist.extend(msr.compute(self))
    return varslist
    
  @staticmethod
  def get_vars_names():
    var_names = ["hs", "meanp", "peakp", "meand", "peakd", "meanl", "wind_module", "wind_dir_rad"]
    for msr in measures:
      var_names.extend(msr.varnames())
    return var_names
    
class point_vars_dict:
  
  def __init__(self):
    self._dict = {}
 
  def add(self, pt_msrs):
    pt_dict = self._dict.get(pt_msrs.point.to_string(), {})
    pt_dict[pt_msrs.tm] = pt_msrs
    self._dict[pt_msrs.point.to_string()] = pt_dict    
 
  def get_msrs_bytime(self, pointstr):
    #returns the list of the measures for all the times for poit
    return self._dict.get(pointstr, None)
    
  def get_pointsstr(self):
    return self._dict.keys()

class grads_interp_service:

  class grads_process(multiprocessing.Process):
    def __init__(self, queue, results_queue, namespace, gradsfile):
      multiprocessing.Process.__init__(self)
      self.queue = queue
      self.results_queue = results_queue
      self.namespace = namespace
      self.timeIndex = 1
      self.gradsfile = gradsfile
      self.daemon = True  
      self.logger = multiprocessing.get_logger()
      
    def run(self):
      try:
        try:
          g = grads.GrADS(Window = False, Echo = False, Bin = gradsBin)
        except NameError:
          #gradsBin variable is not defined. Trying using default grads installation
          g = grads.GrADS(Window = False, Echo = False)

	print("opening grads")
	g.open(self.gradsfile)
	print("grads opened")
	while True:
	  pt_msr = self.queue.get()
	  if self.namespace.timeIndex != self.timeIndex:
	    self.timeIndex = self.namespace.timeIndex
            print("setting time to " + str(self.timeIndex))
	    g("set t " + str(self.timeIndex))

	  pt = pt_msr.point
	  lon = array([pt.lon])
	  lat = array([pt.lat])
          """
          try:
	    print("interpolating point " + pt_msr.point.name + " at time " + str(self.timeIndex))
          except:
            pass
          """
	  hs = g.sampleXY("hs", lon, lat, order = 0)
	  if not hs.mask:
	    pt_msr.hs = float(hs.data)
	    pt_msr.peakp = float(g.sampleXY("peakp", lon, lat, order = 0).data)
	    pt_msr.meanp = float(g.sampleXY("tmn", lon, lat, order = 0).data)
            #WWIII saves propagation direction to east. We want oringin direction to north.
	    pt_msr.meand = float(g.sampleXY("dirmn", lon, lat, order = 0).data)
            pt_msr.meand = - pt_msr.meand - math.pi/2.
            #pt_msr.meand = - pt_msr.meand + math.pi/2.
            while pt_msr.meand < 0:
              pt_msr.meand += 2*math.pi
            #WWIII saves propagation direction to east. We want oringin direction to north.
	    pt_msr.peakd = float(g.sampleXY("peakd", lon, lat, order = 0).data)
            pt_msr.peakd = - pt_msr.peakd - math.pi/2.
            #pt_msr.peakd = - pt_msr.peakd + math.pi/2.
            while pt_msr.peakd < 0:
              pt_msr.peakd += 2*math.pi
	    pt_msr.meanl = float(g.sampleXY("lmn", lon, lat, order = 0).data)
	    wu = float(g.sampleXY("wu", lon, lat, order = 0).data)
	    wv = float(g.sampleXY("wv", lon, lat, order = 0).data)
	    
	    pt_msr.wind_module = (wu**2 + wv**2)**0.5
	    #wsind_dir is computed with respect to north direction. Thus, wv = cos(wind_dir_rad), wu = sin(wind_dir_rad)
	    if wu != 0:
	      pt_msr.wind_dir_rad = math.atan(wu/wv)
	      if wv < 0:
		pt_msr.wind_dir_rad += math.pi
              if pt_msr.wind_dir_rad < 0:
                pt_msr.wind_dir_rad += 2*math.pi
              #pt_masr.wind_dir_rad IS NOW THE WIND PROPAGATION DIRECTION. WE WANT ORIGIN DIRECTION
              #ROTATING IT OF ANOTHER 2PI ANGLE
              pt_msr.wind_dir_rad -= math.pi
              if pt_msr.wind_dir_rad < 0:
                pt_msr.wind_dir_rad += 2*math.pi
         
              """
              if pt_msr.wind_dir_rad < 0:
                print("################")
                print("wind dir < 0")
                print("wu, wv = {wu}, {wv}".format(wu=wu, wv=wv))
                print("tan(wu/wv) = {t}".format(t=math.atan(wu/wv)))
                print("saved: {a}".format(a=pt_msr.wind_dir_rad))
                print("################")
              """
            else:
              pt_msr.wind_dir_rad = 0
            pt_msr.mask = False
	  else:
            try:
              pt_msr.mask = True
	      #print("grads failed to interpolate point " + pt_msr.point.name + " from grid " + self.gradsfile)
            except:
              pass

	  self.queue.task_done()  
	  self.results_queue.put(pt_msr)
      except Exception as e:
	#print(u"exception was raised interpolating point " + pt_msr.point.name + u". Error: " + str(e))
	print("exception was raised interpolating point. Error: " + str(e))
	sys.stdout.flush()
	self.namespace.failure = True
	self.queue.task_done()  
	
      
  def __init__(self, grads_filename):
    self.queue = multiprocessing.JoinableQueue()
    self.results_queue = multiprocessing.Queue()
    self.mgr = multiprocessing.Manager()
    self.namespace = self.mgr.Namespace()
    self.namespace.timeIndex = 1
    self.namespace.failure = False
    self.procs = []
    for i in range(nprocs):
      p = grads_interp_service.grads_process(self.queue, self.results_queue, self.namespace, grads_filename)
      p.start()
      self.procs.append(p)
  
  def interpolate(self, pointsList, timeIndex, tm, measures):
    self.namespace.timeIndex = timeIndex
    for pt in pointsList:
      pt_msr = point_vars(pt, tm)
      measures.append(pt_msr)
      self.queue.put(pt_msr)
    
    #checking that all is ok. If somewhere something failed, quitting the process
    while not self.queue.empty():
      time.sleep(2)
      if self.namespace.failure:
        raise Exception("system failure! Exiting the process")
    
    del measures[:]
    #while not self.results_queue.empty():
    while len(measures) < len(pointsList):
      msr = self.results_queue.get()
      measures.append(msr)
   
  def finalize_procs(self):
    #for p in self.procs:
    #  p.terminate()
    time.sleep(10)
    self.mgr.shutdown()
    self.procs = []
    

from abc import *
import os
from fc_common import fc_exception

class abstractMainProgramScriptManager:
  __metaclass__ = ABCMeta

  @abstractmethod
  def __init__(self, settings, pointsText):
    self.settings = settings
  
  @abstractmethod
  def generateScript(self):
    return templateLines

    
runTypes = ['MULTI', 'SHEL']
def mainProgramScriptManager(mainGridType, settings, pointText):
  if mainGridType == 'MULTI':
    return ww3MultiScriptManager(settings, pointText)
  elif mainGridType == 'SHEL':
    return ww3ShellScriptManager(settings, pointText)
  else:
    raise fc_exception('run type must be one of: ' + ', '.join(runTypes))


class ww3MultiScriptManager(abstractMainProgramScriptManager):
  
  def __init__(self, settings, pointsText):
    self.settings = settings
    self.pointsText = pointsText.strip('\n\r')
    pth = os.path.dirname(os.path.abspath(__file__))
    self.templateFile = os.path.join(pth, '../templates/ww3_multi_TEMPLATE.sh') 

  def generateScript(self):
      
    def getZoomGridsLine(pattern, separator = "\n"):
      reslt = ""
      for zoomgrid in stts.zoom_grid_names:
	reslt += pattern.format(grid = zoomgrid) + separator
      return reslt

    stts = self.settings
    tmplfl = open(self.templateFile)
    lines = tmplfl.readlines()
    tmplfl.close()
    lines = [s.replace("@@MAIN_GRID", stts.main_grid_name) for s in lines]
    zoomgrids_ww3_multi_inp_1 = getZoomGridsLine(pattern = "echo \"'w_{grid}'  F F T F F F F\"   >> ww3_multi.inp")
    lines = [s.replace("@@ZOOM_WW3_MULTI_1_GRIDS", zoomgrids_ww3_multi_inp_1) for s in lines]

    try:
      handleWindInput = self.settings.handleWindInput
    except:
      handleWindInput = True
    if handleWindInput:
      zoomgrids_ww3_multi_inp_2 = getZoomGridsLine\
        (pattern = "echo \"'{grid}' 'no' 'no' 'w_{grid}' 'no' 'no' 'no' 'no' 2 1  0.00 1.00  F\"   >> ww3_multi.inp")
    else:
      zoomgrids_ww3_multi_inp_2 = getZoomGridsLine\
        (pattern = "echo \"'{grid}' 'no' 'no' 'no' 'no' 'no' 'no' 'no' 2 1  0.00 1.00  F\"   >> ww3_multi.inp")
    lines = [s.replace("@@ZOOM_WW3_MULTI_2_GRIDS", zoomgrids_ww3_multi_inp_2) for s in lines]
    
    zoomgrids_ww3_multi_inp_3 = getZoomGridsLine\
        (pattern = "echo \"'{grid}' 'no' 'no' 'no' 'i_{grid}' 'no' 'no' 'no' 2 1  0.00 1.00  F\"   >> ww3_multi.inp")
    lines = [s.replace("@@ZOOM_WW3_MULTI_3_GRIDS", zoomgrids_ww3_multi_inp_3) for s in lines]

    lines = [s.replace("@@POINTS", self.pointsText) for s in lines]
    lines = [s.replace("@@MPI_COMMAND", stts.mpi_command) for s in lines]
    lines = [s.replace("@@OMP_NUM_THREADS", str(stts.omp_num_threads)) for s in lines]
    try:
      omp_stacksize = stts.omp_stacksize
    except:
      omp_stacksize = '64M'
    lines = [s.replace("@@OMP_STACKSIZE", str(omp_stacksize)) for s in lines]
    return ''.join(lines)
    

class ww3ShellScriptManager(abstractMainProgramScriptManager):
  
  def __init__(self, settings, pointsText):
    self.settings = settings
    self.pointsText = pointsText.strip('\n\r')
    pth = os.path.dirname(os.path.abspath(__file__))
    self.templateFile = os.path.join(pth, '../templates/ww3_shel_TEMPLATE.sh') 
  
  def generateScript(self):
    stts = self.settings
    tmplfl = open(self.templateFile)
    lines = tmplfl.readlines()
    tmplfl.close()
    lines = [s.replace("@@MAIN_GRID", stts.main_grid_name) for s in lines]
    lines = [s.replace("@@POINTS", self.pointsText) for s in lines]
    lines = [s.replace("@@MPI_COMMAND", stts.mpi_command) for s in lines]
    lines = [s.replace("@@OMP_NUM_THREADS", str(stts.omp_num_threads)) for s in lines]
    return ''.join(lines)
    return templateLines



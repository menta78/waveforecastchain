main_time_step = 1800 # 1/2 hours
wind_time_step = 3600 # 1 hours
grads_time_step = 3600 # 1 hour

ww3_time_format = "%Y%m%d %H%M%S"

wind_file_time_format = "%Y%m%d%H"
main_grid_name = "mediterr"
zoom_grid_names = ["liguria"]

log_keep_days = 30

loop_time_start = "20130107 000000"

#operative
skip_extra_output = True

#archive generation
#restartdir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/restart_archive_generation"
#skip_extra_output = True

#rescue
#loop_time_start = "20120320 000000"
#restartdir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/restart_rescue"
#skip_extra_output = True


## SETTINGS FOR THE UNIVERSITY 
mpi_enabled = False
mpi_nproc = 24
mpi_command = "/usr/lib64/openmpi/bin/mpirun"

omp_num_threads = 2

basedir = '/f/Users/wavewatch/usr/wavewatch/forecast_chain'
wind_dir = basedir + '/tmp'
wind_grids_dir = basedir + '/winds'
ww3_grids_dir = basedir + '/grids'

results_dir = basedir + "/results"
archive_dir = basedir + "/results/archive"
last_computation_dir = basedir + "/results/lastResults"

log_dir = basedir + "/log"

main_input_file_pattern = "WRF_FAT_@@FIRSTTIME.ctl$"
#liguria zoom is done on wrf zoom. andalusia zoom is done on 10 km data
zoom_input_file_patterns = [main_input_file_pattern]

sim_duration = 3*86400 #72 hours
zoom_sim_duration = 2*86400 #48 hours
#sim_duration = 86400 #24 hours
#sim_duration = 86400/2 #12 hours
simulations_time_interval = 86400
#simulations_time_interval = 86400/2

sqlite3_db = basedir + "/results/db/places.db"
points_interpolation_processes = 24

u_var_name = "ugrd10m"
v_var_name = "vgrd10m"

interpolate_by_wgrib = True
grib_patterns = {main_grid_name: "wind.grb", zoom_grid_names[0]: "wind.grb"}
wgrib2path = "/f/Users/wavewatch/usr/wgrib2/"
gribmap = "/f/Users/wavewatch/usr/grads-2.0.2/bin/gribmap"
gradsBin = "/f/Users/wavewatch/usr/grads-2.0.2/bin/grads"

restartdir = basedir + "/restart"
swanNestDataDir = basedir + "/swannest"

nrestarts = 1
restart_files_keep_days = 7

#wwiiiOutputString = 'T F T F F  T T T T T  T T F F F  F F F F F  F F F F F  F F F F F  F'
wwiiiOutputString = 'T F T F F  T T T T T  T T T T T  T T T T F  F T F F F  F F F F F  F'

##############################
"""

## SETTINGS FOR HOME (TEST)
mpi_enabled = True
mpi_nproc = 4
mpi_command = "mpirun"

wind_dir = '/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/input_data/'

results_dir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/results"
archive_dir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/results/archive"

log_dir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/log"

main_input_file_pattern = "@@FIRSTTIME_mediterr.grb2.ctl$"
zoom_input_file_patterns = ["@@FIRSTTIME_liguria.grb2.ctl$", main_input_file_pattern]

loop_time_start = "19900224 120000"

sim_duration = 2*3600 #2 hours
zoom_sim_duration = 3600 #1 hours
simulations_time_interval = 3600

sqlite3_db = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/results/db/places.db"
points_interpolation_processes = 4

u_var_name = "ugrd10m"
v_var_name = "vgrd10m"

interpolate_by_wgrib = True
grib_patterns = {main_grid_name: "@@FIRSTTIME_mediterr.grb2$", zoom_grid_names[0]: "@@FIRSTTIME_liguria.grb2$",
  zoom_grid_names[1]: "@@FIRSTTIME_mediterr.grb2$"}
wgrib2path = "/home/lmentaschi/usr/wgrib2_IPOLATES/wgrib2/"
gribmap = "/home/lmentaschi/usr/grads-2.0.1.oga.1/Contents/gribmap"

restartdir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/restart"
swanNestDataDir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/swannest"

##############################
"""

####### SWAN SETTINGS ########
"""
class swan_nest_andalus:
  def __init__(self):
    self.points = [(-1.59, 37.35), (-1.59, 36.83), (-1.97, 36.83)]
    self.d = 0.02
"""
"""
class swan_nest_andalus_lev_alm:
  def __init__(self):
    self.points = [(-1.59, 37.45), (-1.59, 37.13), (-1.59, 36.83), (-1.84, 36.83), (-2.10, 36.83)]
    self.d = 0.02
    
class swan_nest_andalus_cabo_gata:
  def __init__(self):
    self.points = [(-1.81, 37.21), (-1.76, 36.77), (-1.81, 36.32), (-2.02, 36.37), (-2.33, 36.32), (-2.28, 36.83)]
    self.d = 0.02

class swan_nest_andalus_malaga:
  def __init__(self):
    self.points = [(-4.73, 36.48),(-4.73, 36.29),(-4.04, 36.29),(-4.04, 36.74)]
    self.d = 0.02

swanNestSettings = [swan_nest_andalus_lev_alm(), swan_nest_andalus_cabo_gata(), swan_nest_andalus_malaga()]
"""
##############################

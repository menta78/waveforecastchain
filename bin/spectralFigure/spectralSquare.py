from numpy import *
import spectralFigureLogics
from geodicca import landMask

class spectralSquare:
  def __init__(self, name, gridname, minlon, minlat, maxlon, maxlat, deltalon, deltalat, firstIndex, coastResolution = 'i'):
    self.name = name
    self.gridname = gridname
    self.points = []
    self.minlon = minlon
    self.minlat = minlat
    self.maxlon = maxlon
    self.maxlat = maxlat
    self.deltalon = deltalon
    self.deltalat = deltalat
    self.firstIndex = firstIndex
    self.coastResolution = coastResolution
    self._buildPoints()
    
  def _buildPoints(self):
    lons = arange(self.minlon, self.maxlon, self.deltalon)
    lats = arange(self.minlat, self.maxlat, self.deltalat)
    lndMskGen = landMask.landMaskBuilder(self.minlon, self.minlat,\
                self.maxlon, self.maxlat, self.deltalon, self.deltalat,\
                coastResolution = self.coastResolution)
    msk = lndMskGen.buildMask()
    indx = self.firstIndex
    for ilat, lat in zip(range(len(lats)), lats):
      for ilon, lon in zip(range(len(lons)), lons):
        if msk[ilat, ilon] != 0:
          pt = spectralFigureLogics.point(indx, lon, lat)
	  self.points.append(pt)	
	  indx += 1
  
  def getPoints(self):
    return self.points

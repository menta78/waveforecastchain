'clear'

_dt = 1

'open grads.liguria.ctl'

'q dims'
say result
line5=sublin(result,5) 
rundate=subwrd(line5,6)
'q file'
say result
line5=sublin(result,5)
tmax=subwrd(line5,12)
say rundate
say tmax

it=0;while(it<tmax);it=it+1;'set t 'it

mappa('Hs')
mappa('Tm')

endwhile

'quit'
************************************************************


************************************************************
* Funzione che disegna le differenti mappe

function mappa(map)

*-------------------------------------------------
if(map='Hs')
'clear'
'set lon 6 11'
'set lat 42.5 44.5'
'cospeakd = cos(peakd)'
'sinpeakd = sin(peakd)'
'set timelab off'
'set grads off'
'set gxout shaded'
'run ramp_middle.gs'
'set cterp on'
'set grid off'
'set mpt * off'
'set mpt 0 1 1 6'
'set mpt 1 15 1 1'
*'set mpdset hires'
'set line 0'
'set mpdset gmthighmap'
'set csmooth on'
'set stid on'
'd hs'
'set gxout vector'
'set ccolor 0'
'set arrscl 0.5 4'
'd skip(cospeakd,5,5);skip(sinpeakd,5,5)'
'set gxout contour'
'set cint 0.5'
'set ccolor 0'
'set clopts 0'
'set clab masked'
'd hs'
'run cbarm.gs'
***
'set line 0'
'draw recf 8.25 1.15 9 1.75' 
***
'set strsiz 0.25'
'set string 1 c 10 0'
'draw string 5.5 7.2 DICAT - University of Genoa'
***
'set strsiz 0.2'
'set string 1 c 5 0'
'draw string 5.5 0.5 Significant Wave Height [m] and Wave Direction'
***
*** Chiamo la funzione che scrive il titolo ed il testo
scrivitit()
 
*** Scrivo il file di output

fmt = '%03.0f'
hhhhh = _tist
rc = math_format(fmt,hhhhh)
tistf=rc

suff='.png'
nomef='Ligurian_Hs_tmp_'%tistf%suff
*say nomef

'set strsiz 0.125'
'set string 1 c 5 0'
'draw string 1.75 6.8 WRF(3.3km)+WWIII(2km)'
***
'printim 'nomef' white'
endif
*-------------------------------------------------


*-------------------------------------------------
if(map='Tm')
'clear'
'set lon 6 11'
'set lat 42.5 44.5'
'set timelab off'
'set grads off'
'set gxout shaded'
**'run ramp_loose.gs'
**'run ramp_dense.gs'
*'run ramp_time.gs'
*'run ramp_time_2.gs'
'run ramp_time_3.gs'
*'set cint 0.5'
'set cterp on'
'set grid off'
'set mpt * off'
'set mpt 0 1 1 6'
'set mpt 1 15 1 1'
*'set mpdset hires'
'set mpdset gmthighmap'
'set csmooth on'
'set stid on'
'd peakp'
'set gxout contour'
'set cstyle 3'
'set cint 1'
'set ccolor 0'
'set clopts 0'
'set clab masked'
'd peakp'
'run cbarm.gs'
'set line 0'
'draw recf 8.25 1.15 9 1.75' 
***
'set strsiz 0.25'
'set string 1 c 10 0'
'draw string 5.5 7.2 DICAT - University of Genoa'
***
'set strsiz 0.2'
'set string 1 c 5 0'
'draw string 5.5 0.5 Mean Wave Period [s]'
***
*** Chiamo la funzione che scrive il titolo ed il testo
scrivitit()
 
*** Scrivo il file di output
fmt = '%03.0f'
hhhhh = _tist
rc = math_format(fmt,hhhhh)
tistf=rc

suff='.png'
nomef='Ligurian_Tm_tmp_'%tistf%suff
*say nomef

'set strsiz 0.125'
'set string 1 c 5 0'
'draw string 1.75 6.8 WRF(3.3km)+WWIII(2km)'
***
'printim 'nomef' white'
endif
*-------------------------------------------------

return
************************************************************



************************************************************
* funzione che scrive titolo ed intestazioni per Mediterraneo
function scrivitit()

'query time'
date=subwrd(result,3);dayweek=subwrd(result,6);day=substr(date,4,2)
*min=substr(date,2,4);
hour=substr(date,1,2);month=substr(date,6,3);year=substr(date,9,4)
 
 
 if(month='JAN');im='01';endif
 if(month='FEB');im='02';endif
 if(month='MAR');im='03';endif
 if(month='APR');im='04';endif
 if(month='MAY');im='05';endif
 if(month='JUN');im='06';endif
 if(month='JUL');im='07';endif
 if(month='AUG');im='08';endif
 if(month='SEP');im='09';endif
 if(month='OCT');im='10';endif
 if(month='NOV');im='11';endif
 if(month='DEC');im='12';endif

_outputdata=year%im%day'_'%hour

*_databella='Time '%hour' of '%dayweek' '%day'-'%im'-'%year
*_databella=' '%hour':'%min' UTC '%dayweek' '%day'-'%im'-'%year
_databella=' '%hour':00 UTC '%dayweek' '%day'-'%im'-'%year

'query dims'
linea5=sublin(result,5);ist=subwrd(linea5,9);_tist=(ist-1)*_dt
linea4=sublin(result,4);level=subwrd(linea4,6)

*say _tist 

_subt=date

'set strsiz 0.125'    
'set string 1 c 5 0'  
'draw string 8.25 6.8 ' _databella ' (00+'_tist')'

return
************************************************************


import traceback
from interp_grads import *
import fm_db
from numpy import array
import time
from grads import GrADS

from fc_settings import *

from fc_common import *
if use_custom_settings:
  from fc_custom_settings import * 

#retruns a list of point instances
def points(dbconn):
  q = dbconn.execute("SELECT * FROM SPOTS")
  pts = []
  for r in q:
    name = r[0]
    lon = r[2]
    lat = r[3]
    pt = point(name, lon, lat)
    pts.append(pt)
  return pts
  
def opengrads(ctl_file):
  try:
    _gradsBin = gradsBin
  except:
    print(traceback.format_exc())
    _gradsBin = ''
  print("gradsBin = " + _gradsBin)
  if _gradsBin:
    g = GrADS(Window = False, Bin = gradsBin)
  else:
    g = GrADS(Window = False)
  f = g.open(ctl_file)
  return g, f  
  
def get_current_grads_time(g):
  tmpl = time.strptime(g.coords().time[0][3:], "%d%b%Y")
  return time.mktime(tmpl)
  
def interp_msrs(dbconn, gradsFileName, pt_msrs_dict, timesteps_count, timeDelta = 3600, timeIndexDelta = 1):
  startTime = time.time()
  g, f = opengrads(gradsFileName)
  #getting first time
  tm = get_current_grads_time(g)
  timeIndex = 1;
  ptslist = points(dbconn);
  interp_service = grads_interp_service(gradsFileName)
  try:
    while timeIndex <= timesteps_count:
      try:
	msrs = []
	interp_service.interpolate(ptslist, timeIndex, tm, msrs)
	for pt_msr in msrs:
          if not pt_msr.mask:
       	    pt_msrs_dict.add(pt_msr)
	timeIndex += timeIndexDelta
	tm += timeDelta*timeIndexDelta
	g("set t " + str(timeIndex))
	#this command raises an exception if timeIndex is out of range
	g.exp("hs")
      except:
	#loop over grads file finished
	break   
  finally:
    interp_service.finalize_procs()
    del interp_service
    print("waiting for all procs to terminate")
    time.sleep(10)
  g("close 1")
  print("elapsed time: {t} s".format(t=time.time() - startTime))
  pass

def execute_interpolation(grads_files, steps_count, time_delta, timeIndexDelta = 1):
  conn = fm_db.createDbConn()
  pt_msrs_dict = point_vars_dict()
  for gr_fl in grads_files:
    print("")
    print("#####################################################")
    print("### INTERPOLATING POINTS FROM GRID {s} ###".format(s=gr_fl))
    print("#####################################################")
    interp_msrs(conn, gr_fl, pt_msrs_dict, steps_count, time_delta, timeIndexDelta)
    print("#####################################################")
    print("### POINTS FROM GRID {s} INTERPOLATED ###".format(s=gr_fl))
    print("#####################################################")
    print("")
  fm_db.saveToDb(pt_msrs_dict, conn)
  conn.commit()
  conn.close()
    
if __name__ == "__main__":  
  #SAMPLE CALLS:
  #python fm.py /home/lmentaschi/tmp/results/WW3_2012020400/ grads.mediterr.ctl,grads.liguria.ctl
  #python fm.py /home/lmentaschi/tmp/results/WW3_2012020400/ grads.mediterr.ctl,grads.liguria.ctl
  import sys
  import os
  curr_dir = sys.argv[1]
  grads_files = sys.argv[2].split(",")
  old_curr_dir = os.getcwd()
  os.chdir(curr_dir)
  try:
    execute_interpolation(grads_files, 49, 3600, 12) #TEST
    #execute_interpolation(grads_files, 49, 3600, 1)
  finally:
    os.chdir(old_curr_dir)
  

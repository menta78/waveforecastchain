import os
import shutil
import time
from fc_settingsLoader import settings
  
def time2str(tm, time_format = "%Y%m%d_%H%M%S"):
  tmtpl = time.gmtime(tm)
  return time.strftime(time_format, tmtpl)

class fc_restartFilesManager:

  def __init__(self, restartdir, maingrid, zoomgrids, nextRunFirstTime, nrestarts):
    self.restartdir = restartdir
    self.gridnames = [maingrid]
    self.gridnames.extend(zoomgrids)
    self.nextRunFirstTime = nextRunFirstTime
    self.nrestarts = nrestarts
    self.simulationsTimeInterval = 3600*24 #1 day
    self.ww3timeformat = "%Y%m%d %H%M%S"

  def getCurrentFileName(self, gridname):
    return os.path.join(self.restartdir, "restart." + gridname)
    
  def getArchiveFileName(self, gridname, tm):
    dateString = time2str(tm)
    return self.getCurrentFileName(gridname) + "." + dateString
  
  def setRestartFiles(self, tm):
    for g in self.gridnames:
      archiveFile = self.getArchiveFileName(g, tm)
      currentFile = self.getCurrentFileName(g)
      if os.path.isfile(archiveFile):
        shutil.copyfile(archiveFile, currentFile)
  
  def cleanRestartFiles(self, keepDays = 3):
    #removing current restart files
    for g in self.gridnames:
      fname = self.getCurrentFileName(g)
      if os.path.isfile(fname):
	os.remove(fname)
    #removing old restarts
    self.cleanOldRestartFiles(keepDays)
  
  def cleanOldRestartFiles(self, keepDays):
    t = time.time()
    fls = os.listdir(self.restartdir)
    for f in fls:
      pth = os.path.join(self.restartdir, f)
      if t - os.path.getmtime(pth) > 86400*keepDays:
        os.remove(pth)
   
  def getRestartTimesString(self):
    result = ""
    for i in range(self.nrestarts):
      if result:
	result += " "
      tm = self.nextRunFirstTime + i*self.simulationsTimeInterval
      tmstr = "\"" + time2str(tm, time_format = self.ww3timeformat) + "\""
      result += tmstr
    return result

def createFilesManager(nextRunFirstTime):
  try:
    nrstrt = settings.nrestarts
  except:
    nrstrt = 1
  fm = fc_restartFilesManager(settings.restartdir, settings.main_grid_name, settings.zoom_grid_names, nextRunFirstTime, nrstrt)
  fm.ww3timeformat = settings.ww3_time_format
  fm.simulationsTimeInterval = settings.simulations_time_interval;
  return fm

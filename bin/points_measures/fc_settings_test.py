## SETTINGS FOR THE UNIVERSITY 
mpi_enabled = True
mpi_nproc = 24
mpi_command = "/usr/lib64/openmpi/bin/mpirun"

wgrib2command = "/home/wavewatch/usr/wavewatch/wgrib2_ipolates/wgrib2"
wind_dir = '/home/wavewatch/usr/wrf/WPS/domains/med2/out_nest'

results_dir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/results"
archive_dir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/results/archive"

log_dir = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/log"

main_grib_pattern = "WRF_FAT_@@FIRSTTIME.grb2$"
zoom_grib_pattern = "WRF_SON_@@FIRSTTIME.grb2$" 

loop_time_start = "20120204 000000"

sim_duration = 2*86400 #48 hours
#sim_duration = 86400 #24 hours
#sim_duration = 86400/2 #12 hours
simulations_time_interval = 86400
#simulations_time_interval = 86400/2

sqlite3_db = "/home/wavewatch/usr/wavewatch/v3.14/forecast_chain/results/db/places.db"
points_interpolation_processes = 24
##############################
"""

## SETTINGS FOR HOME (TEST)
mpi_enabled = True
mpi_nproc = 4
mpi_command = "mpirun"

wgrib2command = "/home/lmentaschi/usr/wgrib2_IPOLATES/wgrib2/wgrib2"
wind_dir = '/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/input_data/'

results_dir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/results"
archive_dir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/results/archive"

log_dir = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/log"

main_grib_pattern = "@@FIRSTTIME_mediterr.grb2$"
zoom_grib_pattern = "@@FIRSTTIME_liguria.grb2$"

loop_time_start = "19900224 120000"

sim_duration = 3600 #1 hours
simulations_time_interval = 3600

sqlite3_db = "/home/lmentaschi/usr/WaveWatchIII/v3.14/forecast_chain/results/db/places.db"
points_interpolation_processes = 4
##############################
"""

main_time_step = 1800 # 1/2 hours
wind_time_step = 3600 # 1 hours
grads_time_step = 3600 # 1 hour

ww3_time_format = "%Y%m%d %H%M%S"

wind_file_time_format = "%Y%m%d%H"
main_grid_name = "mediterr"
zoom_grid_name = "liguria"

log_keep_days = 30

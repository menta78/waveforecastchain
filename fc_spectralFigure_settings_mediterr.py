import numpy as np
from spectralFigure.spectralSquare import *
from fc_swan_settings import swanNestSettings

swNestLastIndex = swanNestSettings[-1].points_indexes[-1]
refLat = 45
dxkm = 50
dykm = 50
minx, maxx, dx = -5.9, 36.5, dxkm/40000.*360./np.cos(refLat/180.*np.pi)
miny, maxy, dy = 30., 46.2, dxkm/40000.*360.
mediterr = spectralSquare('mediterr', 'mediterr', minx, miny, maxx, maxy, dx, dy, swNestLastIndex + 1)
figures = [mediterr]


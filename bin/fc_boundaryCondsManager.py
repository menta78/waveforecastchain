from abc import *
import os
from fc_common import fc_exception
import fc_common

class abstractBoundaryCondsManager:
  __metaclass__ = ABCMeta

  """
  mode can be READ or WRITE
  """
  @abstractmethod
  def __init__(self, settings, boundFilesStr):
    self.settings = settings

  @abstractmethod
  def generateScript(self):
    return templateLines

"""
SPEC: txt spec format. Uses ww3_bound program.
NC: ncdf format. Uses ww3_bounc program. Currently unsupported.
"""
boundCondTypes = ['SPEC', 'NC']
def boundaryCondsManager(boundCondType, settings, boundFilesStr):
  if boundCondType == 'SPEC':
    return specBoundaryCondsManager(settings, boundFilesStr)
  elif boundCondType == 'NC':
    raise fc_exception('NC boundary condition type: yet unsupported')
  else:
    raise fc_exception('boundary condition type must be one of: ' + ','.join(boundCondTypes))


class specBoundaryCondsManager(abstractBoundaryCondsManager):
 
  def __init__(self, settings, boundFiles):
    self.settings = settings
    self.boundFiles = boundFiles
    bindir = os.path.dirname(os.path.abspath(fc_common.__file__))
    self.templatefile = os.path.join(bindir, '../templates/ww3_bound_TEMPLATE.sh')

  def generateScript(self):

    def getSpecFileHandlingStrings():
      shortFLines, lnkLines, rmLines = '', '', ''
      for bf in self.boundFiles:
        f = bf.split(os.path.sep)[-1]
        shortFLines += '\n' + f
        lnkLines += '\n ln -sf ' + bf + ' ' + f
        rmLines += '\n rm -f ' + f
      return shortFLines.strip(' \n'), lnkLines.strip(' \n'), rmLines.strip(' \n')

    fl = open(self.templatefile)
    lines = fl.readlines()
    fl.close()
    lines = [s.replace('@@MAIN_GRID', self.settings.main_grid_name) for s in lines]
    shortFLines, lnkLines, rmLines = getSpecFileHandlingStrings()
    try:
      interpMethod = self.settings.boundaryCondInterpMethod
    except:
      interpMethod = 2
    lines = [s.replace('@@BOUND_INTERP_METHOD', str(interpMethod)) for s in lines]
    lines = [s.replace('@@BOUND_SPEC_LINK_FILES', lnkLines) for s in lines]
    lines = [s.replace('@@BOUND_SPEC_REMOVE_FILES', rmLines) for s in lines]
    lines = [s.replace('@@BOUND_SPEC_SHORT_FILES', shortFLines) for s in lines]
    return ''.join(lines)

def elaborateMainScriptInpuntBC(settings, mainScriptLines):
  try:
    boundaryCond = settings.boundaryCond
    boundaryFileType = settings.boundaryFileType
    boundaryCondFiles = settings.boundaryCondFiles
  except:
    print('Unable to find proper boundary condition import configuration')
    boundaryCond = False
    boundaryFileType = ''
    boundaryCondFiles = []
  if boundaryCond:
    bcManager = boundaryCondsManager(boundaryFileType, settings, boundaryCondFiles)
    bndCondScrpt = bcManager.generateScript()
  else:
    bndCondScrpt = ''  
  mainScriptLines = [s.replace('@@BOUNDARY_INPUT', bndCondScrpt) for s in mainScriptLines]
  return mainScriptLines




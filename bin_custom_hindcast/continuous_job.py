#!/usr/bin/python
import os
import sys

logfile = '/home/wavewatch/usr/wavewatch/v3.14/fc/hindcast/log/run.log'
while True:
  try:
    os.remove(logfile)
  except:
    pass
  print('new simulation')
  sys.stdout.flush()
  error = os.system('./fc_launch.py > ' + logfile  + ' 2>&1')

  if error:
    print('some errors occurred. Quitting continuous job')
    exit()
